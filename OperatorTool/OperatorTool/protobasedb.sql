USE [protobase]
GO
/****** Object:  Table [dbo].[assignedroutes]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[assignedroutes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[rid] [int] NOT NULL,
	[date_assigned] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[assignedvehicles]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[assignedvehicles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[vid] [int] NOT NULL,
	[date_assigned] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[frm_customer_details]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[frm_customer_details](
	[ROWID] [int] IDENTITY(1,1) NOT NULL,
	[RouteID] [int] NULL,
	[StartTime] [datetime] NULL,
	[UserFirstName] [nvarchar](30) NULL,
	[UserLastName] [nvarchar](40) NULL,
	[Route] [nvarchar](50) NULL,
	[Outletname] [nvarchar](50) NULL,
	[Contactname] [nvarchar](50) NULL,
	[Contactnumber] [nvarchar](20) NULL,
	[Street] [nvarchar](30) NULL,
	[Area] [nvarchar](30) NULL,
	[PriceCategory] [nchar](20) NULL,
	[ReturnsCat] [nchar](10) NULL,
	[customer_id] [nvarchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpesa_payments]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpesa_payments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionType] [nchar](60) NULL,
	[TransID] [nchar](20) NULL,
	[TransTime] [nvarchar](50) NULL,
	[TransAmount] [decimal](18, 0) NULL,
	[BusinessShortCode] [nchar](10) NULL,
	[BillRefNumber] [nchar](50) NULL,
	[InvoiceNumber] [nchar](50) NULL,
	[MSISDN] [nchar](15) NULL,
	[FirstName] [nchar](30) NULL,
	[MiddleName] [nchar](30) NULL,
	[LastName] [nchar](30) NULL,
	[OrgAccountBalance] [nchar](20) NULL,
	[DateTimeStamp] [datetime] NULL,
	[status] [int] NULL,
	[UsageBalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[outlets]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[outlets](
	[outid] [int] IDENTITY(1,1) NOT NULL,
	[routename] [varchar](50) NULL,
	[outletname] [varchar](50) NULL,
	[contact] [varchar](15) NULL,
	[dateadded] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[products]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[products](
	[pid] [int] IDENTITY(1,1) NOT NULL,
	[pname] [varchar](50) NOT NULL,
	[pcost] [varchar](50) NOT NULL,
	[date_added] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[routes]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[routes](
	[rid] [int] IDENTITY(1,1) NOT NULL,
	[routename] [varchar](50) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bulk_customers]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bulk_customers](
	[customer_id] [int] NULL,
	[customer_name] [nvarchar](50) NULL,
	[postal_code] [nchar](10) NULL,
	[city] [nchar](20) NULL,
	[street] [nvarchar](100) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bulk_loading]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bulk_loading](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[loading_id] [nchar](15) NULL,
	[loading_point] [nchar](30) NULL,
	[vregno] [nchar](10) NULL,
	[loaded_quantity] [int] NULL,
	[loading_number] [nvarchar](20) NULL,
	[driver_id] [int] NULL,
	[loading_order] [nvarchar](50) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bulk_orders]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bulk_orders](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_id] [nvarchar](20) NULL,
	[customer_id] [nvarchar](20) NULL,
	[price] [int] NULL,
	[quantity] [int] NULL,
	[quantity_approved] [nchar](10) NULL,
	[delivery_date] [datetime] NULL,
	[user_id] [int] NULL,
	[date_added] [datetime] NULL,
	[status] [int] NULL,
	[actual_delivery_date] [datetime] NULL,
	[quantity_delivered] [int] NULL,
	[delivery_note_path] [nchar](50) NULL,
	[delivery_note_no] [nchar](15) NULL,
	[driver_id] [int] NULL,
	[action_message] [nvarchar](220) NULL,
	[vid] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_customer_details]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_customer_details](
	[ROWID] [nvarchar](30) NULL,
	[RouteID] [int] NULL,
	[StartTime] [datetime] NULL,
	[UserFirstName] [nvarchar](30) NULL,
	[UserLastName] [nvarchar](40) NULL,
	[Route] [nvarchar](50) NULL,
	[Outletname] [nvarchar](50) NULL,
	[Contactname] [nvarchar](50) NULL,
	[Contactnumber] [nvarchar](20) NULL,
	[Street] [nvarchar](50) NULL,
	[Area] [nvarchar](30) NULL,
	[PriceCategory] [nchar](20) NULL,
	[ReturnsCat] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fuel_requests]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fuel_requests](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_id] [nchar](15) NULL,
	[driver_id] [int] NULL,
	[vregno] [nchar](10) NULL,
	[request_mileage] [int] NULL,
	[request_quantity] [nvarchar](30) NULL,
	[date_requested] [datetime] NULL,
	[fueling_quantity] [int] NULL,
	[fueling_mileage] [decimal](18, 0) NULL,
	[Status] [int] NULL,
	[approved_quantity] [nvarchar](50) NULL,
	[date_approved] [datetime] NULL,
	[approved_by] [nchar](19) NULL,
	[date_closed] [datetime] NULL,
	[fueling_station] [nvarchar](50) NULL,
	[other_location] [nvarchar](50) NULL,
	[till_number] [nchar](10) NULL,
	[pre_mileage] [nchar](10) NULL,
	[pre_quantity] [nchar](10) NULL,
	[invoice_number] [nchar](19) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_outlets]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_outlets](
	[outlet_id] [int] NOT NULL,
	[route_id] [int] NULL,
	[outletname] [nvarchar](50) NULL,
	[latitude] [nvarchar](50) NOT NULL,
	[longtitude] [nvarchar](50) NULL,
	[contact_person] [nvarchar](50) NULL,
	[contact_number] [nvarchar](50) NULL,
	[street] [nvarchar](50) NULL,
	[area] [nvarchar](50) NULL,
	[date_added] [datetime] NULL,
	[added_by_user_id] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_preorders]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_preorders](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[log_id] [int] NULL,
	[order_id] [nvarchar](50) NULL,
	[outlet_id] [int] NULL,
	[route_id] [int] NULL,
	[user_id] [int] NULL,
	[outletname] [nvarchar](50) NULL,
	[refill_6kg] [int] NULL,
	[refill_13kg] [int] NULL,
	[refill_50kg] [int] NULL,
	[outright_6kg] [int] NULL,
	[outright_13kg] [int] NULL,
	[outright_50kg] [int] NULL,
	[grill] [int] NULL,
	[burner] [int] NULL,
	[TransTime] [datetime] NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_preseller_routes]    Script Date: 11/19/2018 3:06:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_preseller_routes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[route_id] [int] NULL,
	[date_assigned] [datetime] NULL,
	[assigned_by] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_routes]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_routes](
	[route_id] [int] IDENTITY(1,1) NOT NULL,
	[route_name] [nvarchar](50) NULL,
	[territory] [nvarchar](50) NULL,
	[WhatsappGroupID] [nchar](10) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_sap_cylinder_brands]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_sap_cylinder_brands](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[material_code] [nvarchar](50) NULL,
	[size] [nchar](10) NULL,
	[brand_name] [nvarchar](50) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_trips_planning]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_trips_planning](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[trip_id] [nchar](30) NULL,
	[driver_id] [int] NULL,
	[vid] [int] NULL,
	[truck_helper] [nvarchar](50) NULL,
	[Category] [nchar](30) NULL,
	[destination] [nvarchar](50) NULL,
	[route_names] [nvarchar](100) NULL,
	[planned_6KG] [int] NULL,
	[planned_13KG] [int] NULL,
	[planned_50KG] [int] NULL,
	[Burners] [int] NULL,
	[Grills] [int] NULL,
	[date_added] [datetime] NULL,
	[Status] [int] NULL,
	[plan_date] [date] NULL,
	[date_started] [datetime] NULL,
	[date_end] [datetime] NULL,
	[trip_cost] [int] NULL,
	[date_loaded] [datetime] NULL,
	[date_offloaded] [datetime] NULL,
	[deleted] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_truck_helpers]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_truck_helpers](
	[thelper_id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](30) NULL,
	[last_name] [nvarchar](30) NULL,
	[phone_number] [nvarchar](20) NULL,
	[date_added] [datetime] NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_workshop_schedule]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_workshop_schedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[driver_id] [int] NULL,
	[vehicle_reg] [nchar](13) NULL,
	[repair_type] [nchar](30) NULL,
	[description] [nchar](60) NULL,
	[repair_user_info] [nchar](200) NULL,
	[date_checked_in] [datetime] NULL,
	[date_checked_out] [datetime] NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompetitorCylinders]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompetitorCylinders](
	[CylinderID] [int] IDENTITY(1,1) NOT NULL,
	[Brand] [nvarchar](50) NULL,
	[ReturnsCategory] [nchar](10) NULL,
	[Description] [nvarchar](50) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContainers]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContainers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ContainerName] [nchar](30) NULL,
	[Location] [nchar](40) NULL,
	[Capacity] [int] NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCustomers]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCustomers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NULL,
	[CustomerName] [varchar](50) NULL,
	[CustomerPhone] [varchar](50) NULL,
	[RouteID] [varchar](50) NULL,
	[OutletName] [varchar](50) NULL,
	[Street] [varchar](50) NULL,
	[Area] [varchar](50) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCylinderReturns]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCylinderReturns](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SaleID] [varchar](50) NULL,
	[RCylinder] [varchar](30) NULL,
	[RQuantity6KG] [int] NULL,
	[RQuantity13KG] [int] NULL,
	[RQuantity50KG] [int] NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDrivers]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDrivers](
	[DriverID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](30) NULL,
	[LastName] [varchar](50) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Status] [int] NULL,
	[Position] [nchar](15) NULL,
	[date_added] [datetime] NULL,
	[login_type] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMasterSales]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMasterSales](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[trip_id] [nvarchar](30) NULL,
	[SaleID] [varchar](50) NULL,
	[DriverID] [int] NULL,
	[RouteID] [int] NULL,
	[OutletID] [int] NULL,
	[RName] [nchar](30) NULL,
	[TillNumber] [nchar](10) NULL,
	[CustomerName] [varchar](50) NULL,
	[VRegNo] [nchar](10) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Lng] [decimal](18, 0) NULL,
	[TransAmount] [int] NULL,
	[Variance] [nchar](10) NULL,
	[MpesaAmount] [int] NULL,
	[Comment] [nvarchar](50) NULL,
	[BankAmount] [int] NULL,
	[InvoiceAmount] [int] NULL,
	[TransTime] [datetime] NULL,
	[date_added] [datetime] NULL,
	[Date_Paid] [datetime] NULL,
	[TransID] [nchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPayments]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPayments](
	[log_id] [int] NULL,
	[PaymentID] [int] IDENTITY(1,1) NOT NULL,
	[SaleID] [nvarchar](50) NULL,
	[PaymentMethod] [nchar](30) NULL,
	[PhoneNumber] [nchar](20) NULL,
	[TransID] [nchar](60) NULL,
	[TransAmount] [int] NULL,
	[Status] [nchar](15) NULL,
	[DatePaid] [date] NULL,
	[DueDate] [datetime] NULL,
	[DateReceived] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPrices]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPrices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RID] [int] NULL,
	[RName] [varchar](50) NULL,
	[PriceCategory] [varchar](50) NULL,
	[ReturnsCat] [nchar](10) NULL,
	[Refill6KG] [int] NULL,
	[Refill13KG] [int] NULL,
	[Refill50KG] [int] NULL,
	[Outright6KG] [int] NULL,
	[Outright13KG] [int] NULL,
	[Outright50KG] [int] NULL,
	[PriceBurner] [int] NULL,
	[PriceGrill] [int] NULL,
	[date_added] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRouteAssigned]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRouteAssigned](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RID] [varchar](50) NULL,
	[DriverID] [varchar](50) NULL,
	[DateAssigned] [varchar](50) NULL,
	[AssignedBy] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRouteAssignments]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRouteAssignments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RID] [varchar](50) NULL,
	[DriverID] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRoutes]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRoutes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RID] [varchar](50) NULL,
	[RName] [varchar](50) NULL,
	[Territory] [nchar](30) NULL,
	[date_added] [datetime] NULL,
	[WhatsAppGroupID] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSaleTargets]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSaleTargets](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[VID] [int] NULL,
	[VRegNo] [nchar](10) NULL,
	[TBasic] [int] NULL,
	[TierOne] [int] NULL,
	[TierTwo] [int] NULL,
	[TierThree] [int] NULL,
	[TierFour] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSoldProducts]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSoldProducts](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SaleID] [varchar](50) NULL,
	[ProductDesc] [varchar](50) NULL,
	[Quantity] [int] NULL,
	[SKUTransAmount] [int] NULL,
	[date_added] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStockTransfer]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStockTransfer](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [nvarchar](50) NOT NULL,
	[VRegNo] [nchar](10) NULL,
	[UserName] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[DriverID] [int] NULL,
	[tSource] [nvarchar](50) NULL,
	[destination] [nvarchar](50) NULL,
	[Category] [nvarchar](50) NULL,
	[Loaded6KG] [int] NULL,
	[Loaded13KG] [int] NULL,
	[Loaded50KG] [int] NULL,
	[Burners] [int] NULL,
	[Grills] [int] NULL,
	[DateTransferred] [datetime] NULL,
	[DateTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTargetGroup]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTargetGroup](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[RID] [nchar](10) NULL,
	[TGroupID] [nchar](10) NULL,
	[WGroupID] [nchar](10) NULL,
	[RouteName] [nvarchar](50) NULL,
	[TGroupName] [nvarchar](50) NULL,
	[Basic] [int] NULL,
	[TierOne] [int] NULL,
	[TierTwo] [int] NULL,
	[TierThree] [int] NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTillAssignments]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTillAssignments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TillNumber] [char](10) NULL,
	[DriverID] [char](10) NULL,
	[Status] [int] NULL,
	[DateAssigned] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTillNumbers]    Script Date: 11/19/2018 3:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTillNumbers](
	[TID] [int] IDENTITY(1,1) NOT NULL,
	[StoreNumber] [nchar](10) NULL,
	[TillNumber] [nchar](10) NULL,
	[BranchName] [nvarchar](40) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTrips]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTrips](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [nvarchar](50) NOT NULL,
	[DriverID] [int] NULL,
	[VRegNo] [nchar](10) NULL,
	[DateLoaded] [datetime] NULL,
	[DateOffloaded] [datetime] NULL,
	[Status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTruckLoading]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTruckLoading](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [nvarchar](50) NOT NULL,
	[VRegNo] [nchar](10) NULL,
	[UserName] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[DriverID] [int] NULL,
	[op_bal_6KG] [int] NULL,
	[op_bal_13KG] [int] NULL,
	[op_bal_50KG] [int] NULL,
	[op_bal_grills] [int] NULL,
	[op_bal_burners] [int] NULL,
	[Loaded6KG] [int] NULL,
	[Loaded13KG] [int] NULL,
	[Loaded50KG] [int] NULL,
	[Burners] [int] NULL,
	[Grills] [int] NULL,
	[DateLoaded] [datetime] NULL,
	[DateTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTruckUnLoading]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTruckUnLoading](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [nvarchar](50) NOT NULL,
	[VRegNo] [nchar](10) NULL,
	[UserName] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[DriverID] [int] NULL,
	[Mileage] [int] NULL,
	[Unsold6KG] [int] NULL,
	[Unsold13KG] [int] NULL,
	[Unsold50KG] [int] NULL,
	[UnsoldBurners] [int] NULL,
	[UnsoldGrills] [int] NULL,
	[DateOffLoaded] [datetime] NULL,
	[DateTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnloadingReturns]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnloadingReturns](
	[log_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [varchar](50) NULL,
	[RCylinder] [varchar](30) NULL,
	[RQuantity6KG] [int] NULL,
	[RQuantity13KG] [int] NULL,
	[RQuantity50KG] [int] NULL,
	[DateTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUsers]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUsers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[LoginType] [int] NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[assignedLocation] [nchar](30) NULL,
	[date_added] [datetime] NULL,
	[Status] [int] NULL,
	[Position] [nchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVehicleAssignments]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVehicleAssignments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AID] [varchar](50) NULL,
	[DriverID] [varchar](50) NULL,
	[TripID] [nchar](30) NULL,
	[VID] [varchar](50) NULL,
	[DateAssigned] [varchar](50) NULL,
	[AssignedBy] [varchar](50) NULL,
	[Status] [varchar](10) NULL,
	[date_added] [datetime] NULL,
	[truckhelper] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVehicles]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVehicles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[VID] [varchar](50) NULL,
	[VRegNO] [varchar](50) NULL,
	[VType] [varchar](50) NULL,
	[VCapacity] [int] NULL,
	[VStatus] [int] NULL,
	[date_added] [datetime] NULL,
	[UsageType] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWhatsAppGroups]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWhatsAppGroups](
	[GroupID] [varchar](10) NULL,
	[GroupName] [nchar](100) NULL,
	[GroupAdmin] [nchar](15) NULL,
	[ChatID] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[userno] [varchar](10) NULL,
	[typeid] [int] NULL,
	[usernames] [varchar](50) NULL,
	[usercontact] [varchar](15) NOT NULL,
	[username] [varchar](200) NOT NULL,
	[password] [varchar](200) NOT NULL,
	[dateadded] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usertypes]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usertypes](
	[typeid] [int] IDENTITY(1,1) NOT NULL,
	[usertype] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vehicles]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehicles](
	[vid] [int] IDENTITY(1,1) NOT NULL,
	[vtypeid] [int] NOT NULL,
	[regno] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vehicletypes]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehicletypes](
	[vtypeid] [int] IDENTITY(1,1) NOT NULL,
	[vtype] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[assignedroutes] ADD  CONSTRAINT [DF_assignedroutes_date_assigned]  DEFAULT (getdate()) FOR [date_assigned]
GO
ALTER TABLE [dbo].[assignedvehicles] ADD  CONSTRAINT [DF_assignedvehicles_date_assigned]  DEFAULT (getdate()) FOR [date_assigned]
GO
ALTER TABLE [dbo].[mpesa_payments] ADD  CONSTRAINT [DF_mpesa_payments_DateTimeStamp]  DEFAULT (getdate()) FOR [DateTimeStamp]
GO
ALTER TABLE [dbo].[outlets] ADD  CONSTRAINT [DF_outlets_dateadded]  DEFAULT (getdate()) FOR [dateadded]
GO
ALTER TABLE [dbo].[products] ADD  CONSTRAINT [DF_products_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_bulk_loading] ADD  CONSTRAINT [DF_tbl_bulk_loading_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_bulk_orders] ADD  CONSTRAINT [DF_tbl_bulk_orders_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_bulk_orders] ADD  CONSTRAINT [DF_tbl_bulk_orders_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[tbl_fuel_requests] ADD  CONSTRAINT [DF_tbl_fuel_requests_date_requested]  DEFAULT (getdate()) FOR [date_requested]
GO
ALTER TABLE [dbo].[tbl_fuel_requests] ADD  CONSTRAINT [DF_tbl_fuel_requests_fueling_quantity]  DEFAULT ((0)) FOR [fueling_quantity]
GO
ALTER TABLE [dbo].[tbl_fuel_requests] ADD  CONSTRAINT [DF_tbl_fuel_requests_fueling_mileage]  DEFAULT ((0)) FOR [fueling_mileage]
GO
ALTER TABLE [dbo].[tbl_fuel_requests] ADD  CONSTRAINT [DF_tbl_fuel_requests_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[tbl_fuel_requests] ADD  CONSTRAINT [DF_tbl_fuel_requests_approved_quantity]  DEFAULT ((0)) FOR [approved_quantity]
GO
ALTER TABLE [dbo].[tbl_outlets] ADD  CONSTRAINT [DF_tbl_outlets_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_preseller_routes] ADD  CONSTRAINT [DF_tbl_preseller_routes_date_assigned]  DEFAULT (getdate()) FOR [date_assigned]
GO
ALTER TABLE [dbo].[tbl_sap_cylinder_brands] ADD  CONSTRAINT [DF_tbl_sap_cylinder_brands_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_trips_planning] ADD  CONSTRAINT [DF_tbl_trips_planning_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_trips_planning] ADD  CONSTRAINT [DF_tbl_trips_planning_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[tbl_trips_planning] ADD  CONSTRAINT [DF_tbl_trips_planning_deleted]  DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[tbl_truck_helpers] ADD  CONSTRAINT [DF_tbl_truck_helpers_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tbl_truck_helpers] ADD  CONSTRAINT [DF_tbl_truck_helpers_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[tbl_workshop_schedule] ADD  CONSTRAINT [DF_tbl_workshop_schedule_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblContainers] ADD  CONSTRAINT [DF_tblContainers_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblCylinderReturns] ADD  CONSTRAINT [DF_tblCylinderReturns_RQuantity6KG]  DEFAULT ((0)) FOR [RQuantity6KG]
GO
ALTER TABLE [dbo].[tblCylinderReturns] ADD  CONSTRAINT [DF_tblCylinderReturns_RQuantity13KG]  DEFAULT ((0)) FOR [RQuantity13KG]
GO
ALTER TABLE [dbo].[tblCylinderReturns] ADD  CONSTRAINT [DF_tblCylinderReturns_RQuantity50KG]  DEFAULT ((0)) FOR [RQuantity50KG]
GO
ALTER TABLE [dbo].[tblCylinderReturns] ADD  CONSTRAINT [DF_tblCylinderReturns_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblDrivers] ADD  CONSTRAINT [DF_tblDrivers_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[tblDrivers] ADD  CONSTRAINT [DF_tblDrivers_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblMasterSales] ADD  CONSTRAINT [DF_tblMasterSales_BankAmount]  DEFAULT ((0)) FOR [BankAmount]
GO
ALTER TABLE [dbo].[tblMasterSales] ADD  CONSTRAINT [DF_tblMasterSales_InvoiceAmount]  DEFAULT ((0)) FOR [InvoiceAmount]
GO
ALTER TABLE [dbo].[tblMasterSales] ADD  CONSTRAINT [DF_tblMasterSales_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblPayments] ADD  CONSTRAINT [DF_tblPayments_DateReceived]  DEFAULT (getdate()) FOR [DateReceived]
GO
ALTER TABLE [dbo].[tblPrices] ADD  CONSTRAINT [DF_tblPrices_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblSaleTargets] ADD  CONSTRAINT [DF_tblSaleTargets_TierFour]  DEFAULT ((1000)) FOR [TierFour]
GO
ALTER TABLE [dbo].[tblSoldProducts] ADD  CONSTRAINT [DF_tblSoldProducts_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblStockTransfer] ADD  CONSTRAINT [DF_tblStockTransfer_DateTimeStamp]  DEFAULT (getdate()) FOR [DateTimeStamp]
GO
ALTER TABLE [dbo].[tblTillAssignments] ADD  CONSTRAINT [DF_tblTillAssignments_DateAssigned]  DEFAULT (getdate()) FOR [DateAssigned]
GO
ALTER TABLE [dbo].[tblTillNumbers] ADD  CONSTRAINT [DF_tblTillNumbers_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[tblTrips] ADD  CONSTRAINT [DF_tblTrips_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[tblTruckLoading] ADD  CONSTRAINT [DF_tblTruckLoading_DateTimeStamp]  DEFAULT (getdate()) FOR [DateTimeStamp]
GO
ALTER TABLE [dbo].[tblUnloadingReturns] ADD  CONSTRAINT [DF_tblUnloadingReturns_DateTimeStamp]  DEFAULT (getdate()) FOR [DateTimeStamp]
GO
ALTER TABLE [dbo].[tblVehicleAssignments] ADD  CONSTRAINT [DF_tblVehicleAssignments_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[tblVehicles] ADD  CONSTRAINT [DF_tblVehicles_date_added]  DEFAULT (getdate()) FOR [date_added]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__dateadded__286302EC]  DEFAULT (getdate()) FOR [dateadded]
GO
/****** Object:  StoredProcedure [dbo].[AuthenticateUser]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AuthenticateUser]
(
	@Email		nvarchar(100),
	@Password	nvarchar(100)
)
AS
BEGIN
	SELECT * FROM tblUsers WHERE Email=@Email AND Password=@Password
END
GO
/****** Object:  StoredProcedure [dbo].[ContainerStock]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ContainerStock]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select destination,sum(Loaded6KG)in_6kg,sum(Loaded13KG)in_13kg,sum(Loaded50KG)in_50kg,
sum(burners)in_burners,sum(grills)in_grills ,sum(Unsold6KG)Unsold6KG,sum(Unsold13KG)Unsold13KG,sum(Unsold50KG)Unsold50KG,
sum(out_6kg)out_6kg,sum(out_13kg)out_13kg,sum(out_50kg)out_50kg,
sum(out_burners)out_burners,sum(out_grills)out_grills
from tblStockTransfer t
inner join
(
select assignedLocation,
sum (op_bal_6KG+Loaded6KG)out_6kg,sum(op_bal_13KG+Loaded13KG)out_13kg,
sum(op_bal_50KG+Loaded50KG)out_50kg ,sum(op_bal_grills+Grills)out_grills,
sum(op_bal_burners+Burners)out_burners
from tblTruckLoading l inner join tblUsers u on l.UserID=u.UserID
group by assignedLocation
) y on y.assignedLocation=t.destination
inner join
(
select Unsold6KG,Unsold13KG,Unsold50KG,UnsoldBurners,UnsoldGrills, assignedLocation from tblTruckUnLoading l
inner join tblUsers u on u.UserID=l.UserID
)x  on x.assignedLocation=t.destination
group by destination
END
GO
/****** Object:  StoredProcedure [dbo].[EditDriver]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[EditDriver] 
	-- Add the parameters for the stored procedure here
	@DriverID bigint,
	@FirstName nvarchar(50),	
	@LastName nvarchar(50),
	@Email nvarchar(100),	
	@PhoneNumber nvarchar(100),	
	@Password nvarchar(100)
	
    AS
BEGIN
	IF (@Password = NULL) 
		BEGIN
		UPDATE tblDrivers SET 
		[FirstName] = @FirstName,		
		[LastName] = @LastName,
		[Email] = @Email,		
		[PhoneNumber] = @PhoneNumber		
		WHERE DriverID=@DriverID
		END
	ELSE 
		BEGIN		
		UPDATE tblDrivers SET 
		[FirstName] = @FirstName,		
		[LastName] = @LastName,
		[Email] = @Email,		
		[PhoneNumber] = @PhoneNumber,		
		[Password] = @Password		
		WHERE DriverID=@DriverID
		END	 
END
GO
/****** Object:  StoredProcedure [dbo].[FuelingDashBoard]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
create PROCEDURE [dbo].[FuelingDashBoard]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select (
		SELECT COUNT(*) FROM tbl_fuel_requests
		where Status='0'
		) as pending,
	 (
      SELECT COUNT(*)
	  FROM tbl_fuel_requests WHERE Status = '1'
	  ) AS approved,
	  ( SELECT COUNT(*)
	  FROM tbl_fuel_requests WHERE Status = '2'
	  ) AS rejected,
	  ( SELECT COUNT(*)
	  FROM tbl_fuel_requests WHERE Status = '3' 
	  ) AS closed,
	   ( 
	   
	   SELECT COUNT(*)
	  FROM tbl_fuel_requests  where cast(date_requested as date)= cast(getdate() as date)
	  ) AS today_requests
	  
END
GO
/****** Object:  StoredProcedure [dbo].[GetActiveTrips]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetActiveTrips]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select trip_id,destination,PhoneNumber,p.status,FirstName,LastName,VRegNO,route_names,truck_helper as truckhelper,Category,
trip_cost,datediff(mi,p.date_added,getdate())time_taken,p.plan_date as planned_date
 from tbl_trips_planning p inner join tblDrivers d on p.driver_id=d.DriverID
inner join tblVehicles v on v.VID= p.vid where p.plan_date >= cast(getdate() as date)
END
GO
/****** Object:  StoredProcedure [dbo].[GetBulkOrders]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetBulkOrders]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select order_id, customer_name,o.customer_id,street,quantity,delivery_date,status from tbl_bulk_orders o
inner join tbl_bulk_customers c on c.customer_id=o.customer_id
END
GO
/****** Object:  StoredProcedure [dbo].[GetDrivers]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDrivers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
select d.DriverID,Email,a.VID,a.truckhelper, FirstName,VRegNo,LastName,PhoneNumber,TillNumber,
 CurrentRoutes = STUFF((
        SELECT ',' + t.RName
        FROM tblRouteAssignments pt
        LEFT oUTER JOIN tblRoutes t
            ON pt.RID = t.RID
        WHERE pt.DriverID = d.DriverID and pt.Status='1'
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(200)'), 1, 1, '')		
from tblDrivers d LEFT OUTER join (select * from tblVehicleAssignments where status='1') a on a.DriverID=d.DriverID
LEFT OUTER join tblVehicles v on a.VID=v.VID
LEFT OUTER join tblTillAssignments l on l.DriverID=d.DriverID


END
GO
/****** Object:  StoredProcedure [dbo].[GetEditSale]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[GetEditSale]
@SaleID nvarchar(30)
AS
BEGIN
Select FirstName,LastName,VRegNo, CustomerName,s.id,s.SaleID,ProductDesc,s.Quantity,s.SKUTransAmount,s.date_added from tblSoldProducts s
left  join tblMasterSales m on s.SaleID =m.SaleID inner join
tblDrivers d on d.DriverID=m.DriverID where s.SaleID like '%'+@SaleID+'%'
END
GO
/****** Object:  StoredProcedure [dbo].[GetFuelRequests]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetFuelRequests]
AS
BEGIN
SET NOCOUNT ON;
select order_id,driver_id,concat(d.FirstName +' ',d.LastName) driver_name,
fueling_station,vregno,request_mileage,request_quantity,f.status,
date_requested from tbl_fuel_requests f inner join tblDrivers d on 
d.DriverID = f.driver_id where date_requested>=cast(getdate()-2 as date) 
 order by date_requested asc
END
GO
/****** Object:  StoredProcedure [dbo].[GetLoadingVehicles]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetLoadingVehicles]
AS
BEGIN

	SET NOCOUNT ON;
select FirstName ,LastName ,VRegNO,DriverID,trip_id as TripID
from tbl_trips_planning t inner join tblDrivers d on d.DriverID=t.driver_id
inner join tblVehicles v on v.VID=t.vid where t.Status='0'
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetMpesaPayments]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetMpesaPayments]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select SaleID,concat(FirstName+' ',LastName)app_user_name,
'Mpesa Payment' payment_method,TransID,MpesaAmount,Date_Paid,TransTime
from tblMasterSales m inner join tblDrivers d on d.DriverID=m.DriverID
where cast(m.TransTime as date)=cast(getdate()-1 as date) and MpesaAmount >0
END
GO
/****** Object:  StoredProcedure [dbo].[GetPayLaterSales]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPayLaterSales]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select m.SaleID,concat(FirstName+'',LastName)DriverName,VRegno,m.RName,CustomerName,Refill6KG,Refill13KG,Refill50KG,
Outright6KG,Outright13KG,Outright50KG,m.TransAmount, p.PhoneNumber,
p.PaymentMethod, p.Status, m.Date_Paid,m.TransTime,m.MpesaAmount, Comment ,
DATEDIFF(mi, m.TransTime, ISNULL(m.Date_Paid,getdate()))TimeElapsed
from tblPayments p inner 
join tblMasterSales m on p.SaleID = m.SaleID inner join tblDrivers d 
on d.DriverID = m.DriverID  
inner join (

select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID
) y
on y.SaleID =p.SaleID


where p.Status = 'Pay later' and  p.DateReceived >= cast(getdate() as date)
and (Variance)*-1=m.TransAmount


END
GO
/****** Object:  StoredProcedure [dbo].[GetPresellerData]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[GetPresellerData]
@user_id int
AS
BEGIN

SET NOCOUNT ON;
select RowID, Outletname,RouteID,c.PriceCategory, Refill6KG,Refill13KG,Refill50KG,Outright6KG,Outright13KG,Outright50KG
,PriceBurner,PriceGrill from tbl_preseller_routes r inner join frm_customer_details c on c.RouteID=r.route_id
 inner join tblPrices p on p.RID=c.RouteID and p.PriceCategory =c.PriceCategory
 where r.user_id=@user_id
END
GO
/****** Object:  StoredProcedure [dbo].[GetReconSummary]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetReconSummary]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select max(DriverID) DriverID,
max(DriverName) DriverName,VRegNo,sum (Refill6KG)Refill6KG,sum (Refill13KG)Refill13KG,
sum (Refill50KG)Refill50KG,sum (Outright6KG)Outright6KG,sum (Outright13KG)Outright13KG,sum (Outright50KG)Outright50KG,
sum (Grill)Grill,sum (Burner)Burner,sum(TransAmount)TransAmount,
sum(BankPayment) BankPayment,
sum(MpesaPayment)MpesaPayment,sum(ChequePayment)ChequePayment,sum(InvoicePayment)InvoicePayment,
sum(MpesaAmount)MpesaAmount,sum(cast(Variance as integer))Variance
 from (select d.DriverID,concat(d.FirstName+ ' ',d.LastName) DriverName,BankPayment,MpesaPayment,ChequePayment,InvoicePayment,
 VRegNo,Refill6KG,Refill13KG,Refill50KG,Outright6KG,Outright13KG,Outright50KG,
Grill,Burner,TransAmount,MpesaAmount,Variance from tblMasterSales m
inner join
(
select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID) 
p on p.SaleID =m.SaleID
inner join
(
select SaleID,
sum(case when PaymentMethod='Bank Payment' then TransAmount else 0 end) BankPayment,
sum(case when PaymentMethod='Cheque Payment' then TransAmount else 0 end) ChequePayment,
sum(case when PaymentMethod='Invoice Payment' then TransAmount else 0 end) InvoicePayment,
sum(case when PaymentMethod='Mpesa Payment' then TransAmount else 0 end) MpesaPayment

from tblPayments 
where cast(DateReceived as date)=cast(getdate() as date)
group by SaleID
) f on f.SaleID=p.SaleID
inner join tblDrivers d on d.DriverID=m.DriverID
where m.date_added>= cast(getdate() as date)
) y
group by VRegNo
END
GO
/****** Object:  StoredProcedure [dbo].[GetSales]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSales]

AS
BEGIN

SET NOCOUNT ON;



select m.SaleID,d.DriverID,d.FirstName,d.LastName,VRegNo,CustomerName,Refill6KG,Refill13KG,Refill50KG,Outright6KG,
Outright13KG,Outright13KG,Outright50KG,Grill,Burner,
TransAmount,TransTime from tblMasterSales  m
inner join tblDrivers d on d.DriverID=m.DriverID
 inner join 
(select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID)s on 
s.SaleID =m.SaleID where m.date_added >cast(getdate()as date)
END
GO
/****** Object:  StoredProcedure [dbo].[GetSalesFilter]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSalesFilter]
@StartDate datetime,		
@EndDate datetime
AS
BEGIN

SET NOCOUNT ON;



select m.OutletID,m.SaleID,d.DriverID,d.FirstName,d.LastName,VRegNo,CustomerName,Refill6KG,Refill13KG,Refill50KG,Outright6KG,
Outright13KG,Outright13KG,Outright50KG,Grill,Burner,Variance,MpesaAmount,comment,
TransAmount,TransTime from tblMasterSales  m
inner join tblDrivers d on d.DriverID=m.DriverID
 inner join 
(select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID)s on 
s.SaleID =m.SaleID where cast(m.date_added as date)<=cast(@StartDate as date) AND 
cast(m.date_added as date) >=cast(@Enddate as date)
END
GO
/****** Object:  StoredProcedure [dbo].[GetSalesPerTruck]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSalesPerTruck]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select max(DriverID) DriverID,max(DriverName) DriverName,VRegNo,sum (Refill6KG)Refill6KG,sum (Refill13KG)Refill13KG,
sum (Refill50KG)Refill50KG,sum (Outright6KG)Outright6KG,sum (Outright13KG)Outright13KG,sum (Outright50KG)Outright50KG,
sum (Grill)Grill,sum (Burner)Burner,sum(TransAmount)TransAmount
 from (select d.DriverID,concat(d.FirstName+ ' ',d.LastName) DriverName,VRegNo,Refill6KG,Refill13KG,Refill50KG,Outright6KG,Outright13KG,Outright50KG,
Grill,Burner,TransAmount from tblMasterSales m
inner join
(select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID) p on p.SaleID =m.SaleID
inner join tblDrivers d on d.DriverID=m.DriverID
where cast(m.date_added as date)>= cast(getdate() as date)
) y

group by VRegNo
order by (sum (Refill6KG)+ sum(Refill13KG)+
sum (Refill50KG)+sum (Outright6KG)+sum (Outright13KG)+sum (Outright50KG) ) desc


                                
END
GO
/****** Object:  StoredProcedure [dbo].[GetSalesTarget]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSalesTarget]
@driver_id nvarchar(30)
AS
BEGIN
SET NOCOUNT ON;
select a.DriverID ,VRegNo,trip_id, ('500')total_loaded,TBasic,TierOne,TierTwo,TierThree,TierFour 
from tblVehicleAssignments a inner join  tblSaleTargets t on a.VID = t.VID inner join tbl_trips_planning p
on p.driver_id=a.DriverID where p.Status='1'
and a.DriverID =@driver_id  AND A.Status = '1'
END
GO
/****** Object:  StoredProcedure [dbo].[GetSoldProducts]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSoldProducts]
@startDate datetime,		
@endDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select VregNo,CustomerName,concat(FirstName +' ',LastName)DriverName,RName,productdesc,Quantity,
Quantity,SKuTransAmount,p.date_added from tblSoldProducts p inner join tblMasterSales m 
on p.SaleID=m.SaleID inner join tblDrivers d on d.DriverID=m.DriverID
where p.date_added>=nullif(@startDate,cast(getdate() as date)) and  p.date_added<=nullif(@endDate,getdate()+ 1)



END
GO
/****** Object:  StoredProcedure [dbo].[GetStockTransfer]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetStockTransfer]
AS
BEGIN

	SET NOCOUNT ON;
select driver_id as DriverID,trip_id TripID,FirstName,LastName,destination,'' tSource,Category,vregno
from tbl_trips_planning p inner join tblDrivers d on p.driver_id=d.DriverID
inner join tblVehicles v on v.VID=p.vid
where p.Status='1' and p.Category='2'
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetTripsPlanning]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTripsPlanning]
AS
BEGIN

	SET NOCOUNT ON;
SELECT trip_id,vregno,vtype,concat(FirstName+' ',LastName)DriverName,
PhoneNumber,truck_helper,route_names,planned_6KG,planned_13KG,planned_50KG,Burners,Grills,p.date_added
FROM tbl_trips_planning p
inner join tblVehicles v on v.VID=p.vid
inner join  tblDrivers d on d.DriverID=p.driver_id 
where cast(p.date_added as date)=cast(getdate() as date)
END
GO
/****** Object:  StoredProcedure [dbo].[GetTruckSummary]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTruckSummary]
@trip_id nvarchar(20)
AS
BEGIN

	SET NOCOUNT ON;
Select max(DriverID) DriverID,max(DriverName) DriverName,VRegNo,sum (Refill6KG)Refill6KG,sum (Refill13KG)Refill13KG,
sum (Refill50KG)Refill50KG,sum (Outright6KG)Outright6KG,sum (Outright13KG)Outright13KG,sum (Outright50KG)Outright50KG,
sum (Grill)Grill,sum (Burner)Burner, sum(BankPayment)BankPayment, sum(MpesaPayment)MpesaPayment, sum(ChequePayment)ChequePayment,
 sum(InvoicePayment)InvoiceAmount,sum(TransAmount)TransAmount
 from 
 (
select d.DriverID,concat(d.FirstName+ ' ',d.LastName) DriverName,VRegNo,Refill6KG,Refill13KG,Refill50KG,Outright6KG,Outright13KG,Outright50KG,
Grill,Burner,BankPayment,MpesaPayment,ChequePayment,InvoicePayment, TransAmount from tblMasterSales m
inner join
(select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID
)
p on p.SaleID =m.SaleID
inner join tblDrivers d on d.DriverID=m.DriverID

inner join 
( 
SELECT SaleID,
sum(case when PaymentMethod='Bank Payment' then TransAmount else 0 end) BankPayment,
sum(case when PaymentMethod='Cheque Payment' then TransAmount else 0 end) ChequePayment,
sum(case when PaymentMethod='Invoice Payment' then TransAmount else 0 end) InvoicePayment,
sum(case when PaymentMethod='Mpesa Payment' then TransAmount else 0 end) MpesaPayment
from tblPayments group by SaleID
)k ON k.SaleID=m.SaleID
where cast(m.date_added as date)>= cast(getdate()-1 as date)
and trip_id=@trip_id
)y

group by VRegNo 
END
GO
/****** Object:  StoredProcedure [dbo].[GetUnLoadingVehicles]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUnLoadingVehicles]
AS
BEGIN

	SET NOCOUNT ON;
select  FirstName ,LastName ,v.VRegNO,d.DriverID,t.trip_id as TripID,T6KG,T13KG,T50KG 
from tbl_trips_planning t inner join tblDrivers d on d.DriverID=t.driver_id
inner join tblVehicles v on v.VID=t.vid 
left outer join
(
select top 1 trip_id, sum(RQuantity6KG) T6KG ,sum(RQuantity13KG)T13KG,sum(RQuantity50KG)T50KG from tblMasterSales m inner join tblCylinderReturns c
on m.SaleID=c.SaleID  
group by m.trip_id
)ex on ex.trip_id= t.trip_id
where v.VID not in (Select vid from tbl_trips_planning where (Status='1' or status='0'))
and t.date_offloaded is null
END
GO
/****** Object:  StoredProcedure [dbo].[GetUnplannedTrucks]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUnplannedTrucks]
@date date
AS
BEGIN

SET NOCOUNT ON;

SELECT v.VID,v.VRegNO,d.DriverID,FirstName,LastName,truckhelper,d.PhoneNumber,
 CurrentRoutes = STUFF((
        SELECT ',' + t.RName
        FROM tblRouteAssignments pt
        LEFT oUTER JOIN tblRoutes t
            ON pt.RID = t.RID
        WHERE pt.DriverID = d.DriverID and pt.Status='1'
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(200)'), 1, 1, '')
 FROM tblVehicles v
inner join tblVehicleAssignments a on a.vid=v.VID
inner join tblDrivers d on d.DriverID=a.DriverID
WHERE v.vid NOT IN 
(SELECT vid FROM tbl_trips_planning WHERE( status = 1 or status=0) and plan_date=@date)
and v.VID  in (SELECT vid FROM tblVehicleAssignments WHERE status = 1)
and a.Status='1'
END
GO
/****** Object:  StoredProcedure [dbo].[PlanningDashBoard]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE PROCEDURE [dbo].[PlanningDashBoard]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select (
		SELECT COUNT(*) FROM tblVehicles
		WHERE vid NOT IN 
		(SELECT vid FROM tbl_trips_planning WHERE status = 1 or status=0)
		) as unplanned,
	 (
      SELECT COUNT(*)
	  FROM tbl_trips_planning WHERE Status = 0
	  ) AS Pending,
	  ( SELECT COUNT(*)
	  FROM tbl_trips_planning WHERE Status = 1
	  ) AS Active,
	  ( SELECT COUNT(*)
	  FROM tbl_trips_planning WHERE Status = 2 and cast(date_added as date)= cast(getdate() as date)
	  ) AS Completed,
	   ( 
	   
	   SELECT COUNT(*)
	  FROM tbl_trips_planning  where cast(date_added as date)= cast(getdate() as date)
	  ) AS all_trips
	  
END
GO
/****** Object:  StoredProcedure [dbo].[PrePlanning]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PrePlanning] 
AS
BEGIN

	SET NOCOUNT ON;

select d.DriverID,Email,a.VID,a.truckhelper,p.plan_date, p.status,FirstName,VRegNo,LastName,PhoneNumber,TillNumber,
 CurrentRoutes = STUFF((
        SELECT ',' + t.RName
        FROM tblRouteAssignments pt
        LEFT oUTER JOIN tblRoutes t
            ON pt.RID = t.RID
        WHERE pt.DriverID = d.DriverID and pt.Status='1'
        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(200)'), 1, 1, '')		
from tblDrivers d inner join (select * from tblVehicleAssignments where status='1') a on a.DriverID=d.DriverID
inner join tblVehicles v on a.VID=v.VID
LEFT OUTER join tblTillAssignments l on l.DriverID=d.DriverID
inner join tbl_trips_planning p on p.VID=a.VID 
where a.VID is not null and p.date_added is not null

END
GO
/****** Object:  StoredProcedure [dbo].[SalesRecon]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[SalesRecon]
AS
BEGIN

	SET NOCOUNT ON;

SELECT l.TripID, z.VRegNo,DriverName,l.VRegNo,(op_bal_6KG+Loaded6KG) Loaded6KG ,(op_bal_13KG+Loaded13KG)Loaded13KG,
(op_bal_50KG+Loaded50KG) Loaded50KG ,
(Refill6KG+Outright6KG) Sold6KG,(Refill13KG+Outright13KG) Sold13KG,(Refill50KG+Outright50KG) Sold50KG
 ,Unsold6KG,Unsold13KG,Unsold50KG,TransAmount,MpesaPayment,BankPayment,ChequePayment, InvoicePayment,MpesaReceived from tblTruckLoading l
 
inner join
  (
Select trip_id,max(DriverID) DriverID,max(DriverName) DriverName,max(VRegNo)VRegNo,sum (Sold6KG)Refill6KG,sum (Sold13KG)Refill13KG,
sum (Sold50KG)Refill50KG,sum (Outright6KG)Outright6KG,sum (Outright13KG)Outright13KG,sum (Outright50KG)Outright50KG,
sum (SoldGrill)Grill,sum (SoldBurner)Burner,sum(TransAmount)TransAmount,sum(MpesaAmount)MpesaReceived,sum(InvoicePayment)InvoicePayment,
sum(MpesaPayment)MpesaPayment,sum(BankPayment)BankPayment, sum(ChequePayment)ChequePayment
 from 
(
 select trip_id,d.DriverID,concat(d.FirstName+ ' ',d.LastName) DriverName,VRegNo,Sold6KG,Sold13KG,Sold50KG,Outright6KG,Outright13KG,Outright50KG,
SoldGrill,SoldBurner,TransAmount,MpesaAmount,InvoicePayment,MpesaPayment,BankPayment,ChequePayment from tblMasterSales m
inner join
(
select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Sold6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Sold13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Sold50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) SoldGrill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) SoldBurner
from tblSoldProducts 
where cast(date_added as date)>= cast(getdate() as date)
group by SaleID
) 
p on p.SaleID =m.SaleID
inner join (

select SaleID,
sum(case when PaymentMethod='Bank Payment' then TransAmount else 0 end) BankPayment,
sum(case when PaymentMethod='Cheque Payment' then TransAmount else 0 end) ChequePayment,
sum(case when PaymentMethod='Invoice Payment' then TransAmount else 0 end) InvoicePayment,
sum(case when PaymentMethod='Mpesa Payment' then TransAmount else 0 end) MpesaPayment

from tblPayments 
where cast(DateReceived as date)=cast(getdate() as date)
group by SaleID
) f on f.SaleID=m.SaleID

inner join tblDrivers d on d.DriverID=m.DriverID
where cast(m.date_added as date)>= cast(getdate() as date)
) y
group by trip_id
) z on z.trip_id=l.TripID
inner join
(
select TripID,Unsold6KG,Unsold13KG,Unsold50KG,UnsoldGrills,UnsoldBurners 
from tblTruckunLoading where cast(DateOffLoaded as date)>= cast(getdate() as date)
)x on x.TripID=l.TripID
where cast(DateTimeStamp as date)>= cast(getdate()-1 as date)
END
GO
/****** Object:  StoredProcedure [dbo].[SalesReconUnloading]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE  [dbo].[SalesReconUnloading]
@trip_id nvarchar(20)
AS
BEGIN

	SET NOCOUNT ON;

SELECT l.TripID, z.VRegNo,DriverName,l.VRegNo,(op_bal_6KG+Loaded6KG) Loaded6KG ,(op_bal_13KG+Loaded13KG)Loaded13KG,
(op_bal_50KG+Loaded50KG) Loaded50KG ,
(Refill6KG+Outright6KG) Sold6KG,(Refill13KG+Outright13KG) Sold13KG,(Refill50KG+Outright50KG) Sold50KG
 ,Unsold6KG,Unsold13KG,Unsold50KG,TransAmount,MpesaPayment,BankPayment,ChequePayment, InvoicePayment,MpesaReceived from tblTruckLoading l
 
inner join
  (
Select trip_id,max(DriverID) DriverID,max(DriverName) DriverName,max(VRegNo)VRegNo,sum (Sold6KG)Refill6KG,sum (Sold13KG)Refill13KG,
sum (Sold50KG)Refill50KG,sum (Outright6KG)Outright6KG,sum (Outright13KG)Outright13KG,sum (Outright50KG)Outright50KG,
sum (SoldGrill)Grill,sum (SoldBurner)Burner,sum(TransAmount)TransAmount,sum(MpesaAmount)MpesaReceived,sum(InvoicePayment)InvoicePayment,
sum(MpesaPayment)MpesaPayment,sum(BankPayment)BankPayment, sum(ChequePayment)ChequePayment
 from 
(
 select trip_id,d.DriverID,concat(d.FirstName+ ' ',d.LastName) DriverName,VRegNo,Sold6KG,Sold13KG,Sold50KG,Outright6KG,Outright13KG,Outright50KG,
SoldGrill,SoldBurner,TransAmount,MpesaAmount,InvoicePayment,MpesaPayment,BankPayment,ChequePayment from tblMasterSales m
inner join
(
select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Sold6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Sold13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Sold50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) SoldGrill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) SoldBurner
from tblSoldProducts 
where cast(date_added as date)>= cast(getdate() as date)
group by SaleID
) 
p on p.SaleID =m.SaleID
inner join (

select SaleID,
sum(case when PaymentMethod='Bank Payment' then TransAmount else 0 end) BankPayment,
sum(case when PaymentMethod='Cheque Payment' then TransAmount else 0 end) ChequePayment,
sum(case when PaymentMethod='Invoice Payment' then TransAmount else 0 end) InvoicePayment,
sum(case when PaymentMethod='Mpesa Payment' then TransAmount else 0 end) MpesaPayment

from tblPayments 
where cast(DateReceived as date)=cast(getdate() as date)
group by SaleID
) f on f.SaleID=m.SaleID

inner join tblDrivers d on d.DriverID=m.DriverID
where cast(m.date_added as date)>= cast(getdate() as date)
) y
group by trip_id
) z on z.trip_id=l.TripID
inner join
(
select TripID,Unsold6KG,Unsold13KG,Unsold50KG,UnsoldGrills,UnsoldBurners 
from tblTruckunLoading where cast(DateOffLoaded as date)>= cast(getdate() as date)
)x on x.TripID=l.TripID
where cast(DateTimeStamp as date)>= cast(getdate()-1 as date)
and l.TripID=@trip_id
END
GO
/****** Object:  StoredProcedure [dbo].[SaveDriver]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SaveDriver]
		   @FirstName nvarchar(30),		
		   @LastName nvarchar(30),
		   @PhoneNumber nvarchar(30),
		   @Email nvarchar(50),
		   @Password nvarchar(50),
		    @UserType nvarchar(50)
		 
AS
BEGIN
			INSERT INTO [dbo].[tblDrivers]
           ([FirstName]
		   ,[LastName]		   
		   ,[PhoneNumber]
           ,[Email]           
           ,[Password]
		   ,[Position]
		  )
     VALUES
           (@FirstName		   
		   ,@LastName
		   ,@PhoneNumber
           ,@Email           
           ,@Password
		    ,@UserType
		   )
           
END
GO
/****** Object:  StoredProcedure [dbo].[staging_merger]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[staging_merger]
AS
BEGIN
SET NOCOUNT ON;
select app_sale_order_number,max(customer_id)customer_id,max(item_type)item_type, sum(quantity)quantity,
max(delivery_date)delivery_date,
max(route_name)route_name, max(app_user_name)app_user_name,max(return_mat_code)return_mat_code,
max(storage_location)storage_location,max(unit_price_cylinder)unit_price_cylinder,
max(unit_price_gas)unit_price_gas
from tbl_sales GROUP BY app_sale_order_number ,item_type
END
GO
/****** Object:  StoredProcedure [dbo].[Staging13KGOutright]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Staging13KGOutright]
AS
BEGIN
SET NOCOUNT ON;
SELECT customer_id,'ref-13kg' AS item_type,quantity,TransTime,cus.route,
Concat(FirstName+'',LastName)app_user_name,m.SaleID,'' material_code,VRegNo,
(case when cus.PriceCategory='Special Price' then pricing-1365  else  pricing-1430 end)CylinderPrice,
(case when cus.PriceCategory='Special Price' then  (1365/13)  else (1430/13) end)UnitPricing
FROM tblMasterSales m
inner join 
(
select SaleID,ProductDesc,Quantity,SKUTransAmount, (SKUTransAmount/Quantity)Pricing
from tblSoldProducts where ProductDesc='13KG Outright' and
cast(date_added as date)=cast(getdate()-6 as date)

) r ON r.SaleID=m.SaleID

inner join frm_customer_details cus on m.OutletID=cus.ROWID
inner join tblDrivers v on v.DriverID=m.DriverID
where cast(m.TransTime as date)=cast(getdate()-6 as date)
order by m.VRegNo desc
END
GO
/****** Object:  StoredProcedure [dbo].[Staging13KGRefills]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Staging13KGRefills]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
SELECT customer_id,'ref-13kg' AS item_type,quantity,TransTime,cus.route,Concat(FirstName+'',LastName)app_user_name,m.SaleID,material_code,VRegNo,'0' as CylinderPrice,unitprice as UnitPricing
FROM tblMasterSales m
inner join 
(
select SaleID,RCylinder
  item_type,
  quantity
from tblCylinderReturns
unpivot
(
  quantity
  FOR item_type in (rquantity13kg)
) unpiv
) r ON r.SaleID=m.SaleID
inner join 
(
SELECT SaleID,ProductDesc,SKUTransAmount,
(SKUTransAmount/Quantity)/13 UnitPrice
 FROM tblSoldProducts WHERE SKUTransAmount!=0
 and ProductDesc='13KG Refill'
)d ON d.SaleID=m.SaleID
inner join frm_customer_details cus on m.OutletID=cus.ROWID
inner join tbl_sap_cylinder_brands sap on sap.brand_name=r.item_type
inner join tblDrivers v on v.DriverID=m.DriverID
where quantity >0 and sap.size='13KG'  and cast(m.TransTime as date)=cast(getdate()-6 as date)
order by m.VRegNo desc
END
GO
/****** Object:  StoredProcedure [dbo].[Staging6KGOutright]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Staging6KGOutright]
AS
BEGIN

	SET NOCOUNT ON;
SELECT customer_id,'ref-6kg' AS item_type,quantity,TransTime,cus.route,
Concat(FirstName+'',LastName)app_user_name,m.SaleID,'' material_code,VRegNo,
(case when cus.PriceCategory='Special Price' then pricing-630  else  pricing-660 end)CylinderPrice,
(case when cus.PriceCategory='Special Price' then  630  else (660/6) end)UnitPricing
FROM tblMasterSales m
inner join 
(
select SaleID,ProductDesc,Quantity,SKUTransAmount, (SKUTransAmount/Quantity)Pricing
from tblSoldProducts where ProductDesc='6KG Outright' and
cast(date_added as date)=cast(getdate()-6 as date)

) r ON r.SaleID=m.SaleID

inner join frm_customer_details cus on m.OutletID=cus.ROWID
inner join tblDrivers v on v.DriverID=m.DriverID
where cast(m.TransTime as date)=cast(getdate()-6 as date)
order by m.VRegNo desc
END
GO
/****** Object:  StoredProcedure [dbo].[Staging6KGRefills]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Staging6KGRefills]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT customer_id,'ref-6kg' AS item_type,quantity,TransTime,cus.route,Concat(FirstName+'',LastName)app_user_name,m.SaleID,material_code,VRegNo,'0' as CylinderPrice,unitprice as UnitPricing
FROM tblMasterSales m
inner join 
(
select SaleID,RCylinder
  item_type,
  quantity
from tblCylinderReturns
unpivot
(
  quantity
  FOR item_type in (rquantity6kg)
) unpiv
) r ON r.SaleID=m.SaleID
inner join 
(
SELECT SaleID,ProductDesc,SKUTransAmount,
(SKUTransAmount/Quantity)/6 UnitPrice
 FROM tblSoldProducts WHERE SKUTransAmount!=0
 and ProductDesc='6KG Refill'
)d ON d.SaleID=m.SaleID
inner join frm_customer_details cus on m.OutletID=cus.ROWID
inner join tbl_sap_cylinder_brands sap on sap.brand_name=r.item_type
inner join tblDrivers v on v.DriverID=m.DriverID
where quantity >0 and sap.size='6KG'  and cast(m.TransTime as date)=cast(getdate()-6 as date)
order by m.VRegNo desc



END
GO
/****** Object:  StoredProcedure [dbo].[StagingBurners]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[StagingBurners]
AS
BEGIN
SET NOCOUNT ON;
SELECT customer_id,'Burner' AS item_type,quantity,TransTime,cus.route,
Concat(FirstName+'',LastName)app_user_name,m.SaleID,'PETM00007' material_code,VRegNo,
'0' as CylinderPrice,
Pricing as UnitPricing
FROM tblMasterSales m
inner join 
(
select SaleID,ProductDesc,Quantity,SKUTransAmount, (SKUTransAmount/Quantity)Pricing
from tblSoldProducts where ProductDesc='Burner' and
cast(date_added as date)=cast(getdate()-6 as date)

) r ON r.SaleID=m.SaleID

inner join frm_customer_details cus on m.OutletID=cus.ROWID
inner join tblDrivers v on v.DriverID=m.DriverID
where cast(m.TransTime as date)=cast(getdate()-6 as date)
order by m.VRegNo desc
END
GO
/****** Object:  StoredProcedure [dbo].[StagingGrills]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[StagingGrills]
AS
BEGIN
SET NOCOUNT ON;
SELECT customer_id,'Grill' AS item_type,quantity,TransTime,cus.route,
Concat(FirstName+'',LastName)app_user_name,m.SaleID,'PETM00006' material_code,VRegNo,
'0' as CylinderPrice,
Pricing as UnitPricing
FROM tblMasterSales m
inner join 
(
select SaleID,ProductDesc,Quantity,SKUTransAmount, (SKUTransAmount/Quantity)Pricing
from tblSoldProducts where ProductDesc='Grill' and
cast(date_added as date)=cast(getdate()-6 as date) and quantity>0

) r ON r.SaleID=m.SaleID

inner join frm_customer_details cus on m.OutletID=cus.ROWID
inner join tblDrivers v on v.DriverID=m.DriverID
where cast(m.TransTime as date)=cast(getdate()-6 as date)
order by m.VRegNo desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionLevelRecon]    Script Date: 11/19/2018 3:06:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TransactionLevelRecon]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select d.DriverID,concat(d.FirstName+ ' ',d.LastName) DriverName,
 VRegNo,Refill6KG,Refill13KG,Refill50KG,Outright6KG,Outright13KG,Outright50KG,
Grill,Burner,TransAmount,MpesaAmount as ActualMpesaPayment,BankPayment,InvoicePayment,ChequePayment,Variance,TransTime from tblMasterSales m
inner join
(
select SaleID,
sum(case when ProductDesc='6KG Refill' then Quantity else 0 end) Refill6KG,
sum(case when ProductDesc='13KG Refill' then Quantity else 0 end) Refill13KG,
sum(case when ProductDesc='50KG Refill' then Quantity else 0 end) Refill50KG,
sum(case when ProductDesc='6KG Outright' then Quantity else 0 end) Outright6KG,
sum(case when ProductDesc='13KG Outright' then Quantity else 0 end) Outright13KG,
sum(case when ProductDesc='50KG Outright' then Quantity else 0 end) Outright50KG,
sum(case when ProductDesc='Grill' then Quantity else 0 end) Grill,
sum(case when ProductDesc='Burner' then Quantity else 0 end) Burner
from tblSoldProducts group by SaleID) 
p on p.SaleID =m.SaleID
inner join
(
select SaleID,
sum(case when PaymentMethod='Bank Payment' then TransAmount else 0 end) BankPayment,
sum(case when PaymentMethod='Cheque Payment' then TransAmount else 0 end) ChequePayment,
sum(case when PaymentMethod='Invoice Payment' then TransAmount else 0 end) InvoicePayment,
sum(case when PaymentMethod='Mpesa Payment' then TransAmount else 0 end) MpesaPayment

from tblPayments 
where cast(DateReceived as date)=cast(getdate() as date)
group by SaleID
) f on f.SaleID=p.SaleID
inner join tblDrivers d on d.DriverID=m.DriverID
where m.date_added>= cast(getdate() as date)

END
GO
