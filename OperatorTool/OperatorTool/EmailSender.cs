﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace OperatorToolSpace
{
    public class EmailSender
    {
        public static void SendEmail(string order_id,string fueling_location,DateTime date_approved,
            string vregno,string current_mileage,string quantity,string driver_name)
        {
            string body;
            using (StreamReader reader = new StreamReader("C:/Fueling/fueling.html"))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{order_id}", order_id);
            body = body.Replace("{fueling_location}", fueling_location);
            body = body.Replace("{date_approved}", date_approved.ToShortDateString());
            body = body.Replace("{vregno}", " " + vregno);
            body = body.Replace("{current_mileage}", " " + current_mileage);
            body = body.Replace("{quantity}", " " + quantity);
            body = body.Replace("{driver_name}", driver_name);
            body = body.Replace("{approved_by}", " Collins Arodi");
            string dir = "C:/Fueling/EmailtoSend.html";
            using (StreamWriter write = new StreamWriter(dir))
            {
                write.Write(body);
            }
            var Renderer = new IronPdf.HtmlToPdf();
            var PDF = Renderer.RenderUrlAsPdf(dir);
            string SaveLocation = "C:/Fueling/" + order_id + ".pdf";
            PDF.SaveAs(SaveLocation);
            try
            {
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress("logistics@protoenergy.com");               
                mm.To.Add("carodi@protoenergy.com");
                mm.To.Add("pawinja@protoenergy.com");
                mm.To.Add("swaireri@protoenergy.com");
                mm.CC.Add("jrotich@protoenergy.com");
                mm.To.Add("wjowi@protoenergy.com");
                string salution = "";
                if (fueling_location.Trim()== "Komarock Service Station")
                {
                   mm.To.Add("gakumbiautospare@gmail.com");
                    salution = "Dear Njoroge,\n";
                }
                if (fueling_location.Trim() == "Ongata Rongai")
                {
                   mm.To.Add("ongatarongaitssyd@gmail.com");
                    salution = "Dear Lucy,\n";
                }
                if (fueling_location.Trim() == "Shell Kasarani")
                {
                    mm.To.Add("heykalinvestmentsltd@gmail.com");
                    salution = "Dear Esther,\n";
                }
                if (fueling_location.Trim()== "Shell Kawangware")
                {
                   mm.To.Add("wangaribe@yahoo.com");
                    salution = "Dear Beatrice,\n";
                }
                if (fueling_location.Trim() == "Kabati Plant")
                {
                    mm.To.Add("s.nyongesa@onepet.co.ke");
                    salution = "Dear Stanley,\n";
                }
                if (fueling_location.Trim() == "Ke Plant")
                {
                    mm.To.Add("pellah.starn99@gmail.com");
                    salution = "Dear Stantely,\n";
                }
                mm.Subject = "TRUCK FUELING FOR " + vregno;
                mm.Body = salution+"Kindly fuel the truck in attached order accordingly.";
                mm.Attachments.Add(new Attachment(SaveLocation));
                mm.IsBodyHtml = true;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = "smtp.office365.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "logistics@protoenergy.com";
                NetworkCred.Password = "abc.1234";
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);               
            }
            catch (Exception ex)
            {
               
            }
        }
    }
}