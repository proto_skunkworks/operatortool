﻿using OperatorToolSpace.Models;
using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static OperatorToolSpace.Services.PlanningSQL;

namespace OperatorToolSpace.Controllers
{
    public class PlanningController : Controller
    {
        PlanningSQL _planningsql = new PlanningSQL();
        public ActionResult dashboard()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "dashboard";
                return View(_planningsql.GetPlanningDashBoard());
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult planning()
        {
            return View();
        }

        public ActionResult plan_asset(string date, int id)
        {
            ViewBag.Message = date;
            return View(ViewBag);
        }
        [HttpPost]
        public JsonResult GetPlanningTrucks(string Prefix)
        {
            var vregs = _planningsql.TruckRegsData();

            var names = (from c in vregs
                         where c.vregno.StartsWith(Prefix, StringComparison.OrdinalIgnoreCase)
                         select new { c.vregno });
            return Json(names, JsonRequestBehavior.AllowGet);
        }
        // load vehicle current assignment after vehicle is selected
        [HttpPost]
        public JsonResult GetVehicleAssignments(string VID)
        {
            var SearchResults = _planningsql.GetVehicleData().ToList().Where(s=>s.VID==VID);           
            return Json(SearchResults, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetLastLoaded(int vid)
        {
            var _lastLoaded = _planningsql.GetLastLoadedQuantity(vid).ToList();
            return Json(_lastLoaded, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetTruckRegs()
        {
            var Trucks = _planningsql.GetTruckAssignment().ToList();
            return Json(Trucks, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SavePlanning(TripsPlanningModel data)
        {
            int rs = _planningsql.SaveTripsPlanning(data);
            if (rs == 1)
            {
                return Json(new { success = true, responseText = "Trip Planned Successfully" }, JsonRequestBehavior.AllowGet);

            }
            else if(rs==2)
            {
                return Json(new { success = false, responseText = "You have already planned for this vehicle on the selected date." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Failed to save the planned trip, Please try again." }, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult active_trips()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "active_trips";
                return View(_planningsql.GetActiveTrips().ToList().Where(s => s.status == "Active"));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult unplannedVehicles(DateTime date)
        {
            
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "unplannedVehicles";
                return View(_planningsql.GetUnplannedVehicles(date).ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult allVehicles(DateTime date)
        {

            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "allVehicles";
                return View(_planningsql.GetActiveTrips().ToList().Where(s=>s.planned_date.Date==date));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        public ActionResult plannedtrips(DateTime date)
        {

            ViewBag.ActiveMenu = "plannedtrips";
            var _planned = _planningsql.GetActiveTrips().ToList().Where(s => s.planned_date.Date == date.Date);
            return PartialView("plannedtrips", _planned);
        }
        public ActionResult unplannedAssets(string date,string status)
        {
            DateTime filter= Convert.ToDateTime(date);

            if (Session["Email"] != null && Session["UserID"] != null)
            {
       
                if (status.Trim()=="all")
                {

                    var data=_planningsql.Preplanning().ToList().GroupBy(p => new { p.vregno })
                    .Select(g => g.First())
                    .ToList().Where(s=>s.routes!="");
                    return PartialView("unplannedAssets", data);
                }
                else if(status.Trim() == "0")
                {
                    var data = _planningsql.Preplanning().ToList().GroupBy(p => new { p.vregno })
                    .Select(g => g.First())
                    .ToList().Where(s => (s.routes != "") && (s.status == "0") && (s.date_added == filter));
                    return PartialView("unplannedAssets", data);
                }
                else
                {

                    var data = _planningsql.Preplanning().ToList().GroupBy(p => new { p.vregno })
                    .Select(g => g.First())
                    .ToList().Where(s => (s.routes != "")&&(s.status!="0"));
                    return PartialView("unplannedAssets", data);
                }
              
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        public ActionResult allUnplannedAssets()
        {
                    var data = _planningsql.Preplanning().ToList().GroupBy(p => new { p.vregno })
                .Select(g => g.First())
                .ToList().Where(s => s.routes != "");
                 return PartialView("allUnplannedAssets", data);
            
        }
        public ActionResult unplanned()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "unplanned";
                return View(_planningsql.TripsDownload().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult completed_trips()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "completed_trips";
                return View(_planningsql.GetActiveTrips().ToList().Where(s=>s.status=="Completed")); 
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult pending_trips()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "pending_trips";
                return View(_planningsql.GetActiveTrips().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        public ActionResult all_trips()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "all_trips";
                return View(_planningsql.GetActiveTrips().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

      
        public JsonResult Getpending_trips()
        {
          
                ViewBag.ActiveMenu = "pending_trips";
                var _pending = _planningsql.GetActiveTrips().ToList().Where(s => s.status == "Pending").Take(20);
                
                return Json(_pending, JsonRequestBehavior.AllowGet);
           
        }


        //export planning as excel

        public void ExportAsExcel(string token, DateTime? StartDate, DateTime? EndDate)
        {
            List<TripsDownloadModel> model = new List<TripsDownloadModel>();
            model = _planningsql.TripsDownload();

            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            var ws = wb.Worksheets.Add("Trip ID");
            ws.Row(1).Cell(1).Value = "Truck Reg";
            ws.Row(1).Cell(2).Value = "Type";
            ws.Row(1).Cell(2).Value = "Driver Name";
            ws.Row(1).Cell(2).Value = "PhoneNumber";
            ws.Row(1).Cell(2).Value = "truck_helper";
            ws.Row(1).Cell(2).Value = "route_names";
            ws.Row(1).Cell(3).Value = "6KG";
            ws.Row(1).Cell(4).Value = "13KG";
            ws.Row(1).Cell(5).Value = "50KG";
            ws.Row(1).Cell(9).Value = "Grills";
            ws.Row(1).Cell(10).Value = "Burners";
            ws.Row(1).Cell(12).Value = "Date Planned";
            int index = 2;
            foreach (TripsDownloadModel planning in model)
            {
                ws.Row(index).Cell(1).Value = planning.trip_id;
                ws.Row(index).Cell(2).Value = planning.vtype;
                ws.Row(index).Cell(3).Value = planning.DriverName;
                ws.Row(index).Cell(4).Value = planning.PhoneNumber;
                ws.Row(index).Cell(5).Value = planning.truck_helper;
                ws.Row(index).Cell(6).Value = planning.route_names;
                ws.Row(index).Cell(7).Value = planning.planned_6KG;
                ws.Row(index).Cell(8).Value = planning.planned_13KG;
                ws.Row(index).Cell(9).Value = planning.planned_50KG;
                ws.Row(index).Cell(10).Value = planning.Grills;
                ws.Row(index).Cell(11).Value = planning.Burners;
                ws.Row(index).Cell(12).Value = planning.date_added;                
                index++;
            }

            ws.Range("A1", "L1").Style.Font.Bold = true;
            
            Response.ClearContent();
            Response.AppendCookie(new HttpCookie("fileDownloadToken", token));
            Response.AddHeader("content-disposition", "inline; filename=planning.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();

        }

    }
}
