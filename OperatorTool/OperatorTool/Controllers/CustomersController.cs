﻿using OperatorToolSpace.Models;
using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class CustomersController : Controller
    {
        //
        // GET: /Customers/
        SalesSQLServices _SalesqlService = new SalesSQLServices();
        [HttpGet]
        public ActionResult CustomerList()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "CustomerList";
                return View(_SalesqlService.GetCustomersList().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        [HttpGet]
        public ActionResult CustomerSales()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
               // List.Select(x => new { x.Name, x.Exp }).ToList();
                ViewBag.ActiveMenu = "CustomerSales";
                return View(_SalesqlService.CustomerSaleList().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        

        [HttpPost]
        public JsonResult SalesPerCustomer(string customer_id)
        {
            var _SalesSQL = new SalesSQLServices();
            var _SalesData = _SalesSQL.GetAllSales( DateTime.Now, DateTime.Now.AddMonths(-1)).Where(s => s.OutletID == customer_id).Select(x => new { x.SaleID,x.Truckreg,x.DriverName,x.OutletName,x.Refill_6KG,x.Refill_13KG,x.Refill_50KG,x.Outright_6KG,x.Outright_13KG,x.Outright_50KG,x.DateSold}).ToList();
            return Json(_SalesData, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetCylinderReturns(string SaleID)
        {
            var _SalesSQL = new SalesSQLServices();
            var _SalesData = _SalesSQL.GetCylinderReturns(SaleID).ToList();
                return Json(_SalesData, JsonRequestBehavior.AllowGet);

        }
        public ActionResult CustomerEdit(int id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "CustomerEdit";
                CustomersModel _EditCustomer = _SalesqlService.GetCustomersList().Find(uid => uid.OutletID == id);
                return View(_EditCustomer);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult customers()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
               
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        [HttpPost]

        public JsonResult ChangePricing(string OutletID, string PriceCategory)
        {
            int rs = _SalesqlService.ChangePricing(OutletID, PriceCategory);
            if (rs != 1)
            {
                return Json(new { success = true, responseText = "Price Changes Failed!" }, JsonRequestBehavior.AllowGet);              
            }
            else
            {
                return Json(new { success = true, responseText = "Prices Updated!" }, JsonRequestBehavior.AllowGet);
              
            }


        }

    }
}
