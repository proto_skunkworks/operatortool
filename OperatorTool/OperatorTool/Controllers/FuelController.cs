﻿using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class FuelController : Controller
    {
        ////
        //// GET: /Fuel/
        Fuelsql _fuelsql = new Fuelsql();
    
        public ActionResult fuelrequests()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "fuelrequests";
                return View(_fuelsql.GetFuelRequests().ToList()); // redirecting
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
    }
}
