﻿using Newtonsoft.Json;
using OperatorToolSpace.Models;
using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class SalesController : Controller
    {
        SalesSQLServices _SalesqlService = new SalesSQLServices();

        public ActionResult PriceList()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "PriceList";               
                return View(_SalesqlService.GetRoutePrices().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

       
        [HttpGet]
        public ActionResult EditPrices(int id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "EditPrices";
                EditPricesModel _EditPrices = _SalesqlService.ViewPriceList().Find(uid => uid.RouteID == id);
              
                return View(_EditPrices);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        [HttpGet]
        public ActionResult AcceptedCylinders()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "AcceptedCylinders";               
                return View(_SalesqlService.GetAcceptedCylinders().ToList());                
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult _editSale(string row_id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "_editSale";
                EditSalesModel _EditSale = _SalesqlService.GetProductSales("").Find(id => id.RowID == row_id);
                
                return PartialView("_editSale", _EditSale);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        [HttpPost]
        public JsonResult AcceptedCylinders(bool status, int CylinderID)
        {
            int rs = _SalesqlService.UpdateAcceptedCylinders(status, CylinderID);
            if (rs != 1)
            {
                return Json(new { success = true, responseText = "Update Failed!" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = true, responseText = "Cylinder Updated." }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpPost]
        public ActionResult UpdatePrices(EditPricesModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                   // HttpPostedFileBase file = Request.Files["ImageData"];
                    _SalesqlService.UpdateRoutePrices(model);
                    TempData["Success"] = "New Prices for "+model.RName +" upated.";
                    return RedirectToAction("PriceList");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Fail"] = "Fail";
                    return View("PriceList", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("PriceList");
            }

        }


        [HttpGet]
        public ActionResult Sales(DateTime? StartDate, DateTime? EndDate)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "Sales";
                return View(_SalesqlService.GetAllSales(StartDate,EndDate).ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        [HttpGet]
        public ActionResult soldproducts(DateTime? startDate,DateTime? endDate)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "soldproducts";
                return View(_SalesqlService.GetSoldProducts(startDate,endDate).ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        [HttpGet]
        public ActionResult ViewSale(string id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "ViewSale";
                return View(_SalesqlService.GetProductSales(id).ToList());

            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        public string getTerritorySummary()
        {
           
            var _data = _SalesqlService.TerritorySummaryGraph();
            
            var jsonData = JsonConvert.SerializeObject(_data);
            return jsonData;
        }




        public void ExportAsExcel(string token, DateTime? StartDate, DateTime? EndDate)
        {

            List<SalesDataModels> model = new List<SalesDataModels>();
            model = _SalesqlService.GetAllSales(StartDate, EndDate);

            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            var ws = wb.Worksheets.Add("Sales");
            ws.Row(1).Cell(1).Value = "Driver Name";
            ws.Row(1).Cell(2).Value = "Vehicle Reg";
            ws.Row(1).Cell(3).Value = "Refill 6KG";
            ws.Row(1).Cell(4).Value = "Refill 13KG";
            ws.Row(1).Cell(5).Value = "Refill 50KG";
            ws.Row(1).Cell(6).Value = "Outright 6KG";
            ws.Row(1).Cell(7).Value = "Outright 13KG";
            ws.Row(1).Cell(8).Value = "Outright 50KG";
            ws.Row(1).Cell(9).Value = "Grills";
            ws.Row(1).Cell(10).Value = "Burners";
            ws.Row(1).Cell(11).Value = "TransAmount";
            ws.Row(1).Cell(12).Value = "Trans Time";
            int index = 2;
            foreach (SalesDataModels driver in model)
            {
                ws.Row(index).Cell(1).Value = driver.DriverName;
                ws.Row(index).Cell(1).Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                ws.Row(index).Cell(2).Value = driver.Truckreg;
                ws.Row(index).Cell(2).Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                ws.Row(index).Cell(3).Value = driver.Refill_6KG;
                ws.Row(index).Cell(4).Value = driver.Refill_13KG;
                ws.Row(index).Cell(5).Value = driver.Refill_50KG;
                ws.Row(index).Cell(6).Value = driver.Outright_6KG;
                ws.Row(index).Cell(7).Value = driver.Outright_13KG;
                ws.Row(index).Cell(8).Value = driver.Outright_50KG;
                ws.Row(index).Cell(9).Value = driver.Grills;
                ws.Row(index).Cell(10).Value = driver.Burners;
                ws.Row(index).Cell(11).Value = driver.TranAmount;
                ws.Row(index).Cell(12).Value = driver.DateSold;
                // ws.Row(index).Cell(6).Value = driver.Tran;
                index++;
            }

            ws.Range("A1", "F1").Style.Font.Bold = true;



            Response.ClearContent();
            Response.AppendCookie(new HttpCookie("fileDownloadToken", token));
            Response.AddHeader("content-disposition", "inline; filename=Sales.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();
         
        }

        //Downlaod sold products

        public void ExportSldProducts(string token, DateTime? StartDate, DateTime? EndDate)
        {

            List<SoldProductsModel> model = new List<SoldProductsModel>();
            model = _SalesqlService.GetSoldProducts(StartDate, EndDate);

            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            var ws = wb.Worksheets.Add("SoldProducts");
            ws.Row(1).Cell(1).Value = "Vehicle Reg";
            ws.Row(1).Cell(2).Value = "Driver Name";
            ws.Row(1).Cell(3).Value = "Customer Name";
            ws.Row(1).Cell(4).Value = "Route";
            ws.Row(1).Cell(5).Value = "Product";
            ws.Row(1).Cell(6).Value = "Quantity";
            ws.Row(1).Cell(7).Value = "SKU Amount";
            ws.Row(1).Cell(8).Value = "Date Sold";           
            int index = 2;
            foreach (SoldProductsModel driver in model)
            {
                ws.Row(index).Cell(1).Value = driver.vregno;
                ws.Row(index).Cell(1).Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                ws.Row(index).Cell(2).Value = driver.driver_name;
                ws.Row(index).Cell(2).Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                ws.Row(index).Cell(3).Value = driver.outletname;
                ws.Row(index).Cell(4).Value = driver.route_name;
                ws.Row(index).Cell(5).Value = driver.productdesc;
                ws.Row(index).Cell(6).Value = driver.quantity;
                ws.Row(index).Cell(7).Value = driver.sku_trans_amount;
                ws.Row(index).Cell(8).Value = driver.date_added;               
                index++;
            }

            ws.Range("A1", "F1").Style.Font.Bold = true;



            Response.ClearContent();
            Response.AppendCookie(new HttpCookie("fileDownloadToken", token));
            Response.AddHeader("content-disposition", "inline; filename=soldproducts.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();
        }

        [HttpPost]
        public JsonResult AddPayment(AddPaymentModel data)
        {
            int rs = _SalesqlService.AddPayment(data);
            if (rs == 1)
            {
                return Json(new { success = true, responseText = "Payment Added Successfully" }, JsonRequestBehavior.AllowGet);

            }
            else if (rs == 2)
            {
                return Json(new { success = false, responseText = "Failed to Add Payment" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Failed to save payment, Please try again." }, JsonRequestBehavior.AllowGet);

            }
        }

    }
}
