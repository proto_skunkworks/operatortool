﻿using OperatorToolSpace.Models;
using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class TrucksController : Controller
    {
        SalesSQLServices _SalesqlService = new SalesSQLServices();
        //
        // GET: /Trucks/

        [HttpGet]
        public ActionResult TruckWiseSale()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "TruckWiseSale";
                return View(_SalesqlService.TruckWiseSales().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

    
        public ActionResult Sales(string id)
        {
            DateTime s = DateTime.Now;
            DateTime e = DateTime.Now;
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "Sales";               
                return View(_SalesqlService.GetAllSales(s,e).Where(p => p.DriverID == id));               
              
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }


        [HttpGet]
        public ActionResult viewpayments(string id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "viewpayments";
                return View(_SalesqlService.GetSalesPayments(id).ToList());

            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult AddPayment(string row_id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
               // ViewBag.ActiveMenu = "_editSale";
               // EditSalesModel _EditSale = _SalesqlService.GetProductSales("").Find(id => id.RowID == row_id);

                return PartialView("AddPayment");
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
    }
}
