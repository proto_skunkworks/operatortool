﻿using OperatorToolSpace.Models;
using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class bulkController : Controller
    {
        SalesSQLServices _sqlHelper = new SalesSQLServices();
        public ActionResult bulkorders()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "bulkorders";
                return View(_sqlHelper.GetBulkOrders().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult pendingplanning()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "pendingplanning";
                return View(_sqlHelper.GetPlanningOrders().ToList());
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult pendingdelivery()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "pendingdelivery";
                return View(_sqlHelper.getpendingOrders().ToList().Where(d=>d.status== "Planned"));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult deliveredorders()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "deliveredorders";
                return View(_sqlHelper.getpendingOrders().Where(d => d.status == "Delivered"));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

       

        [HttpGet]
        public JsonResult GetBulkTruckRegs()
        {
            var Trucks = _sqlHelper.GetBulkTrucks().ToList();
            return Json(Trucks, JsonRequestBehavior.AllowGet);
        }
       
       
        public JsonResult approveorder(OrderApproval order)
        {
            int rs = _sqlHelper.ApproveBulkOrder(order);
            if (rs == 1)
            {
                return Json(new { success = true, Message = "Order Approved Successfully. Awaiting Delivery." });

            }
            else
            {
                return Json(new { success = true, Message = "Failed to execute your request." });
            }
        }

        public JsonResult saveplanning(DeliveryPlan order)
        {
            int rs = _sqlHelper.UpdateDeliveryPlan(order);
            if (rs == 1)
            {
                return Json(new { success = true, Message = "Planned Successfully. Driver Notified." });

            }
            else
            {
                return Json(new { success = true, Message = "Failed to execute your request." });
            }
        }
    }
}
