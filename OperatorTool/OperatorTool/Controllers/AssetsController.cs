﻿using OperatorToolSpace.Models;
using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class AssetsController : Controller
    {
        Fuelsql _fuelsql = new Fuelsql();
        VehicleSql _sqlHelper = new VehicleSql();
        public ActionResult dashboard()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "dashboard";
                return View(_sqlHelper.GetFuelingDashBoard());
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult Trucks()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "Trucks";
                return View(_sqlHelper.GetVehicleList().ToList()); // redirecting to all vehicles
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
       
    

        [HttpGet]
        public ActionResult editVehicle(int id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "editVehicle";
                VehicleDataModel _VData = _sqlHelper.GetVehicleList().Find(uid => uid.VID == id);
                //_EditUserModel.sex = _SystemTools.GetGender();
                return View(_VData);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult addasset()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "addasset";
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        //SaveGarageSchedule

        //[HttpPost]
        //public ActionResult SaveGarageSchedule(WorkshopDataModel schedule)
        //{
        //    //int rs = 1;
        //    // int rs = _sqlHelper.SaveGarageSchedule(schedule);
        //    //if (rs != 1)
        //    //{
        //    //    return Json(new { success = true, responseText = "Update Failed!" }, JsonRequestBehavior.AllowGet);

        //    //}
        //    //else
        //    //{
        //    //    return Json(new { success = true, responseText = "Cylinder Updated." }, JsonRequestBehavior.AllowGet);

        //    //}

        //}


        //Fuel Requests


        public ActionResult fuelrequests()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "fuelrequests";
                return View(_fuelsql.GetFuelRequests().ToList()); // redirecting
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult approved_requests()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "approved_requests";
                return View(_fuelsql.GetFuelRequests().Where(s => s.request_status == "Approved"));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult pending_requests()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "pending_requests";
                return View(_fuelsql.GetFuelRequests().Where(s => s.request_status == "Pending"));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult rejected_requests()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "rejected_requests";
                return View(_fuelsql.GetFuelRequests().ToList().Where(s=>s.request_status=="Rejected")); 
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult closed_requests()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "closed_requests";
                return View(_fuelsql.GetFuelRequests().Where(s => s.request_status == "Closed"));
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult fuelinghistory(int id)
        {
           
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "fuelinghistory";
                FuelRequestsModel _fuelingHistory = _fuelsql.GetFuelRequests().Find(uid => uid.order_id == id);
                return View(_fuelingHistory);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }


        [HttpPost]
        public ActionResult ApproveFueling(string order_id,string approved_quantity)
        {

            int rs = _fuelsql.ApproveFueling(order_id,approved_quantity,Session["UserID"].ToString());

            if (rs == 1)
            {

                return Json(new { success = true, responseText = "Fueling Approved!" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = true, responseText = "Failed to Approved fuel!" }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public ActionResult RejectFueling(string order_id)
        {

            int rs = _fuelsql.RejectFueling(order_id);

            if (rs == 1)
            {

                return Json(new { success = true, responseText = "Fueling Rejected!" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = true, responseText = "Failed to Reject fueling,Try Again Later!" }, JsonRequestBehavior.AllowGet);

            }

        }
    }
}
