﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using OperatorToolSpace.Services;
using OperatorToolSpace.Models;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace OperatorToolSpace.Controllers
{
    public class DriversController : Controller
    {
        //
        // GET: /UsersManagement/
        DriversService _DriverService = new DriversService();
        SystemTools _SystemTools = new SystemTools();

        public ActionResult AddDriver()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "AddDriver";
                AddDriverModel _AddUserModel = new AddDriverModel();
                _AddUserModel.Position = _SystemTools.GetGender();
                return View(_AddUserModel);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

            
        }

        [HttpPost]
        public JsonResult IsDriverExists(string PhoneNumber)
        {
            return Json(CheckUserEmail(PhoneNumber));
        }

        public bool CheckUserEmail(string PhoneNumber)
        {
            var result = _DriverService.GetDriversList().Find(Rdr => Rdr.PhoneNumber == PhoneNumber);

            bool status;

            if (result != null)
            {
                //Already registered  
                status = false;
            }
            else
            {
                //Available to use  
                status = true;
            }

            return status;
        }

        [HttpPost]
        public ActionResult CreateDriver(AddDriverModel model)
        {
            if (ModelState.IsValid)
            {
               
                try
                {
                    _DriverService.CreateDriver(model);
                    TempData["Success"] = model.FirstName +" Successfully added as "+model.UserType;
                    return RedirectToAction("AddDriver");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Success"] = "Fail to Add Try again";
                    return View("AddDriver", model);
                }
            }
            else
            {
                TempData["Success"] = "Fail to Add Try again";
                return RedirectToAction("AddDriver");
            }

        }

        [HttpPost]
        public JsonResult GetTruckRegs(string Prefix)
        {
            var Trucks = _DriverService.TruckRegsData().ToList();
            return Json(Trucks, JsonRequestBehavior.AllowGet);
        }
       [HttpPost]
       
        public JsonResult AssignTruck(PlanningDataModel planning)
        {
            int rs = _DriverService.AssignVehicle(planning);
            if (rs == 1)
            {
                return Json(new { success = true, responseText = "Driver Assigned Vehicles Successfully." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = true, responseText = "Failed to Assign Vehicle" }, JsonRequestBehavior.AllowGet);

            }


        }

        [HttpPost]
        public JsonResult GetRoutes(string Prefix)
        {
            var Trucks = _DriverService.GetRoutes().ToList();
            return Json(Trucks, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetContainers()
        {
            var Containers = _DriverService.GetContainers().ToList();
            return Json(Containers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTruckHelper(string Prefix)
        {
            var Trucks = _DriverService.GetTruckHelper().ToList();
            return Json(Trucks, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public JsonResult AssignRoutes(string[] Routes, string DriverID)
        {
            int rs = _DriverService.AssignRoutes(Routes, DriverID);
            if (rs == 1)
            {
                return Json(new { success = true, responseText = "Assignment Failed!" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = true, responseText = "Rooutes Assigned Successfully!" }, JsonRequestBehavior.AllowGet);

            }

        }
        public JsonResult RevokeVehicle(string DriverID)
        {
            int rs = _DriverService.DeassignVehicle(DriverID);
            if (rs == 1)
            {
                return Json(new { success = true, responseText = "Vehicle Divested Successfully!" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = true, responseText = "Failed to Divest vehicle. Try Again!" }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult DriversList()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "DriversList";
                return View(_DriverService.GetDriversList().ToList()); // redirecting to all Users List
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            
        }

        public ActionResult leave_off()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "leave_off";
                return View(_DriverService.GetDriversList().ToList()); // redirecting to all Users List
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        [HttpPost]
        public ActionResult DeactivateActivateUser(DriverListModel model)
        {
            if (ModelState.IsValid)
            {
                
                try
                {
                    _DriverService.DeactivateActivateUser(model);
                    TempData["Success"] = "Success";
                    return RedirectToAction("UsersList");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Fail"] = "Fail";
                    return View("UsersList", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("UsersList");
            }

        }
        public void ExportAsExcel(string token)
        {

            List<DriverListModel> model = new List<DriverListModel>();
            model = _DriverService.GetDriversList();

            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook();
            var ws = wb.Worksheets.Add("Drivers");
            ws.Row(1).Cell(1).Value = "First Name";
            ws.Row(1).Cell(2).Value = "Phone Number";
            ws.Row(1).Cell(3).Value = "Truck reg";
            ws.Row(1).Cell(4).Value = "Current Route";
            ws.Row(1).Cell(5).Value = "";
            ws.Row(1).Cell(6).Value = "";
            
            int index = 2;
            foreach (DriverListModel driver in model)
            {
                ws.Row(index).Cell(1).Value = driver.FirstName;
                ws.Row(index).Cell(1).Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                ws.Row(index).Cell(2).Value =   driver.LastName;
                ws.Row(index).Cell(2).Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                ws.Row(index).Cell(3).Value = driver.PhoneNumber;
                ws.Row(index).Cell(4).Value = driver.Truckreg;
                ws.Row(index).Cell(5).Value = driver.CurrentRoute;
                ws.Row(index).Cell(6).Value = "";
                ws.Row(index).Cell(6).Value = "";
                index++;
            }

            ws.Range("A1", "F1").Style.Font.Bold = true;



            Response.ClearContent();
            Response.AppendCookie(new HttpCookie("fileDownloadToken", token));
            Response.AddHeader("content-disposition", "inline; filename=DriverPlanning.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();            
            //var gv = new GridView();
            //gv.DataSource = _DriverService.GetDriversList();
            //gv.DataBind();

            //Response.ClearContent();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment; filename=DriverPlanning.xls");
            //Response.ContentType = "application/ms-excel";

          
            //Response.Charset = "";
            //StringWriter objStringWriter = new StringWriter();
            //HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);

            //gv.RenderControl(objHtmlTextWriter);

            //Response.Output.Write(objStringWriter.ToString());
            //Response.Flush();
            //Response.End();

            //return View("MpesaRecon");

        }
        public ActionResult ActiveUsers()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "ActiveUsers";
                return View(_DriverService.ActiveUsersList().ToList()); // redirecting to all Activve Users List
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            
        }

        [HttpPost]
        public ActionResult ActiveUsersPageDeactivateUser(DriverListModel model)
        {
            if (ModelState.IsValid)
            {
                
                try
                {
                    _DriverService.DeactivateActivateUser(model);
                    TempData["Success"] = "Success";
                    return RedirectToAction("ActiveUsers");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Fail"] = "Fail";
                    return View("ActiveUsers", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("ActiveUsers");
            }

        }

        public ActionResult InActiveUsers()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "InActiveUsers";
                return View(_DriverService.InActiveUsersList().ToList()); // redirecting to all InActive Users List
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            
        }

        [HttpPost]
        public ActionResult InActiveUsersPageActivateUser(DriverListModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    _DriverService.DeactivateActivateUser(model);
                    TempData["Success"] = "Success";
                    return RedirectToAction("InActiveUsers");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Fail"] = "Fail";
                    return View("InActiveUsers", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("InActiveUsers");
            }

        }

        [HttpPost]
        public ActionResult DeassignTruck(DriverListModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    _DriverService.RemoveDriver(model);
                    TempData["Success"] = "Driver Removed";
                    return RedirectToAction("DriversList");
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Success"] = "Fail";
                    return View("DriversList", model);
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("DriversList");
            }

        }

        public ActionResult ArchivedUsers()
        {
            return View(_DriverService.ArchivedUsersList().ToList()); // redirecting to all Removed Users List
        }

        [HttpGet]
        public ActionResult EditUser(int id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "AddUser";
                EditUserModel _EditUserModel = _DriverService.ViewUserDetailsList().Find(uid => uid.UserID == id);
                //_EditUserModel.sex = _SystemTools.GetGender();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            
        }

        [HttpPost]
        public ActionResult UpdateDriver(EditDriverModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    HttpPostedFileBase file = Request.Files["ImageData"];
                    _DriverService.UpdateUser(model);
                    TempData["Success"] = "Success";                   
                    return RedirectToAction("EditDriver", new { id = model.DriverID });
                }
                catch (Exception e)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("", e.Message);
                    TempData["Fail"] = "Fail";
                    return RedirectToAction("EditDriver", new { id = model.DriverID });
                }
            }
            else
            {
                TempData["ModelsError"] = "Error";
                return RedirectToAction("EditDriver", new { id = model.DriverID });
            }

        }

        [HttpGet]
        public ActionResult EditDriver(int id)
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "EditDriver";
                EditDriverModel _EditDriver = _DriverService.GetEditDriver().Find(uid => uid.DriverID == id);
                return View(_EditDriver);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        
    }
}
