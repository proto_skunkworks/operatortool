﻿using OperatorToolSpace.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperatorToolSpace.Controllers
{
    public class workshopController : Controller
    {
        WorkshopService _sqlHelper = new WorkshopService();
        [HttpGet]
        public ActionResult dashboard()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
             
              
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult underrepair()
        {
            if (Session["Email"] != null && Session["UserID"] != null)
            {
                ViewBag.ActiveMenu = "underrepair";
                return View(_sqlHelper.GetUnderRepair().ToList()); // redirecting to all vehicles
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }

    }
}
