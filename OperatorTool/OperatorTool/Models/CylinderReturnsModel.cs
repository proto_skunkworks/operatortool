﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class SoldProductsModel
    {
        public string vregno { get; set; }
        public string outletname { get; set; }
        public string driver_name { get; set; }
        public string route_name { get; set; }
        public string productdesc { get; set; }
        public int quantity { get; set; }
        public int sku_trans_amount { get; set; }
        public DateTime date_added { get; set; }
    }
}