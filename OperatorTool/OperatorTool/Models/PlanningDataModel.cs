﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class TripsPlanningModel
    {
          public string vid { get; set; }
          public string Category { get; set; }
        public string destination { get; set; }
        public DateTime planned_date { get; set; }
        public string  kg6_refill { get; set; }
        public string  kg13_refill { get; set; }
        public string burners { get; set; }
        public string  grills { get; set; }
        public string kg50_refill { get; set; }
    }

    public class AddPaymentModel
    {
        public string saleid { get; set; }
        public string paymentMethod { get; set; }
        public string datePaid { get; set; }
        public string paidAmount { get; set; }
        public string phoneNumber { get; set; }
    }

    public class ActiveTripsModel
    {
        public string vregno { get; set; }
        public string driver_name { get; set; }
        public string Category { get; set; }
        public string destination { get; set; }
        public DateTime planned_date { get; set; }
        public string  trip_id { get; set; }
        public string truck_helper { get; set; }
        public string time_taken { get; set; }
        public string routes { get; set; }
        public string driver_phone { get; set; }
        public string trip_cost { get; set; }
        public string status { get; set; }
    }
    public class PlanningDashBoard
    {
        public string Active { get; set; }
        public string Completed { get; set; }
        public string Pending { get; set; }
        public string unplanned { get; set; }
        public string all_trips { get; set; }

    }
    public class TripsDownloadModel
    {
        public string vregno { get; set; }
        public string trip_id { get; set; }
        public string vtype { get; set; }
        public string DriverName { get; set; }
        public string PhoneNumber { get; set; }
        public string truck_helper { get; set; }
        public string route_names { get; set; }
        public string planned_6KG { get; set; }
        public string planned_13KG { get; set; }
        public string planned_50KG { get; set; }
        public string Burners { get; set; }
        public string Grills { get; set; }
        public DateTime date_added { get; set; }


    }
    public class TripsPreplanning
    {
        public string vregno { get; set; }
        public int vid { get; set; }
        public string driver_name { get; set; }
        public string phone_number { get; set; }
        public string truck_helper { get; set; }
        public string routes { get; set; }
        public string status { get; set; }
        public Nullable<DateTime> date_added { get; set; }
    }

    public class UnloadedVehicles
    {
        public int vid { get; set; }
        public string vregno { get; set; }
        public string driver_name { get; set; }
        public string truckhelper { get; set; }
        public string currentroutes { get; set; }
        public string phone_number { get; set; }
        public int driver_id { get; set; }
    }
}