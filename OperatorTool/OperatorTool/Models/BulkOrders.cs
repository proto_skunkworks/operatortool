﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class BulkOrdersModel
    {
        public string customer_id { get; set; }
        public string customer_name { get; set; }
        public int price { get; set; }
        public int driver_id { get; set; }
        public int requested_quantity { get; set; }
        public int approved_quantity { get; set; }
        public string order_id { get; set; }
        public string driver_name { get; set; }
        public string vregno { get; set; }
        public string status { get; set; }
        public DateTime actual_delivery_date { get; set; }
        public DateTime expected_delivery_date { get; set; }
    }
    public class OrderApproval
    {
        public int app_quantity { get; set; }
        public string order_id { get; set; }
        public string message { get; set; }
    }

    public class DeliveryPlan
    {
        public int driver_id { get; set; }
        public string order_id { get; set; }
        public int vid { get; set; }

    }
}