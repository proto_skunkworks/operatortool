﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace OperatorToolSpace.Models
{
    public class DriversModel
    {
    }
    public class AddDriverModel
    {
        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserType { get; set; }
        [Required]
        [Display(Name = "Email")]

      
        public string Email { get; set; }
        [Required]
        [Display(Name = "PhoneNumber")]
        [Remote("IsDriverExists", "Drivers", HttpMethod = "POST", ErrorMessage = "Phone Number already registered!")]
        public string PhoneNumber { get; set; }       

        [Required]
        [Display(Name = "Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password confirmation must match password.")]
        public string CPassword { get; set; }

        public IEnumerable<StaffTypeModel> Position { get; set; }

    }

    public class StaffTypeModel
    {
        public string StaffTypeID { get; set; }
        public string Description { get; set; }
    }

    public class DriverListModel
    {
        
        public int DriverID { get; set; }
        public string VID { get; set; }
        public string NationalID { get; set; }
        public string MiddleName { get; set; }
        public string truck_helper { get; set; }
        public string PhoneNumber { get; set; }
        public string Truckreg { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }       
        public string CurrentRoute { get; set; }
        public string CurrentTill { get; set; }

        public string Position { get; set; }

        public string Password { get; set; }
    }


    public class EditDriverModel
    {
        public int DriverID { get; set; }        
        public string PhoneNumber { get; set; }
        public string Truckreg { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }       
        public string CurrentRoute { get; set; }
        public string CurrentTill { get; set; }
        public string Password { get; set; }
        public string Position { get; set; }
    }

    public class EditUserModel
    {
        public int UserID { get; set; }

        [Required]
        [Display(Name = "Firstname")]
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Position { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password confirmation must match password.")]
        public string CPassword { get; set; }
        public byte[] Image { get; set; }

        public IEnumerable<StaffTypeModel> sex { get; set; }

    }
   

    public class ContainersDataModel
    {
        public string ContainerName { get; set; }
        public int id { get; set; }
    }
    public class RoutesModel
    {
        public string RouteName { get; set; }
        public int id { get; set; }
    }
    public class TruckHelperModel
    {
        public int thelper_id { get; set; }
        public string truck_helper { get; set; }
       
    }

    public class PlanningDataModel
    {
        public string  DriverID { get; set; }
        public string Truckreg { get; set; }
        public string truck_helper { get; set; }
}

    public class FuelRequestsModel
    {
        public int order_id { get; set; }
        public string driver_id { get; set; }
        public string driver_name { get; set; }
        public string date_requested { get; set; }
        public string request_mileage { get; set; }
        public string request_quantity { get; set; }
        public string request_vregno { get; set; }
        public string request_status { get; set; }
        public string approved_quantity { get; set; }
        public string fueling_mileage { get; set; }
        public string fueling_quantity { get; set; }
        public string fueling_station { get; set; }

        public bool  authorization { get; set; }
    }


}