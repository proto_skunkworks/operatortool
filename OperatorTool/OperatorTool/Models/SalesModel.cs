﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class EditPricesModel
    {
        public string RName { get; set; }
        public string PriceCategory { get; set; }
        public int RouteID { get; set; }
        public string Refill6KG { get; set; }
        public string Refill13KG { get; set; }
        public string Refill50KG { get; set; }
        public string Outright6KG { get; set; }
        public string Outright13KG { get; set; }

        public string Outright50KG { get; set; }

        public string PriceGrill { get; set; }

        public string PriceBurner { get; set; }
        public int Status { get; set; }

    }
    public class TruckregsModel
    {
        public string Truckreg { get; set; }
        public int id { get; set; }

    }


    public class AcceptedCylinderModel
    {
        public string BrandName { get; set; }
        public int id { get; set; }
        public string Category { get; set; }
        public bool Status { get; set; }

    }

    public class ModifyAcceptedCylindersModel
    {
        public int id { get; set; }

        public bool Selected { get; set; }

    }

    public class SalesDataModels
    {
        
        public string SaleID { get; set; }
        public string OutletID { get; set; }
        public string DriverID { get; set; }
        public string OutletName { get; set; }
        public string Truckreg { get; set; }
        public string DriverName { get; set; }
        public int Refill_6KG { get; set; }
        public int Refill_13KG { get; set; }
        public int Refill_50KG { get; set; }
        public int Outright_6KG { get; set; }
        public int Outright_13KG { get; set; }
        public int Outright_50KG { get; set; }
        public int Burners { get; set; }
        public int Grills { get; set; }
        public DateTime DateSold { get; set; }
        public string Route { get; set; }
        public int TotalReturned { get; set; }
        public int TranAmount { get; set; }
        public int MpesaAmount { get; set; }
        public int Variance { get; set; }
        public string comment { get; set; }
    }


    public class EditSalesModel
    {

        public string RowID { get; set; }
        public string SaleID { get; set; }
        public string DriverName { get; set; }
        public string OutletName { get; set; }
        public string Truckreg { get; set; }
        public string ProductDesc { get; set; }
        public int Quantity { get; set; }
        public int SKUTransAmount { get; set; }
        public DateTime DateSold { get; set; }
    }

    public class TruckWiseSalesModel
    {
        public string DriverName { get; set; }
        public string Truckreg { get; set; }
        public string DriverID { get; set; }
        public int Refill_6KG { get; set; }
        public int Refill_13KG { get; set; }
        public int Refill_50KG { get; set; }
        public int Outright_6KG { get; set; }
        public int Outright_13KG { get; set; }
        public int Outright_50KG { get; set; }
        public int Grills { get; set; }
        public int Burners { get; set; }
        public int TotalSold { get; set; }
        public int TotalRevenue { get; set; }

    }

    public class SaleDetailsModel
    {
        public IEnumerable<EditSalesModel> SoldProducts { get; set; }
        public IEnumerable<PaymentsModel> Payments { get; set; }

    }


    public class PaymentsModel
    {
        public string PaymentMethod { get; set; }
        public int AmountPaid { get; set; }
        public string PhoneNumber { get; set; }
        public string TransID { get; set; }

        public string RowID { get; set; }
        public string SaleID { get; set; }

        public DateTime DateReceived { get; set; }

    }

    public class TerritorySummaryGraph
    {
        public string TerritoryName { get; set; }
        public int TerritorySales { get; set; }
    }

    
    public class VehicleDataModel
    {
        public string VRegNo { get; set; }
        public  int VID { get; set; }
        public string CurrentDriver { get; set; }

        public string AssetType { get; set; }
        public int Capacity { get; set; }
        public string Availability { get; set; }       
    }


  public class customerslistmodel
    {

    }
}