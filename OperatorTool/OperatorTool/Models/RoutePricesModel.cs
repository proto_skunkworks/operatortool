﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class RoutePricesModel
    {
        public string RName { get; set; }
        public string PriceCategory { get; set; }
        public string RouteID { get; set; }
        public string Refill6KG { get; set; }
        public string Refill13KG { get; set; }
        public string Refill50KG { get; set; }
        public string Outright6KG { get; set; }
        public string Outright13KG { get; set; }

        public string Outright50KG { get; set; }

        public string PriceGrill { get; set; }

        public string PriceBurner { get; set; }
        public int Status { get; set; }
    }
}