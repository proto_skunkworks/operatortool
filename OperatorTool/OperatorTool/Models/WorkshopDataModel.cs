﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class WorkshopDataModel
    {
            public string drivername { get; set; }
            public string VRegNo { get; set; }
            public string repairType { get; set; }
            public string description { get; set; }
            public string MoreInfo { get; set; }
            public string date_checkedin { get; set; }
            public string date_checkedout { get; set; }
            public DateTime date_requested { get; set; }
        
    }
}