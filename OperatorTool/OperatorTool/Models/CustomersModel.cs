﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Models
{
    public class CustomersModel
    {
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public string Route { get; set; }
        public string Area { get; set; }
        public string PriceCategory { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string SelectedStatus { set; get; }

    }

    


    public class EditOutletModel
    {
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public string Route { get; set; }
        public string Area { get; set; }
        public string PriceCategory { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }

    }

    
}