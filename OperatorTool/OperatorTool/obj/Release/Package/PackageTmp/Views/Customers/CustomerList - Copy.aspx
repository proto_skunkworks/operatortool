﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.CustomersModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Drivers List" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/jquery.min.js"></script>
<link href="../../Scripts/bootstrap.min.css" rel="stylesheet" />
<script src="../../Scripts/bootstrap.min.js"></script>
    <!-- row -->
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>
    <script type="text/javascript">
    function ExportAsExcel() {           
    
   $.ajax(
            {

                url: "/Sales/ExportCustomers",
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',                
                type: "POST",
                success: function (d) {
                  
                  window.location = '/Sales/ExportCustomers';
                },
              error: function (errormessage) {
            alert(errormessage.responseText);
              }
            });
   }
          
</script>
    <div class="row">

        <div class="col-xs-12">
             <button type="button"  class="btn btn-primary" id="btnExcelDownload" onclick="ExportAsExcel();">EXPORT AS EXCEL</button>
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>                           
                           <tr>
                              
                                 <th>Outlet Name</th> 
                                 <th>Route Name</th> 
                                 <th>Area Name</th>
                                 <th>Contact Person</th>
                                 <th>Contact Number</th>                              
                                 <th>Price Category</th>
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                            <tr>
                               
                                <td><%: Html.DisplayFor(m => item.OutletName) %></td>
                                <td><%: Html.DisplayFor(m => item.Route) %></td>
                                <td><%: Html.DisplayFor(m => item.Area) %></td>
                                <td><%: Html.DisplayFor(m => item.ContactName) %></td>   
                                <td><%: Html.DisplayFor(m => item.ContactNumber) %></td>
                                <td><%: Html.DisplayFor(m => item.PriceCategory) %></td>
                                <td><a class="btn btn-xs btn-primary" href="/Customers/CustomerEdit/<%: Html.DisplayFor(m => item.OutletID) %>"><i class="fa fa-truck"></i></a></td>
                            </tr>
                            <% } %>
                        </tbody>                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</asp:Content>
