﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OperatorToolSpace.Models.EditSalesModel>" %>


<style type="text/css">
    #gridContent
    {
        width:70%;
    }
</style>
<div id="gridContent">
    <div class="row">
                <% using (Html.BeginForm("updateSoldProduct", null, FormMethod.Post, new { role = "form", id = "FormUser", enctype = "multipart/form-data" }))
                { %>
        <form>
                <div class="form-horizontal">
                <label >Product</label>
               <select class="form-control" style = "width:50%">

                   <option value="6KG Refill">6KG Refill</option>
                    <option value="13KG Refill">13KG Refill</option>
                    <option value="6KG Outright">6KG Outright</option>
                    <option value="13KG Outright"> 13KG Outright</option>               

               </select>
                </div>
                <div class="form-group">
                <label>Quantity</label>
                <%: Html.TextBoxFor(model => model.Quantity, new { @class = "form-control", @style = "width:50%" }) %>
                </div>
                <div class="form-group">
                <label >SKU TransAmount</label>
                <%: Html.TextBoxFor(model => model.SKUTransAmount, new { @class = "form-control", @style = "width:50%" }) %>
                </div>
                                
                <button type="submit" class="btn btn-primary" id="SaveSale">Save</button>
                
            </form>
                <%} %>
        </div>
</div>