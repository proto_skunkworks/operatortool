﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.AcceptedCylinderModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Accepted Cylinders" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script src="../../Scripts/jquery.min.js"></script>
<script src="../../Scripts/bootstrap.min.js"></script>
<link href="../../Scripts/bootstrap.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b></b> <%: TempData["Cylinder Added"] %> 
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                  <table id="example1" class="table table-bordered">
                        <thead>                           
                           <tr>                              
                                <th>Brand</th> 
                                <th>Category</th>
                                <th>Accepted</th>
                                <th>Update</th> 
                            </tr>
                        </thead>
                        <tbody>
                        <% foreach (var item in Model)
                               {%>
                            <tr> 
                            <% using (Html.BeginForm("UpdateCylinder", null, FormMethod.Post, new { role = "form", id = "FormUser", enctype = "multipart/form-data" }))
                           { %>
                             <td> <%: Html.DisplayFor(m => item.BrandName) %> </td>
                                  <td> <%: Html.DisplayFor(m => item.Category) %> </td>
                            
                             <td> <div class="checkbox">  <%: Html.CheckBoxFor(m => item.Status,new {@class="flat-red",@id="checkbx"+item.id}) %>  </div> </td> 
                              
                             <td> <button type="button" class="btn btn-primary" id="btnUpdate" onclick="UpdateCylinder(<%: item.id%>);">Update</button></td>
                             <% } %>

                            </tr>                               
                            <% } %>
                    </tbody>                        
               </table>
            </div>
            <!-- /.box -->
        </div>
    </div>
           <style>
                #example1{
                    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;

                    width: 60%;
                }
                #example1 td, #example1 th {
                    border: 0px solid #ddd;
                    padding: 8px;
                }
                #example1 tr:nth-child(even){background-color: #f2f2f2;}

            </style>
    <script>

function UpdateCylinder(id) {  
    var selectedRoutes = id;
    var checkbx =$("#checkbx"+id).is(":checked");
    
    var empObj = {
        Status: $("#checkbx" + id).is(":checked"),
        CylinderID:id,
    };
    console.log(id, checkbx);
    $.ajax({  
        url: "/Sales/AcceptedCylinders",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alert(result.responseText);
          
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}
    </script>
</asp:Content>
