﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.BulkOrdersModel>>" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Bulk Orders" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- row -->
     <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.min.css" rel="stylesheet" />
        <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/select2.min.js"></script>
    <link href="../../Scripts/select2.min.css" rel="stylesheet" />
    <link href="../../Scripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <%: @TempData["Success"] %> 
       
            </div>
            <% } %>
        </div>
    </div>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                             <tr>  
                                <th>Order ID</th>
                                <th>Customer ID</th>  
                                <th>Customer Name</th>                                
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Delivery Date</th> 
                                 <th>Status</th>  
                                <th>Approval</th>
                             </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                            <tr> 
                                <td><%: Html.DisplayFor(m => item.order_id) %></td>
                                <td><%: Html.DisplayFor(m => item.customer_id) %></td>
                                <td><%: Html.DisplayFor(m => item.customer_name) %></td>                              
                                <td><%: Html.DisplayFor(m => item.price) %></td>
                                <td><%: Html.DisplayFor(m => item.approved_quantity) %></td>
                                <td><%: Html.DisplayFor(m => item.expected_delivery_date) %></td>
                                <td><%: Html.DisplayFor(m => item.status) %></td>
                               
                               
                                 <td>
                               <button type="button" class="btn btn-xs btn-primary" onclick="LoadTrucks(<%:item.order_id %>);" data-toggle="modal" data-target="#<%: Html.DisplayFor(m => item.order_id) %>">PLAN</button>
                               <div class="modal fade" id="<%: Html.DisplayFor(m => item.order_id) %>" role="dialog">
                               <div class="modal-dialog modal-sm">
                               <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title"><%: Html.DisplayFor(m => item.customer_name) %> order delivery.</h4>
                                </div>
                                <div class="modal-body">
                                     <form>
                                  <h4><%: Html.DisplayFor(m => item.customer_name) %></h4>  
                                    <div class="form-group">
                                    <label>Delivery Vehicle:</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-truck"></i>
                                      </div>
                                    <select class="form-control select2"  id="Truckreg<%:item.order_id %>" style="width: 100%;"> </select>
                                    </div>
                                   </div> 
                                    <div id="details"> </div>
                                    </form>
                                   <input type="hidden" id="saleid" value="<%:item.order_id %>" />
                                    <input type="hidden" id="driver_id" />
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="AllocateTruck(<%:item.order_id %>);">SAVE</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                       </td>
                            </tr>
                            <% } %>
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
 </div>
 <script>
$( document ).ready(function() {
   //LoadTrucks();
});
     function LoadTrucks(order_id) {
          var vType = {Category:2};
            var Truckregs = $("#Truckreg"+order_id);
            Truckregs.empty().append('<option selected="selected" value="0" disabled = "disabled">Select Truck</option>');
            $.ajax({
                type: "GET",
                url: "/Bulk/GetBulkTruckRegs",
                data: JSON.stringify(vType),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.each(response, function () {
                        Truckregs.append($("<option></option>").val(this['id']).html(this['Truckreg']));
                    });
                },
                failure: function (response) {
                    //alert(response.responseText);
                },
                error: function (response) {
                    //alert(response.responseText);
                }
            });
        }

        function VehicleDetails(vid) {            
            var vidObj =
            {
                VID: vid,                
            };
            $.ajax({
                type: "POST",
                url: "/planning/GetVehicleAssignments",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vidObj),
                dataType: "json",
                success: function (data) {                  
                 var table = $("#details");         
                 var count = Object.keys(data).length;
                 if(count>0)
                 {
                     var driver_id;                    
                     var eTable = "<table><thead><tr><th>VREG</th><th>Driver</th><th>Change</th></tr></thead><tbody>"
                   $.each(data,function(index, row){   
                    eTable += "<tr>";
                      $.each(row, function (key, value) {
                          if (key == "driver_id") {
                              driver_id = value;
                               $("#driver_id").val(value);
                          }
                          if ( (key == "driver_id") || (key == "TruckHelper") || (key == "VID") || (key == "routes") ){
                             
                          }
                          else {
                               eTable += "<td>" + value + "</td>";
                          }
                      });
                      eTable += '<td><a class="btn btn-xs btn-default" href="/Drivers/EditDriver/' + driver_id + '"><i class="fa fa-edit">CHANGE</i></a </td>';
                    eTable += "</tr>";
                  });
                  eTable +="</tbody></table>";
                 table.html(eTable);

                 }
                else{
                table.html("No records found!");
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

          }
        var $eventSelect = $(".select2");
        $eventSelect.select2();       
        $eventSelect.on("change", function (e)
        {
            var VID = $(".select2 :selected").attr('value');
            VehicleDetails(VID);
         });

     function  AllocateTruck(order_id)
     {
         var driver_id = $("#driver_id").val();
         var order_id = order_id;
         var vid = $(".select2 :selected").attr('value');
         var AssObj = {
             driver_id: driver_id,
             vid: vid,
             order_id:order_id
         }
        $.ajax({  
        url: "/Bulk/SavePlanning",  
        data: JSON.stringify(AssObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alertify.alert('Message', result.Message, function () { alertify.message('Ok'); }); 
                           
        },  
         failure: function (result) {
                  alertify.alert('Error', 'Ooops! Make sure you filled all the necessary fields', function () { alertify.error('Ok'); });
                },
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });
  }
</script>
  <style>
        #details{
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size:11px;
        width: 100%;
        }
        #details td, #details th {
        border: 1px solid #ddd;
        padding: 5px;
        }
        #details tr:nth-child(even){background-color: #f2f2f2;}

        </style>
</asp:Content>
