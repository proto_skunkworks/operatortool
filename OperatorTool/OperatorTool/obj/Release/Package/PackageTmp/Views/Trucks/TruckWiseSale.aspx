﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.TruckWiseSalesModel>>" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Sales" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/moment/min/moment.min.js"></script>
   
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <%: @TempData["Success"] %> 
       
            </div>
            <% } %>
        </div>
    </div>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                             <tr>                              
                                <th>Driver Name</th> 
                                <th>Truck reg</th>                                
                                <th>6KG Refill</th>
                                 <th>13KG Refill</th>
                                <th>50KG Refill</th>
                                <th>6KG Outright</th>                                
                                <th>13KG Outright</th>                              
                                <th>50KG Outright</th> 
                                <th>Grills</th> 
                                <th>Burners</th> 
                                 <th>Total Sold</th>   
                                <th>Total Amount</th>                                 
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                            <tr onclick="location.href = '<%:(Url.Action("Sales", null, new { id = item.DriverID })) %>'">                               
                                <td><%: Html.DisplayFor(m => item.DriverName) %></td>
                                <td><%: Html.DisplayFor(m => item.Truckreg) %></td>                                                             
                                <td><%: Html.DisplayFor(m => item.Refill_6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Burners) %></td>
                                <td><%: Html.DisplayFor(m => item.Grills) %></td>
                                <td id="totalSold"><%: Html.DisplayFor(m => item.TotalSold) %></td>
                                <td><%: Html.DisplayFor(m => item.TotalRevenue) %></td>
                               
                            </tr>
                            <% } %>
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        
    </div>
    <link href="../../Scripts/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <style>
        #totalSold
        {
           border: 1px solid #ddd; 
          padding: 2px; padding-top: 4px; padding-bottom: 4px; text-align: left; background-color: #626262;
    color: white;
        }
    </style>   
    <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  })
</script>


</asp:Content>
