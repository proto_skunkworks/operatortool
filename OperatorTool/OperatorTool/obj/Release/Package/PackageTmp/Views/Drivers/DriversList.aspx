﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.DriverListModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Drivers List" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <script src="../../Scripts/jquery.min.js"></script>

    <link href="../../Scripts/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>
    <script type="text/javascript">
        function ExportAsExcel() {  
             var fileDownloadCheckTimer;
            blockUIForDownload();

    var token = $('#download_token_value_id').val();
   $.ajax(
            {

                url: "/Drivers/ExportAsExcel?token="+token,
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',                
                type: "POST",
                success: function (d) {
                  
                  window.location = '/Drivers/ExportAsExcel?token='+token;
                },
              error: function (errormessage) {
            alert(errormessage.responseText);
              }
            });
   }
   function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#download_token_value_id').val(token);
        $('#spinner').show();
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = Cookies.get('fileDownloadToken');
            if (cookieValue == token)
                finishDownload();
        }, 1000);
    }        
</script>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                    <button type="button"  class="btn btn-primary" id="btnExcelDownload" onclick="ExportAsExcel();">EXPORT AS EXCEL</button>
                </div>
                         
              </div>
             

         </div>
        
        
            
    </div>
    <div class="row">

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>                           
                           <tr>                              
                                <th>First Name</th> 
                                <th>Last Name</th> 
                                <th>Phone Number</th>
                                <th>Truck</th>
                                <th>Current Routes</th>                              
                                <th>Till Number</th>
                                <th>Actions</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                                {%>
                            <tr>                               
                                <td><%: Html.DisplayFor(m => item.FirstName) %></td>
                                <td><%: Html.DisplayFor(m => item.LastName) %></td>
                                <td><%: Html.DisplayFor(m => item.PhoneNumber) %></td>
                                <td><%: Html.DisplayFor(m => item.Truckreg) %>
                                
                                </td>   
                                <td><%: Html.DisplayFor(m => item.CurrentRoute) %></td>
                                <td><%: Html.DisplayFor(m => item.CurrentTill) %></td>
                                <td><a class="btn btn-xs btn-primary" href="/Drivers/EditDriver/<%: Html.DisplayFor(m => item.DriverID) %>"><i class="fa fa-truck"></i></a>
                                 
                               <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#<%: Html.DisplayFor(m => item.DriverID) %>">LEAVE</button>
                               <div class="modal fade" id="<%: Html.DisplayFor(m => item.DriverID) %>">" role="dialog">
                               <div class="modal-dialog modal-sm">
                               <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Approve Off/Leave</h4>
                                </div>
                                <div class="modal-body">
                                  <p><%: item.FirstName+" " %> <%: item.FirstName %></p>
                                    <div class="form-group">
                                        <label>From</label>
                                        <input type="text" id="from_date" class="form-control"  style="width:60%"/>
                                    </div>
                                     <div class="form-group">
                                        <label>To</label>
                                        <input type="text" id="to_date" class="form-control" style="width:60%"/>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">SAVE</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                                    </td>
                           </tr>
                            <% } %>
                        </tbody>                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <link href="../../Scripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />   <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#to_date').datepicker({
      autoclose: true
    })
      
       //Date picker
    $('#from_date').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  })
</script>

    <script src="../../Scripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            <script src="../../Scripts/bower_components/moment/moment.js"></script>
    <script src="../../Scripts/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
</asp:Content>
