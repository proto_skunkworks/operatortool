﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.EditDriverModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Amend Driver Details" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 

    <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/jquery-ui.min.js"></script>

    <script src="../../Scripts/select2.min.js"></script>
    <link href="../../Scripts/select2.min.css" rel="stylesheet" />
    <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.css" rel="stylesheet" />

    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Edit Form <%: Html.DisplayTextFor(model => model.FirstName) %>  <%: Html.DisplayTextFor(model => model.LastName) %> </h3>
                </div>
                
                <div class="box-body">
                    <div class="row">
                       
                         <div class="container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" class="nav_link" data_id="7" href="#planning">PLANNING</a></li>
                            <li><a data-toggle="tab" class="nav_link" data_id="1" href="#home">TRUCK</a></li>
                            <li><a data-toggle="tab" class="nav_link" data_id="2" href="#menu1">ROUTE(S)</a></li>                                                          
                            <li><a data-toggle="tab" class="nav_link" data_id="3" href="#menu2">TILL NUMBER</a></li>
                            <li><a data-toggle="tab" class="nav_link" data_id="6" href="#menu4">BIO DATA</a></li>
                            <li><a data-toggle="tab" class="nav_link" data_id="5" href="#menu5">ACCOUNT</a></li>
                                                            
                        </ul>

                        <div class="tab-content">
                                <div id="planning" class="tab-pane fade in active">
                            <div class="row">
          <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">New  Trip</h3>
            </div>
            <div class="box-body">
             
                 <div class="form-group">
                <label>Category</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-shopping-cart"></i>
                  </div>
                 <select class="form-control select2" style="width: 100%;" id="Category">  
                  <option selected="selected" value="0" disabled = "disabled">Select one</option>
                  <option value="1">Sales</option>
                  <option value="2">Replenishment</option>                 
                </select>
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Destination</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-map-marker"></i>
                  </div>
                  <select class="form-control select2" style="width: 100%;" id="destination"> </select>
                </div>
                <!-- /.input group -->
              </div>
              <!-- Date -->
               <div class="form-group">
                <label>Date: (Date you are Planning for)</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                <label>Quantity 6KG</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-upload"></i>
                  </div>
                  <input type="text" class="form-control" id="kg6_refill" />
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Quantity 13KG</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-upload"></i>
                  </div>
                  <input type="text" class="form-control" id="kg13_refill" />
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Quantity 50KG</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-upload"></i>
                  </div>
                  <input type="text" class="form-control" id="kg50_refill" />
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Burners</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sun-o"></i>
                  </div>
                  <input type="text" class="form-control" id="burners" />
                </div>
                <!-- /.input group -->
              </div>
                  <div class="form-group">
                <label>Grills</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sun-o"></i>
                  </div>
                  <input type="text" class="form-control" id="grills" />
                </div>
                <!-- /.input group -->
              </div>
               <div class="box-footer">
                   <input type="hidden" class="form-control" id="driver_id" />
                   <input type="hidden" class="form-control" id="truckhelper" />
                <button type="submit" class="btn btn-primary" onclick="SavePlanning();">Submit</button>
              </div>
                
            </div>
            
          </div>
        </div>
          
            <div class="box-body">
             <div class="col-md-6">
             <div class="box box-primary">
              <div id="details"> </div>
              </div>
               </div>
                </div>
               
            </div>

                                </div>




                            <div id="home" class="tab-pane fade in active">
                                <h4>ASSIGN TRUCK</h4>
                                    CURRENT TRUCK: <strong><%: Html.DisplayFor(model => model.Truckreg, "", new { @class = "text-danger" ,@id=""}) %> </strong>
                                    <button type="button" class="btn btn-xs btn-danger" id="btnRevoke" onclick="RevokeVehicle();">REVOKE</button>
                                <input type="hidden" id="DriverID" value=<%: Html.DisplayTextFor(model => model.DriverID) %> /> 
                                    <input type="hidden" id="assignedVehicle" value=<%: Html.DisplayTextFor(model => model.Truckreg) %> />
                                    <br><label>Trucks</label> 
                                    <div class="form-group">     
                                    <select id="Truckreg" class="form-control select2"   style="width:30%"></select>
                                    </div> 
                                   
                                    Truck Helper
                                    Current: <strong><%: Html.DisplayFor(model => model.FirstName, "", new { @class = "text-danger" }) %> </strong>
                                    <div><label>Change Truck Helper</label> </div>
                                   <div class="form-group">          
                                    <select id="TruckHelper" class="form-control select2" style="width:30%"></select>                                                                        
                                    </div>
                                <button type="button" class="btn btn-primary" id="btnUpdateTruck" onclick="AssignTruck();">Save Changes</button>
                                <br />
                                <div id="ProductsDiv1">
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <h4>ASSIGN ROUTE</h4>
                                    CURRENT ROUTES: <strong><%: Html.DisplayFor(model => model.CurrentRoute, "", new { @class = "text-danger" }) %> </strong>
                                    <div class="form-group">
                                    <label>Routes</label>
                                    <select class="form-control select2" multiple="multiple" id ="routes"data-placeholder="Select Routes"
                                    style="width: 40%;">
                                    </select>
                                    </div>
                                <button type="button" class="btn btn-primary" id="btnUpdateRoute" onclick="AssignRoutes();">Assign</button>                                                                
                                <div id="ProductsDiv2">
                                </div>
                                </div>
                            <div id="menu2" class="tab-pane fade">
                                <h3>ASSIGN TILL</h3>
                                CURRENT TILL: <strong><%: Html.DisplayFor(model => model.CurrentTill, "", new { @class = "text-danger" }) %> </strong>
                                    <div class="form-group">
                                    <label>Select Till Number</label>                                
                                    <select id="TillNumber" class="form-control" style="width:30%"></select>                                                                        
                                    </div>
                                <button type="button" class="btn btn-primary" id="btnUpdateTill" onclick="UpdateTillNumber();">Update</button>
                                <div id="ProductsDiv3">
                                </div>
                            </div>

                                <div id="menu4" class="tab-pane fade">
                                <h3>EDIT DRIVER</h3>
                                    <% using (Html.BeginForm("UpdateDriver", null, FormMethod.Post, new { role = "form", id = "FormUser", enctype = "multipart/form-data" }))
                                        { %>
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">First Name</label>
                                    <%: Html.TextBoxFor(model => model.FirstName, new { @class = "form-control", @required = "true", @style = "width:30%" }) %>                                
                                    <input type="hidden" class="form-control" name="UserID" id="UserID" value="<%: Model.DriverID %>" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last Name</label>
                                    <%: Html.TextBoxFor(model => model.LastName, new { @class = "form-control", @style = "width:30%" }) %>
                                </div>
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <%: Html.TextBoxFor(model => model.Email, new { @class = "form-control", @style = "width:30%" }) %>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Phone Number</label>
                                    <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "form-control", @style = "width:30%" }) %>
                                </div>
                                <%: Html.HiddenFor(model => model.DriverID, new { @class = "form-control", @style = "width:30%" }) %>
                                <div class="form-group">
                                    <label>Change Password</label>
                                    <%: Html.TextBoxFor(model => model.Password, new { @class = "form-control", @style = "width:30%" }) %>
                                </div>
                                <button type="submit" class="btn btn-primary" id="EditDriver">Update</button>
                                <div id="ProductsDiv4">
                                </div>
                                    <%} %>
                            </div>
                            <div id="menu5" class="tab-pane fade">
                                <h3>App Accout Status</h3>
                                    Account Status: <strong><%: Html.DisplayFor(model => model.CurrentTill, "", new { @class = "text-danger" }) %> </strong>
                                                                 
                                <button type="submit" class="btn btn-danger" id="BlockAccount" onclick="Block();">BLOCK</button>
                                                               
                            </div>
                        </div>
                    </div>

                </div>
                     
            </div>
                
                                
        </div>
            <!-- /.box -->

        </div>
         <div class="row"  align="center">
         <a class="btn btn-xs btn-primary" href="/Drivers/DriversList"><i class="fa fa-angle-double-left" >Back</i></a>
        </div>
</div>
      <script type="text/javascript">
          GetContainers();
          $("#selectContainer").hide();


//          $( document ).ready(function() {
//              console.log("ready!");
//              var truck = $("#assignedVehicle").val();
//               console.log(truck);
//              var s = $("#Truckreg").val('82').trigger('change'); 
//              console.log(s);
//});
         
    // $("#Truckreg option[text='"+truck+"']").attr("selected","selected"); 
      $(".myselect").select2();
        $(function () {
            var Truckregs = $("#Truckreg");
            //Truckregs.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
            $.ajax({
                type: "POST",
                url: "/Drivers/GetTruckRegs",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.each(response, function () {
                        Truckregs.append($("<option></option>").val(this['id']).html(this['Truckreg']));
                    });
                },
                failure: function (response) {
                    //alert(response.responseText);
                },
                error: function (response) {
                    //alert(response.responseText);
                }
            });
         });
 function AssignTruck() {  
     var Truck = $("#Truckreg :selected").attr('value');    
     var truck_helper = $("#TruckHelper :selected").attr('value');
     var empObj = {
         DriverID: $('#DriverID').val(),
         Truckreg: Truck.trim(),        
         truck_helper: truck_helper.trim(),
         
    };  
    console.log(empObj);
    $.ajax({  
        url: "/Drivers/AssignTruck",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alertify.alert('Message', result.responseText, function () { alertify.message('Ok'); });
             $('#assignedVehicle').append(Truckreg);
             $("#btnRevoke").show();
          
        },  
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });  
   }

         
function AssignRoutes() {  
    var selectedRoutes = $('#routes').val();
    if (selectedRoutes==null) {       
        alertify.alert('Error', 'You must select Route to assign!', function(){ alertify.error('Ok'); });
        return;
}
    var empObj = {
        Routes: selectedRoutes,   
           DriverID: $('#DriverID').val(),
    };  
   // console.log(empObj);
    $.ajax({  
        url: "/Drivers/AssignRoutes",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
         
            alertify.alert('Success', result.responseText, function () { alertify.message('Ok'); });
          
        },  
        error: function (errormessage) {  
              alertify.alert('Error', errormessage.responseText, function(){ alertify.error('Ok'); });           
        }  
    });  
 }
function RevokeVehicle() {  
    var assignedVehicle = $('#assignedVehicle').val();
    if (assignedVehicle=="/") {       
        alertify.alert('Error', 'No Vehicle is Assigned to the Driver!', function () { alertify.error('Ok'); });
         $("#btnRevoke").hide();
        return;
}
    var empObj = {      
        DriverID: $('#DriverID').val()
    };  
    console.log(empObj);
    $.ajax({  
        url: "/Drivers/RevokeVehicle",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {
            alertify.alert('Success', result.responseText, function () { alertify.message('Ok'); });
            $('#assignedVehicle').empty();
        },  
        error: function (errormessage) {             
           alertify.alert('Error', errormessage.responseText, function(){ alertify.error('Ok'); });          
            
        }  
    });  
 }

         $(function () {
            var ddlCustomers = $("#routes");
            $.ajax({
                type: "POST",
                url: "/Drivers/GetRoutes",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                        ddlCustomers.append($("<option></option>").val(this['id']).html(this['RouteName']));
                    });
                },
                failure: function (response) {
                   alertify.alert(response.responseText);
                },
                error: function (response) {
                   alertify.alert(response.responseText);
                }
            });
         });

         $(function () {
            var TruckHelper = $("#TruckHelper");
            $.ajax({
                type: "POST",
                url: "/Drivers/GetTruckHelper",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                        TruckHelper.append($("<option></option>").val(this['thelper_id']).html(this['truck_helper']));
                    });
                },
                failure: function (response) {
                   alertify.alert(response.responseText);
                },
                error: function (response) {
                   alertify.alert(response.responseText);
                }
            });
        });
 
        $(function(){
        $("#routes").select2();
          });
        $(function(){
        $("#TruckHelper").select2();
        })
            $(function () {
        $("#lSource").select2();
        })
          
        $(function(){
        $("#Truckreg").select2();
        })
          
        $(function(){
        $("#destination").select2();
        })
   $( "#routes" ).change(function() {  
       var str = "";  
       $( "select option:selected" ).each(function() {  
           str += $(this).text() + " , ";          
       });
     }).trigger("change");  


    $('#Category').change( function() {
    var selectedItem = $(this).val();
    if (selectedItem == "Replenishment") {
        $("#destination").empty();
        $("#selectContainer").show();
         GetContainers();
    }
    else {
         $("#selectContainer").hide();
    } });

          function GetContainers() {
              var destination = $("#destination");
            var lSource = $("#lSource");
            $.ajax({
                type: "GET",
                url: "/Drivers/GetContainers",               
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                          destination.append($("<option></option>").val(this['ContainerName']).html(this['ContainerName']));
                          lSource.append($("<option></option>").val(this['ContainerName']).html(this['ContainerName']));
                    });
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

          }

</script> 
     <script type="text/javascript">

        $('#datepicker').datepicker({
            autoclose: true,
            format:"dd-mm-yyyy"
        })
          $(function(){            
              $("#destination").select2();
          })
         
         function GetContainers() {
              var destination = $("#destination");           
            $.ajax({
                type: "GET",
                url: "/Drivers/GetContainers",               
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                          destination.append($("<option></option>").val(this['ContainerName']).html(this['ContainerName']));                         
                    });
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

        }
     
$( document ).ready(function() {
    LoadTrucks();
});
     function LoadTrucks() {
            var Truckregs = $("#Truckreg");
            Truckregs.empty().append('<option selected="selected" value="0" disabled = "disabled">Select Truck</option>');
            $.ajax({
                type: "POST",
                url: "/planning/GetTruckRegs",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.each(response, function () {
                        Truckregs.append($("<option></option>").val(this['id']).html(this['Truckreg']));
                    });
                },
                failure: function (response) {
                    //alert(response.responseText);
                },
                error: function (response) {
                    //alert(response.responseText);
                }
            });
        }

        function GetVehicleAssignments(vid) {            
            var vidObj =
            {
                VID: vid,
            };
            $.ajax({
                type: "POST",
                url: "/planning/GetVehicleAssignments",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vidObj),
                dataType: "json",
                success: function (data) {                  
                 var table = $("#details");         
                 var count = Object.keys(data).length;
                 if(count>0)
                 {
                     var driver_id;
                    
                     var eTable = "<table><thead><tr><th>VID</th><th>Vreg</th><th>Driver</th><th>Truck Helper</th><th>Routes</th><th>Change</th></tr></thead><tbody>"
                  $.each(data,function(index, row){   
                    eTable += "<tr>";
                      $.each(row, function (key, value) {
                          if (key == "driver_id") {
                              driver_id = value;
                          }
                          if (key == "driver_id") {
                             
                          }
                          else {
                               eTable += "<td>" + value + "</td>";
                          }
                      });
                      eTable += '<td><a class="btn btn-xs btn-default" href="/Drivers/EditDriver/' + driver_id + '"><i class="fa fa-edit">RETURNS</i></a </td>';
                    eTable += "</tr>";
                  });
                  eTable +="</tbody></table>";
                 table.html(eTable);

                 }
                else{
                table.html("No records found!");
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

        }
        function GetLastLoadedQuantity(VID) {            
            var vidObj =
            {
                VID: VID,
            };
            $.ajax({
                type: "POST",
                url: "/planning/GetLastLoaded",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vidObj),
                dataType: "json",
                success: function (data) { 
                 var count = Object.keys(data).length;
                 if(count>0)
                 { 
                  $.each(data,function(index, row)
                        {  
                        $.each(row, function (key, value) 
                             {
                              if (key == "kgs_6") {
                               $('#kg6_refill').val(value);
                             }
                             if (key == "kgs_13") {
                               $('#kg13_refill').val(value);
                              }
                             if (key == "kgs_50") {
                               $('#kg50_refill').val(value);
                              }
                             if (key == "burners") {
                               $('#burners').val(value);
                            }
                              if (key == "grills") {
                               $('#grills').val(value);
                               }
                            
                            });
                     
                        });
                 }
                else{
                
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });
        }


    $('#Category').change( function() {    
        var selectedItem = $(this).val();
        console.log(selectedItem);
    if (selectedItem == "2") {
        $("#destination").empty();       
        GetContainers();
        $("#destination").prop("disabled", false);
    }
    else {
        $("#destination").empty();
        $("#destination").empty().append('<option selected="selected" value="Sales" disabled = "disabled">Sales</option>');
         $("#destination").prop("disabled", true);
            }
        });

      var $eventSelect = $("#Truckreg");
      $eventSelect.select2();       
        $eventSelect.on("change", function (e)
        {
            var VID = $("#Truckreg :selected").attr('value');
            GetVehicleAssignments(VID);
            GetLastLoadedQuantity(VID);
        });
   function SavePlanning() {  
       var Truck = $("#Truckreg :selected").attr('value');
       var Category = $("#Category :selected").attr('value');
       var destination = $("#destination :selected").attr('value');
       var planned_date = $("#datepicker").datepicker().val();
       var kg6_refill = $("#kg6_refill").val();
       var kg13_refill = $("#kg13_refill").val();
       var burners = $("#burners").val();
       var grills = $("#grills").val();
       var kg50_refill = $("#kg50_refill").val();

       if ($.trim(Truck) =="0") {
           alertify.alert('Error', 'Ooops! You must Select vehicle to plan trip.', function () { alertify.error('Ok'); });
           return;
       }
       
       if ($.trim(Truck) =="" || $.trim(Category) == "" || $.trim(destination) == "" ||  $.trim(planned_date)=="") {
           alertify.alert('Error', 'Ooops! Make sure you filled all the necessary fields', function () { alertify.error('Ok'); });
           return;
       }
       if ($.trim(kg6_refill) == "" &&
           $.trim(kg13_refill) == "" &&
           $.trim(burners) == "" &&
           $.trim(grills) == "" &&
           $.trim(kg50_refill) == "") {
           alertify.alert('Error', 'Ooops! Quantity fields are empty!', function () { alertify.error('Ok'); });
           return;
       }
        var empObj = {      
         vid: Truck.trim(),       
         Category: Category.trim(),
         destination: destination.trim(),
         planned_date: planned_date,
         kg6_refill: kg6_refill,
         kg13_refill: kg13_refill,
         burners: burners,
         grills: grills,
         kg50_refill:kg50_refill,
        
       };  

   
    $.ajax({  
        url: "/Planning/SavePlanning",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alertify.alert('Message', result.responseText, function () { alertify.message('Ok'); }); 
                $("#Truckreg").empty(); 
                LoadTrucks();            
        },  
         failure: function (result) {
                  alertify.alert('Error', 'Ooops! Make sure you filled all the necessary fields', function () { alertify.error('Ok'); });
                },
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });  
   }
</script>
</asp:Content>
