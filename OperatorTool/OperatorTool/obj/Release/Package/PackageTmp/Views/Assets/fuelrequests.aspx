﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.FuelRequestsModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Fuel Requests" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js" type="text/javascript"></script>  
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <script type="text/javascript">
        function ExportAsExcel() {  
             var fileDownloadCheckTimer;
            blockUIForDownload();

    var token = $('#download_token_value_id').val();
        $.ajax(
            {

                url: "/Drivers/ExportAsExcel?token="+token,
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',                
                type: "POST",
                success: function (d) {
                  
                  window.location = '/Drivers/ExportAsExcel?token='+token;
                },
              error: function (errormessage) {
            alert(errormessage.responseText);
              }
            });
   }
   function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#download_token_value_id').val(token);
        $('#spinner').show();
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = Cookies.get('fileDownloadToken');
            if (cookieValue == token)
                finishDownload();
        }, 1000);
    }        
</script>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                    <button type="button"  class="btn btn-primary" id="btnExcelDownload" onclick="ExportAsExcel();">EXPORT AS EXCEL</button>
                </div>
                         
              </div>
             

         </div>
        
        
            
    </div>
    <div class="row">

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>                           
                           <tr>  
                                <th>Order ID</th> 
                                <th>Driver Name</th> 
                                <th>Asset</th> 
                                <th>Mileage </th>
                               <th>Current Level </th>
                                <th>Fueling Location </th>
                                <th>Date Requested</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                                {%>
                            <tr>                               
                                <td><%: Html.DisplayFor(m => item.order_id) %></td>
                                <td><%: Html.DisplayFor(m => item.driver_name) %></td>
                                <td><%: Html.DisplayFor(m => item.request_vregno) %></td>
                                  <td><%: Html.DisplayFor(m => item.request_mileage)+" Km" %></td>
                                   <td><%: Html.DisplayFor(m => item.request_quantity) %></td>
                                  <td><%: Html.DisplayFor(m => item.fueling_station) %></td>
                                <td><%: Html.DisplayFor(m => item.date_requested) %></td> 
                                <%if (item.request_status == "Pending")
                                              { %>
                                        <td><span class="label label-default"><%: Html.DisplayFor(m => item.request_status) %></span> </td>
                                   <% } else if (item.request_status == "Approved") {%>
                               <td><span class="label label-success"><%: Html.DisplayFor(m => item.request_status) %></span> </td>

                                <%}  else if (item.request_status == "Closed") {%>
                                <td><span class="label label-warning"><%: Html.DisplayFor(m => item.request_status) %></span> </td>

                                <%}  else if (item.request_status == "Rejected") {%>
                                 <td><span class="label label-danger"><%: Html.DisplayFor(m => item.request_status) %></span> </td>

                                <%} %>
                                 <%if (item.request_status == "Pending")
                                     { %>
                               <td>
                               <a class="label label-success" href="/Assets/fuelinghistory/<%: Html.DisplayFor(m => item.order_id) %>">VIEW<i class="fa fa-check-circle"></i></a>
                                  
                                </td>
                                <%} else { %>
                               
                                 <td>_</td>
                                 <%} %>
                            </tr>
                            <% } %>
                        </tbody>                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <script type="text/javascript">
        $(function () {

            var new_dialog = function (type, row) {
                var dlg = $("#dialog-form").clone();
                var fname = dlg.find(("#first-name")),
            lname = dlg.find(("#last-name")),
            email = dlg.find(("#email")),
            password = dlg.find(("#password"));
                type = type || 'Create';
                var config = {
                    autoOpen: true,
                    height: 300,
                    width: 350,
                    modal: true,
                    buttons: {
                        "Create an account": save_data,
                        "Cancel": function () {
                            dlg.dialog("close");
                        }
                    },
                    close: function () {
                        dlg.remove();
                    }
                };
                if (type === 'Edit') {
                    config.title = "Edit User";
                    get_data();
                    delete (config.buttons['Create an account']);
                    config.buttons['Edit account'] = function () {
                        row.remove();
                        save_data();

                    };

                }
                dlg.dialog(config);

                function get_data() {
                    var _email = $(row.children().get(1)).text(),
                _password = $(row.children().get(2)).text();
                    email.val(_email);
                    password.val(_password);

                }

                function save_data() {
                    $("#users tbody").append("<tr>" + "<td>" + (fname.find("option:selected").text() + ' ').concat(lname.find("option:selected").text()) + "</td>" + "<td>" + email.val() + "</td>" + "<td>" + password.val() + "</td>" + "<td><a href='' class='edit'>Edit</a></td>" + "<td><span class='delete'><a href=''>Delete</a></span></td>" + "</tr>");
                    dlg.dialog("close");
                }
            };

            $(document).on('click', 'span.delete', function () {
                $(this).closest('tr').find('td').fadeOut(1000,

        function () {
            // alert($(this).text());
            $(this).parents('tr:first').remove();
        });

                return false;
            });
            $(document).on('click', 'td a.edit', function () {
                new_dialog('Edit', $(this).parents('tr'));
                return false;
            });

            $("#create-user").button().click(new_dialog);

        });
    </script>
</asp:Content>
