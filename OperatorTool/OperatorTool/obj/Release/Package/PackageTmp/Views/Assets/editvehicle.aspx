﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.VehicleDataModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Plan for Garage" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
    <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/jquery-ui.min.js"></script>

    <script src="../../Scripts/select2.full.min.js"></script>
    <script src="../../Scripts/select2.min.js"></script>
    <link href="../../Scripts/select2.min.css" rel="stylesheet" />
    <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.css" rel="stylesheet" />

    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="row">
                         <div class="container">    
                        <div id="home" class="tab-pane fade in active">
                            <h4> <strong><%: Html.DisplayFor(model => model.VRegNo, "", new { @class = "text-danger" ,@id=""}) %> </strong> </h4>  
                            
                             <input type="hidden" id="VRegNo" value=<%: Html.DisplayTextFor(model => model.VRegNo) %> />                                                                                          
                                <br><label>Workshop</label> 
                                <div class="form-group">     
                                <select id="workshop" class="form-control" style="width:30%">
                                    <option value="Kabati">Kabati</option>                                                                                                                                         
                                </select>
                                </div> 
                            <div class="form-group">
                                <label>Type of Repair</label>                                
                                <select id="repairType"  class="form-control select2" style="width:30%">
                                     <option value="0">Select</option>
                                    <option value="1">Repair</option>
                                    <option value="2">Accident Repair</option>  
                                     <option value="3">Tyre Maintainance</option>
                                    <option value="4">Refurbishment</option>
                                     <option value="5">Something Here</option>
                                    <option value="6">Break Down</option> 
                                </select>
                            </div>
                                <div class="form-group">
                                    <label>Issue Description</label><br>
                                     <select id="description" class="form-control select2" style="width:30%">
                                </select>
                                </div>
                               
                               <div class="form-group"> 
                                   <label>Date Service</label> <br>
                                <input type="text" class="form-control pull-left" id="checkedInDate" style="width:30%">                                                                       
                                </div>
                             <br>
                            <div class="form-group"> 
                                    <label>Date Out of Service</label> <br>
                                <input type="text" class="form-control pull-left" id="ExpectedOut" style="width:30%">                                                                       
                                </div>
                           
                             <br>
                             <div class="form-group">
                                <label>General Description</label>                                
                                 <textarea id="MoreInfo"class="form-control" rows="3" placeholder="Enter ..."  style="width:30%"></textarea>
                            </div>
                            <button type="button" class="btn btn-primary" id="btnGaragePlan" onclick="SaveGarageSchedule();">Save Changes</button>
                            <br />
                            <div id="ProductsDiv1">
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
          
        </div>
         <div class="row" align="center">
         <a class="btn btn btn-primary" href="/Drivers/DriversList"><i class="fa fa-angle-double-left" >Back</i></a>
        </div>
        </div>

        <link href="../../Scripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
        <script src="../../Scripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
 <script type="text/javascript">
      $("#selectContainer").hide();
     $(".myselect").select2();


   $('#checkedInDate').datepicker({
      autoclose: true
     })

      $('#ExpectedOut').datepicker({
      autoclose: true
     })

   $( "#routes" ).change(function() {  
       var str = "";  
       $( "select option:selected" ).each(function() {  
           str += $(this).text() + " , ";          
       });
                                                                                                                            }).trigger("change");  

$('#repairType').change( function() {
    var description = $("#description");
    
    var selectedItem = $(this).val();//Repair
    if (selectedItem == "0") {
        $("#description").empty();      
    }
    if (selectedItem == "1") {
        $("#description").empty();
        description.append($("<option></option>").val("Steering and Suspension").html("Steering and Suspension"));
        description.append($("<option></option>").val("Engine and Transmission").html("Engine and Transmission"));
         description.append($("<option></option>").val("Electrical").html("Electrical"));
         description.append($("<option></option>").val("Body Repair").html("Body Repair"));
    }
    else if  (selectedItem == "2")//Accident Repair
    {
          $("#description").empty();
         description.append($("<option></option>").val("Minor").html("Minor"));
        description.append($("<option></option>").val("Major").html("Major"));
    }
    else if  (selectedItem == "3")//Tyre Maintenannce
    {
          $("#description").empty();
        description.append($("<option></option>").val("Type Repair").html("Tyre Repair"));
        description.append($("<option></option>").val("Retracting").html("Retracting"));
        description.append($("<option></option>").val("Tyre Change").html("Tyre Change"));
        description.append($("<option></option>").val("Tyre Inspection").html("Tyre Inspection"));
    }
     else if  (selectedItem == "4")//Refurb
    {
          $("#description").empty();
          description.append($("<option></option>").val("Minor").html("Minor"));
        description.append($("<option></option>").val("Major").html("Major"));
    }
     else if  (selectedItem == "5")//Refurbishment
    { $("#description").empty();
        description.append($("<option></option>").val("Minor").html("Minor"));
           description.append($("<option></option>").val("Medium").html("Medium"));
        description.append($("<option></option>").val("Major").html("Major"));
    }
      else if  (selectedItem == "6")//Break down
    { $("#description").empty();
          description.append($("<option></option>").val("Break In").html("Break Down"));
        description.append($("<option></option>").val("Break Down").html("Break Down"));
    }
    
});

$('#destination').change( function() {
    
    var selectedItem = $(this).val();
    if (selectedItem == "Replenishment") {
        $("#Container").empty();
        $("#selectContainer").show();
         GetContainers();
    }
    else {
         $("#selectContainer").hide();
    }
    // $.get('/Sales/MyAction/' + selectedID , function(data) {
      //   $('#partialPlaceHolder').html(data);       
      //   $('#partialPlaceHolder').fadeIn('fast');
   //  });

});

//save garage plan 
   function SaveGarageSchedule()                                                                                                      
    {
        var VRegNo = $('#VRegNo').val();
        var repairType = $('#repairType').val();
        var description = $('#description').val();
        var MoreInfo = $('#MoreInfo').val();
        var date_checkedin = $('#checkedInDate').val();
        var date_checkedout = $('#ExpectedOut').val(); 
        var urlvalue = '<%:Url.Action("SaveGarageSchedule","Assets")%>'; 
        var garageModel = {
        "VRegNo" : VRegNo,
        "repairType" : repairType,
        "description": description,
        "MoreInfo": MoreInfo,
        "date_checkedin" : date_checkedin,
        "date_checkedout" :date_checkedout, 
       };
       console.log(garageModel);
        $.ajax({
            url: urlvalue,
            data: JSON.stringify(garageModel),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                    alertify.alert("Workshop Successfully Scheduled!");
            }
        });
    }
</script> 
</asp:Content>
