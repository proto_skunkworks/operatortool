﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.FuelRequestsModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Fueling History" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 

    <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.css" rel="stylesheet" />
    
      <script src="../../Scripts/plugins/iCheck/icheck.min.js"></script>

    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Fuel Request for <%: Html.DisplayTextFor(model => model.request_vregno) %></h3>
                </div>
                        <div class="container">              
                             
                          <div class="form-group">
                                    <label for="exampleInputEmail1">ORDER ID</label>
                                    <%: Html.DisplayFor(model => model.order_id, new { @class = "form-control", @style = "width:30%" }) %>
                          </div> 
                             <div class="form-group">
                                    <label for="exampleInputEmail1">DATE REQUESTED:</label>
                                    <%: Html.DisplayFor(model => model.date_requested, new { @class = "form-control", @style = "width:30%" }) %>
                          </div>
                               <input type="hidden" class="form-control" name="UserID" id="order_id" value="<%: Model.order_id %>" />
                                <div class="form-group">
                                <label for="exampleInputEmail1">Driver: </label>
                                    <%: Html.DisplayFor(model => model.driver_name, new { @class = "form-control", @style = "width:30%" }) %>
                          </div> 
                          <div class="form-group">
                                    <label for="exampleInputEmail1">CURRENT LEVEL:</label>
                                    <%: Html.DisplayFor(model => model.request_quantity, new { @class = "form-control", @style = "width:30%" }) %>
                          </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">MILEAGE:</label>
                                    <%: Html.DisplayFor(model => model.request_mileage, new { @class = "form-control", @style = "width:30%" }) %>
                                 
                             </div>
                                 <div class="form-group">
                                    <label for="exampleInputEmail1">ENTER QUANTITY TO APPROVE</label>
                                     <input type="text" class="form-control" id="approved_quantity" style=" width:20%" /> 
                                   
                             <div class="form-group">
                                   FULL TANK <input type="checkbox" id="fullTank" >
                                   </div>  
                                 <button type="button" class="btn btn-success" id="btnApprove" onclick="ApproveFueling();">Approve</button>
                             <button type="button" class="btn btn-danger" id="btnReject" onclick="RejectFueling();">Reject</button>                           
                       </div>             
                    
            </div>
                
                                
        </div>
            <!-- /.box -->

        </div>
         <div class="row"  align="center">
         <a class="btn btn-xs btn-primary" href="/Assets/fuelrequests"><i class="fa fa-angle-double-left" >Back</i></a>
        </div>

    <link href="../../Scripts/plugins/iCheck/all.css" rel="stylesheet" />

<script type="text/javascript">




 $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
 
        function ApproveFueling() {  
     
        var approved_quantity=$('#approved_quantity').val();
            var fullTank = $('#fullTank:checked').val() ? true : false;
            console.log(approved_quantity);
if($.trim(approved_quantity)=="" && fullTank==false)
			{
    alertify.alert('Message', 'You must enter the quantity before approving.', function () { alertify.message('Ok'); });
    return;
			}
if(parseInt(approved_quantity)<10)
{
    alertify.alert('Message', 'You cannot fuel less than 10 litres.', function () { alertify.message('Ok'); });
    return;
}

        if (fullTank == true) {
            approved_quantity = "Full Tank";
        }
        else {
            approved_quantity = approved_quantity;
        }
     var empObj = {
          order_id: $('#order_id').val(),
         approved_quantity:approved_quantity,         
        }; 
        
    console.log(empObj);
    $.ajax({  
        url: "/Assets/ApproveFueling",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alertify.alert('Message', result.responseText, function () { alertify.message('Ok'); });
        },  
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });  
 }

function RejectFueling() {  
    
     var empObj = {
          order_id: $('#order_id').val(),       
    };  
    console.log(empObj);
    $.ajax({  
        url: "/Assets/RejectFueling",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alertify.alert('Message', result.responseText, function () { alertify.message('Ok'); });
        },  
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });  
 }
</script> 

</asp:Content>
