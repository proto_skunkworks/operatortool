﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.ActiveTripsModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Active Trips" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="content-wrapper">    

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>                                
                                <th>Vehicle</th>
                                <th>Driver Name</th>                               
                                <th>Truck Helper</th>
                                <th>Routes</th>
                                <th>Date Planned</th>
                                <th>Category</th>
                                 <th>Destination</th>
                                <th>Time Taken</th>
                                 <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                                    <tr>
                                    <td><%:Html.DisplayFor(m =>item.vregno) %>
                                    <td><%:Html.DisplayFor(m =>item.driver_name) %>
                                    <td><%:Html.DisplayFor(m =>item.truck_helper) %>
                                    <td><%:Html.DisplayFor(m =>item.routes) %>
                                    <td><%:Html.DisplayFor(m =>item.planned_date) %>
                                    <td><%: Html.DisplayFor(m => item.Category) %></td>
                                    <td><%: Html.DisplayFor(m => item.destination) %></td>
                                    <td><%: Html.DisplayFor(m => item.time_taken) %></td>
                                    <td><a class="btn btn-xs btn-primary" href="/planning/edit_trip/<%: Html.DisplayFor(m => item.trip_id) %>">EDIT<i class="fa fa-edit"></i></a>                          
                                    </tr>
                            <% } %>
                        </tbody>
                       
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

     <style>           

#example1 {
width:100%;
   font-size:12px;
      table-layout:fixed;     
      
}
 </style>
     </div>
</asp:Content>
