﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.DashboardModels>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Overview" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
     <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-compass"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Active Trips</span>
              <span class="info-box-number">10</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-suitcase"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Closed Trips</span>
              <span class="info-box-number">65</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
           <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">All Trips</span>
              <span class="info-box-number">65</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         
          </div>
          <div class="row">
         <div class="col-md-6 col-sm-6 col-xs-12">
             <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Trips</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Trip ID</th>
                    <th>VReg No</th>
                    <th>Loading Source</th>
                    <th>Destination</th>
                    <th>Date Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">NEW TRIP</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Trips</a>
            </div>
                 </div>
    </div>
</div>
 </section>
<link href="../../Scripts/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Scripts/bower_components/adminlite.css" rel="stylesheet" />
</asp:Content>
