﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.DashboardModels>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Dashboard" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/jquery.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script src="../../Scripts/bootstrap.min.js"></script>
    <link href="../../Scripts/bootstrap.min.css" rel="stylesheet" />
    
    <script src="../../Scripts/jquery.min.js"></script>
<!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner"> Routes
                   <h5>Price List </h5>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
                <a href="/Sales/PriceList" class="small-box-footer">PriceList <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner"> 8453
                      <h5>Customers </h5>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
                <a href="/Customers/CustomerList" class="small-box-footer">Customers <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner"> 65
                        <h5>Allocations </h5>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="/Drivers/DriversList" class="small-box-footer">Drivers <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner"> Truck sales
                      <h5>Sales </h5>
                </div>
                <div class="icon">
                    <i class="fa fa-truck"></i>
                </div>
                <a href="/Sales/Sales" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->

    <!-- top row -->
    <div class="row">
      <div id="chartdiv"></div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
 <script>
     $(document).ready(function () {
         var dataSource = GetTerritorySales();

         var chart = AmCharts.makeChart("chartdiv", {
             "type": "serial",
             "theme": "light",
             "marginRight": 70,
             "dataProvider": dataSource,
             "valueAxes": [{
                 "axisAlpha": 0,
                 "position": "left",
                 "title": "Territory Summary"
             }],
             "startDuration": 1,
             "graphs": [{
                 "balloonText": "<b>[[TerritoryName]]: [[TerritorySales]]</b>",
                 "fillColorsField": "color",
                 "labelText": "[[TerritorySales]]",
                 "fillAlphas": 0.9,
                 "lineAlpha": 0.2,
                 "type": "column",
                 "valueField": "TerritorySales"
             }],
             "chartCursor": {
                 "categoryBalloonEnabled": false,
                 "cursorAlpha": 0,
                 "zoomable": false
             },
             "categoryField": "TerritoryName",
             "categoryAxis": {
                 "gridPosition": "start",
                 "labelRotation": 45
             },
             "export": {
                 "enabled": true
             }

         });


         function GetTerritorySales() {
             var dataOutput;
             $.ajax({
                 type: "post",
                 url: "/Sales/getTerritorySummary",                
                 dataType: "json",
                 //     data: myusername,           
                 contentType: "application/json; charset=utf-8",
                 async: false,
                 success: function (data) {
                     dataOutput = data;
                 }
             });
             return dataOutput;
         }
     });

</script>
    <style>
#chartdiv {
  width: 70%;
  height: 400px;
}

.amcharts-export-menu-top-right {
  top: 10px;
  right: 0;
}
</style>
</asp:Content>
