﻿using OperatorToolSpace.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace OperatorToolSpace.Services
{
    public class PlanningSQL
    {
        string DBConn = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
        public class TruckDataModel
        {
            public string vregno { get; set; }
            public int id { get; set; }
        }
        public List<TruckDataModel> TruckRegsData()
        {
            List<TruckDataModel> __Trucks = new List<TruckDataModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("Select VID,VRegNo from tblVehicles", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TruckDataModel __trucks = new TruckDataModel()
                        {
                            vregno = reader["VRegNo"].ToString(),
                            id = Convert.ToInt32(reader["VID"].ToString()),

                        };
                        __Trucks.Add(__trucks);
                    }

                }
            }
            return __Trucks;
        }

        public class VehicleAssignments
        {
            public string VID { get; set; }
            public string vregno { get; set; }
            public string DriverName { get; set; }
            public string TruckHelper { get; set; }
            public string routes { get; set; }
            public string driver_id { get; set; }
        }
        
        public List<VehicleAssignments> GetVehicleData()
        {
            List<VehicleAssignments> _Vdata = new List<VehicleAssignments>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetDrivers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        VehicleAssignments _data = new VehicleAssignments()
                        {
                            DriverName = reader["FirstName"].ToString() + " " + reader["LastName"].ToString(),
                            vregno = reader["VRegNo"].ToString(),
                            routes = reader["CurrentRoutes"].ToString().Trim(),
                            TruckHelper = reader["truckhelper"].ToString().Trim(),
                            VID = reader["VID"].ToString().Trim(),
                            driver_id = reader["DriverID"].ToString().Trim(),
                        };
                        _Vdata.Add(_data);
                    }
                }
            }

            return _Vdata;

        }
        public class LastLoadedQuantity
        {
            public string kgs_6 { get; set; }
            public string kgs_13 { get; set; }
            public string kgs_50 { get; set; }
            public string grills { get; set; }
            public string burners { get; set; }
        }
   
        public List<LastLoadedQuantity> GetLastLoadedQuantity(int vid)
        {
           var QData = new List<LastLoadedQuantity>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select top 1 planned_6KG,planned_13KG,planned_50KG,Burners,Grills  from tbl_trips_planning where vid = '"+vid+"' " +
                    "order by date_added desc", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        LastLoadedQuantity _data = new LastLoadedQuantity()
                        {
                            kgs_6 = reader["planned_6KG"].ToString(),
                            kgs_13 = reader["planned_13KG"].ToString(),
                            kgs_50 = reader["planned_50KG"].ToString().Trim(),
                            burners = reader["Burners"].ToString().Trim(),
                            grills = reader["Grills"].ToString().Trim(),
                        };
                        QData.Add(_data);
                    }
                }
            }
            return QData;
        }
        public List<TruckregsModel> GetTruckAssignment()
        {
            List<TruckregsModel> __Trucks = new List<TruckregsModel>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT VID,VRegNO FROM tblVehicles WHERE vid NOT IN (SELECT vid FROM tbl_trips_planning WHERE status = 1 or status=0) " +
                    "and VID  in (SELECT vid FROM tblVehicleAssignments WHERE status = 1) and UsageType='1'", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TruckregsModel __trucks = new TruckregsModel()
                        {
                            Truckreg = reader["VRegNo"].ToString(),
                            id = Convert.ToInt32(reader["VID"].ToString()),

                        };
                        __Trucks.Add(__trucks);
                    }

                }
            }
            return __Trucks;
        }



        public int SaveTripsPlanning(TripsPlanningModel data)
        {
            bool exists = false;
            int i = 0;
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            string trip_id= Convert.ToInt64((DateTime.Now.ToUniversalTime() - epoch).TotalSeconds).ToString();
            var _DriverData = new DriversService();
            var _Vdata = _DriverData.GetDriversList().Where(s=>s.VID==data.vid).ToList();
            string driver_id = _Vdata[0].DriverID.ToString();
            string truck_helper = _Vdata[0].truck_helper.ToString();
            string route_names = _Vdata[0].CurrentRoute.ToString();
            string vregno = _Vdata[0].Truckreg.ToString();
            string driver_name = _Vdata[0].FirstName.ToString().Trim() +" "+ _Vdata[0].LastName.ToString().Trim();
            string sql = "SELECT * from tbl_trips_planning where vid='"+data.vid.Trim()+"' and cast(plan_date as date)='"+data.planned_date.Date.ToString("yyyy-MM-dd")+"'AND (status='0' or status='1')";
                try
                {
                exists = checkrecord(sql);
                if (exists == false)
                {
                    using (SqlConnection con = new SqlConnection(DBConn))
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("Insert into tbl_trips_planning(trip_id,driver_id,vid,truck_helper,Category,destination,route_names,planned_6KG,planned_13KG,planned_50KG,Burners,Grills,plan_date)" +
                            "VALUES(@trip_id,@driver_id,@vid,@truck_helper,@Category,@destination,@route_names,@planned_6KG,@planned_13KG,@planned_50KG,@Burners,@Grills,@plan_date)", con);
                        com.CommandType = CommandType.Text;
                        com.Parameters.AddWithValue("@trip_id", trip_id.Trim());
                        com.Parameters.AddWithValue("@driver_id", driver_id.Trim());
                        com.Parameters.AddWithValue("@vid", data.vid.Trim());
                        com.Parameters.AddWithValue("@truck_helper", truck_helper.Trim());
                        com.Parameters.AddWithValue("@Category", data.Category.Trim());
                        com.Parameters.AddWithValue("@destination", data.destination.Trim());
                        com.Parameters.AddWithValue("@route_names", route_names.Trim());
                        com.Parameters.AddWithValue("@planned_6KG", data.kg6_refill.Trim());
                        com.Parameters.AddWithValue("@planned_13KG", data.kg13_refill.Trim());
                        com.Parameters.AddWithValue("@planned_50KG", data.kg50_refill.Trim());
                        com.Parameters.AddWithValue("@plan_date", data.planned_date);
                        com.Parameters.AddWithValue("@Burners", data.burners.Trim());
                        com.Parameters.AddWithValue("@Grills", data.grills.Trim());
                        i = com.ExecuteNonQuery();
                    }
                }
                else
                {
                    i = 2;
                }
                //run in task to release cursor...
                Task.Run(() => sendWhatsAppMessage("PLANNING\n*Driver: " + driver_name + "*\nTruck: " + vregno + "\nTruck Helper:" + truck_helper + " \nRoute:" + route_names + "\nDate:" + data.planned_date.ToShortDateString()));
                }
           
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }        
            return i;

        }

        //check if record exists

        public bool checkrecord(string sql)
        {
            bool exists = false;
            try
            {
              
                using (SqlConnection con = new SqlConnection(DBConn))
                {

                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);
                    com.CommandType = CommandType.Text;
                    SqlDataReader reader = com.ExecuteReader();
                    if (reader.HasRows)
                    {
                        exists = true;
                    }
                    else
                    {
                        exists = false;
                    }

                }
            }
            catch (Exception ex)
            {
                exists = false;
                //ErrorLogger.writeLogs("checkrecord " + ex.Message);
            }
            return exists;
        }
        class WhatsAppDataModel
        {
            public string chatId { get; set; }
            public string body { get; set; }
        }//model class
        public void sendWhatsAppMessage(string msgBody)
        {
            var obj = new WhatsAppDataModel
            {
               // chatId = "254727337354-1522302786@g.us",
                chatId = "254728719865@c.us",
                body = msgBody,
            };
            var json = new JavaScriptSerializer().Serialize(obj);
            WhatsappSender.SendReport(json);
        }
        public List<ActiveTripsModel> GetActiveTrips()
        {
            List<ActiveTripsModel> _activeTrips = new List<ActiveTripsModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetActiveTrips", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int _categoryid = Convert.ToInt32(reader["Category"].ToString());
                        Tripcategory _category = (Tripcategory)_categoryid;
                        Status _status = (Status)Convert.ToInt32(reader["status"].ToString());
                        ActiveTripsModel __trucks = new ActiveTripsModel()
                        {
                            trip_id = reader["trip_id"].ToString(),
                            destination = reader["destination"].ToString(),                           
                            driver_phone = reader["PhoneNumber"].ToString(),
                            driver_name = reader["FirstName"].ToString()+" " + reader["LastName"].ToString(),                           
                            vregno = reader["VRegNo"].ToString(),                        
                            routes = reader["route_names"].ToString(),
                            truck_helper = reader["truckhelper"].ToString(),
                            Category = _category.ToString(),
                            trip_cost = reader["trip_cost"].ToString(),
                            time_taken = reader["time_taken"].ToString(),
                            planned_date = Convert.ToDateTime(reader["planned_date"].ToString()),
                            status = _status.ToString(),

                        };
                        _activeTrips.Add(__trucks);
                    }

                }
            }
            return _activeTrips;
        }

      
        public List<TripsDownloadModel> TripsDownload()
        {
            var _downloads = new List<TripsDownloadModel>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetTripsPlanning", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TripsDownloadModel __trucks = new TripsDownloadModel()
                        {
                            trip_id = reader["trip_id"].ToString(),
                            vregno = reader["vregno"].ToString(),
                            vtype = reader["vtype"].ToString(),
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            DriverName = reader["DriverName"].ToString(),
                            planned_6KG = reader["planned_6KG"].ToString(),
                            planned_13KG = reader["planned_13KG"].ToString(),
                            planned_50KG = reader["planned_50KG"].ToString(),
                            truck_helper = reader["truck_helper"].ToString(),
                            route_names = reader["route_names"].ToString(),
                            Burners = reader["Burners"].ToString(),
                            Grills = reader["Grills"].ToString(),
                            date_added = Convert.ToDateTime(reader["date_added"].ToString()),
                        };
                        _downloads.Add(__trucks);
                    }

                }
            }
            return _downloads;
        }
        public PlanningDashBoard GetPlanningDashBoard()
        {
          var _activeTrips = new PlanningDashBoard();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("PlanningDashBoard", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        _activeTrips.unplanned = reader["unplanned"].ToString();
                        _activeTrips.Active = reader["Active"].ToString();
                        _activeTrips.Completed = reader["Completed"].ToString();
                        _activeTrips.Pending = reader["Pending"].ToString();
                        _activeTrips.all_trips = reader["all_trips"].ToString();

                    }

                }
            }
            return _activeTrips;
        }
        private enum Tripcategory
        {
            Sales = 1,
            Replenishment = 2,
        }

        private enum Status
        {
            Pending = 0,
            Active = 1,
            Completed = 2,
        }


        


        public List<TripsPreplanning> Preplanning()
        {
            var _downloads = new List<TripsPreplanning>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("PrePlanning", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@date",dt);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TripsPreplanning __trucks = new TripsPreplanning()
                        {
                            driver_name = reader["FirstName"].ToString() +" " + reader["LastName"].ToString(),
                            vregno = reader["vregno"].ToString(),
                            phone_number = reader["PhoneNumber"].ToString(),
                            routes = reader["CurrentRoutes"].ToString(),
                            status = reader["status"].ToString(),
                            truck_helper = reader["truckhelper"].ToString(),
                            vid = Convert.ToInt32(reader["VID"].ToString()),
                            date_added = Convert.ToDateTime(reader["plan_date"].ToString()),
                        };
                        _downloads.Add(__trucks);
                    }

                }
            }
            return _downloads;
        }


        //this is for planning
     

        public List<UnloadedVehicles> GetUnplannedVehicles(DateTime date)
        {
            List<UnloadedVehicles> _Unplanned = new List<UnloadedVehicles>();          
                using (SqlConnection Conn = new SqlConnection(DBConn))
                {
                    string sql = "GetUnplannedTrucks";
                    using (SqlCommand Cmd = new SqlCommand(sql, Conn))
                    {
                        Conn.Open();
                        Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        Cmd.Parameters.AddWithValue("@date",date);
                        SqlDataReader reader = Cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            UnloadedVehicles _data = new UnloadedVehicles()
                            {
                                driver_name = reader["FirstName"].ToString().Trim() + " " + reader["LastName"].ToString().Trim(),
                                vregno = reader["vregno"].ToString().Trim(),
                                driver_id = Convert.ToInt32(reader["DriverID"].ToString().Trim()),
                                vid = Convert.ToInt32(reader["vid"].ToString().Trim()),
                                truckhelper = reader["truckhelper"].ToString().Trim(),
                                currentroutes = reader["currentroutes"].ToString().Trim(),
                                phone_number = reader["PhoneNumber"].ToString().Trim(),

                            };
                            _Unplanned.Add(_data);
                        }
                    }
                }

           
            return _Unplanned;
        }
    }
}