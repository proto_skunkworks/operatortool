﻿using OperatorToolSpace.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace OperatorToolSpace.Services
{
    public class Fuelsql
    {
        string DBConn = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
        public List<FuelRequestsModel> GetFuelRequests()
        {
            List<FuelRequestsModel> _requests = new List<FuelRequestsModel>();
            using (SqlConnection Conn = new SqlConnection(DBConn))
            {
                string sql = "GetFuelRequests";

                using (SqlCommand Cmd = new SqlCommand(sql, Conn))
                {
                    Conn.Open();
                    Cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    SqlDataReader reader = Cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int request_status = Convert.ToInt32(reader["status"].ToString());
                        FuelRequestStatus _status = (FuelRequestStatus)request_status;
                        string strStatus = _status.ToString();
                        FuelRequestsModel _data = new FuelRequestsModel()
                        {
                            request_status = strStatus,
                            order_id = Convert.ToInt32(reader["order_id"].ToString().Trim()),
                            driver_id = reader["driver_id"].ToString().Trim(),
                            driver_name = reader["driver_name"].ToString().Trim(),
                            request_mileage = reader["request_mileage"].ToString().Trim(),
                            request_quantity = reader["request_quantity"].ToString().Trim(),
                            request_vregno = reader["vregno"].ToString().Trim(),
                            date_requested = reader["date_requested"].ToString().Trim(),
                            fueling_station = reader["fueling_station"].ToString().Trim(),
                            // approved_quantity = reader["approved_quantity"].ToString()
                        };
                        _requests.Add(_data);
                    }

                }

            }
            return _requests;
        }
        public int SaveFuelRequest(FuelRequestsModel requests)
        {
            int i = 0;
            try
            {
                TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
                int order_id = (int)t.TotalSeconds;
                using (SqlConnection con = new SqlConnection(DBConn))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("INSERT INTO tbl_fuel_requests([order_id],[driver_id],[vregno],[request_mileage],[request_quantity],[fueling_station]) VALUES(" +
                        "@order_id,@driver_id,@vregno,@request_mileage,@request_quantity,@fueling_station)", con);
                    com.CommandType = CommandType.Text;
                    com.Parameters.AddWithValue("@order_id", order_id);
                    com.Parameters.AddWithValue("@driver_id", requests.driver_id);
                    com.Parameters.AddWithValue("@vregno", requests.request_vregno);
                    com.Parameters.AddWithValue("@request_mileage", requests.request_mileage);
                    com.Parameters.AddWithValue("@request_quantity", requests.request_quantity);
                    com.Parameters.AddWithValue("@fueling_station", requests.fueling_station);
                    i = com.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                i = 0;
                // ErrorLogger.writeLogs(ex.Message);
            }
            return i;
        }


        public int closerequest(FuelRequestsModel requests)
        {
            int i = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(DBConn))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("UPDATE tbl_fuel_requests set fueling_quantity=@fueling_quantity , fueling_mileage=@fueling_mileage ,Status='3',date_closed=@date_closed where order_id='" + requests.order_id + "'", con);
                    com.CommandType = CommandType.Text;
                    com.Parameters.AddWithValue("@fueling_mileage", requests.fueling_mileage);
                    com.Parameters.AddWithValue("@fueling_quantity", requests.fueling_quantity);
                    com.Parameters.AddWithValue("@date_closed", DateTime.Now);
                    i = com.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                i = 0;
                // ErrorLogger.writeLogs(ex.Message);
            }
            return i;
        }

        public enum FuelRequestStatus
        {
            Pending = 0,
            Approved = 1,
            Rejected = 2,
            Closed = 3,
        }

        public int ApproveFueling(string order_id, string approved_quantity,string UserID)
        {  
                int i = 0;
                try
                {
               // string strSession = HttpContext.Current.Session.SessionID;
                    using (SqlConnection con = new SqlConnection(DBConn))
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("UPDATE tbl_fuel_requests set approved_quantity=@approved_quantity,status='1',approved_by=@approved_by,date_approved=@date_approved where order_id='" + order_id + "'", con);
                        com.CommandType = CommandType.Text;
                        com.Parameters.AddWithValue("@approved_quantity", approved_quantity);
                        com.Parameters.AddWithValue("@date_approved", DateTime.Now);
                        com.Parameters.AddWithValue("@approved_by", UserID);
                         i = com.ExecuteNonQuery();
                        SendSMS(order_id, approved_quantity);
                }
                }
                catch (Exception ex)
                {
                Console.WriteLine(ex.Message);
                    i = 0;                    
                }
                return i;
        }
        public int RejectFueling(string order_id)
        {
            {
                int i = 0;
                try
                {
                    using (SqlConnection con = new SqlConnection(DBConn))
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("UPDATE tbl_fuel_requests set approved_quantity='0',status='2',date_approved=@date_approved where order_id='" + order_id + "'", con);
                        com.CommandType = CommandType.Text;                       
                        com.Parameters.AddWithValue("@date_approved", DateTime.Now);
                        i = com.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    i = 0;
                    Console.WriteLine(ex.Message);
                }
                return i;
            }
        }
        public void SendSMS(string order_id,string quantity)
        {
            SMSSender SMS = new SMSSender();
            using (SqlConnection Conn = new SqlConnection(DBConn))
            {
                string phone_number="";
                string fueling_station = "";
                string approved_quantity = "";
                string driver_name="";
                string request_mileage = "";
                string vregno = "";
                string sql = "select PhoneNumber,FirstName,request_mileage,LastName,fueling_station,approved_quantity,vregno from tblDrivers d  inner join tbl_fuel_requests f " +
                    "on f.driver_id = d.DriverID where f.order_id = '"+order_id+"'";
                using (SqlCommand Cmd = new SqlCommand(sql, Conn))
                {
                    Conn.Open();
                    Cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = Cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        driver_name = reader["FirstName"].ToString().Trim() +" "+reader["LastName"].ToString().Trim();
                        phone_number = reader["PhoneNumber"].ToString().Trim();
                        vregno = reader["vregno"].ToString().Trim();
                        fueling_station = reader["fueling_station"].ToString().Trim();                        
                        approved_quantity = reader["approved_quantity"].ToString().Trim();
                    }
                }
                EmailSender.SendEmail(order_id, fueling_station, DateTime.Now, vregno, request_mileage, approved_quantity, driver_name);
                string _msg = "Dear "+driver_name +",\n"+"Your fuel request for "+vregno+" has been approved. \nApproved Quantity:"+approved_quantity+"\n Use your app to close the order on fueling.";
                if(phone_number!="" && _msg!="")
                {
                    SMS.sendSMS(phone_number, _msg);
                }
            }
        }
    }
}
