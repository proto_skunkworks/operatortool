﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.Web.Mvc;
using System.IO;
using OperatorToolSpace.Models;
using System.Data;

namespace OperatorToolSpace.Services
{
    public class DriversService
    {
        //Db Connection string
        string DBCon = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;

        SystemTools _SystemTools = new SystemTools();
        
        //Save use to the database
        public string CreateDriver(AddDriverModel entity)
        {
            string rs = "";
            try
            {
                using (SqlConnection conn = new SqlConnection(DBCon))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SaveDriver", conn);//call Stored Procedure
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", entity.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", entity.LastName);
                    cmd.Parameters.AddWithValue("@PhoneNumber", entity.PhoneNumber);
                    cmd.Parameters.AddWithValue("@Email", entity.Email);
                    cmd.Parameters.AddWithValue("@UserType", entity.UserType);
                    cmd.Parameters.AddWithValue("@Password", _SystemTools.EncryptPass(entity.Password));
                    rs = cmd.ExecuteNonQuery().ToString();
                    return rs;
                }
            }

            catch (Exception ex)
            {
                return rs = ex.Message;
            }
        }

        //View
        public List<DriverListModel> GetDriversList()
        {
            List<DriverListModel> _DriverList = new List<DriverListModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("GetDrivers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DriverListModel _data = new DriverListModel()
                        {
                            DriverID = int.Parse(reader["DriverID"].ToString()),
                            VID =reader["VID"].ToString(),
                            NationalID = reader["DriverID"].ToString(),
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            Email = reader["Email"].ToString(),
                            Truckreg = reader["VRegNo"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            CurrentRoute = reader["CurrentRoutes"].ToString(),
                            truck_helper = reader["truckhelper"].ToString(),
                            CurrentTill = reader["TillNumber"].ToString(),
                        };



                        _DriverList.Add(_data);
                    }
                }
            }

            return _DriverList;

        }

        public List<EditDriverModel> GetEditDriver()
        {
            List<EditDriverModel> _DriverList = new List<EditDriverModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("GetDrivers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        EditDriverModel _data = new EditDriverModel()
                        {
                            DriverID = int.Parse(reader["DriverID"].ToString()),                          
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            Email = reader["Email"].ToString(),
                            Truckreg = reader["VRegNo"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            CurrentRoute = reader["CurrentRoutes"].ToString(),
                            CurrentTill = reader["TillNumber"].ToString(),
                        };



                        _DriverList.Add(_data);
                    }
                }
            }

            return _DriverList;

        }

        public int DeactivateActivateUser(DriverListModel model)
        {
            using (SqlConnection conn = new SqlConnection(DBCon))
            {               
                conn.Open();
                SqlCommand cmd = new SqlCommand("DeactivateActivateUser", conn);//call Stored Procedure
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", model.DriverID);
                cmd.Parameters.AddWithValue("@Operation", model.DriverID);                
                int rs = cmd.ExecuteNonQuery();

                return rs;

            }
        }

        //View Active users list
        public List<DriverListModel> ActiveUsersList()
        {
            List<DriverListModel> _UsersListModel = new List<DriverListModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("GetActiveUsers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DriverListModel __UsersListModel = new DriverListModel()
                        {
                            DriverID = int.Parse(reader["DriverID"].ToString()),
                            NationalID = reader["DriverID"].ToString(),
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            Truckreg = reader["VRegNo"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            CurrentRoute = reader["CurrentRoutes"].ToString(),
                            CurrentTill = reader["TillNumber"].ToString(),

                        };


                        _UsersListModel.Add(__UsersListModel);
                    }
                }
            }

            return _UsersListModel;

        }

        //View InActive users list
        public List<DriverListModel> InActiveUsersList()
        {
            List<DriverListModel> _UsersListModel = new List<DriverListModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("GetInActiveUsers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DriverListModel __UsersListModel = new DriverListModel()
                        {
                            DriverID = int.Parse(reader["DriverID"].ToString()),
                            NationalID = reader["DriverID"].ToString(),
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            Truckreg = reader["VRegNo"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            CurrentRoute = reader["CurrentRoutes"].ToString(),
                            CurrentTill = reader["TillNumber"].ToString(),

                        };

                        _UsersListModel.Add(__UsersListModel);
                    }
                }
            }

            return _UsersListModel;

        }

        //Remove User
        public int RemoveDriver(DriverListModel model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(DBCon))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("RemoveDriver", conn);//call Stored Procedure
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DriverID", model.DriverID);
                    int rs = cmd.ExecuteNonQuery();

                    return rs;

                }
            }
            catch
            {
                return 0;
            }
        }
        public List<TruckregsModel> TruckRegsData()
        {
            List<TruckregsModel> __Trucks = new List<TruckregsModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT VID,VRegNO FROM tblVehicles WHERE VID NOT IN  (SELECT VID     FROM tblVehicleAssignments WHERE STATUS = '1')", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TruckregsModel __trucks = new TruckregsModel()
                        {
                            Truckreg = reader["VRegNo"].ToString(),
                            id = Convert.ToInt32(reader["VID"].ToString()),

                        };
                        __Trucks.Add(__trucks);
                    }

                }
            }
            return __Trucks;
        }

        public List<RoutesModel> GetRoutes()
        {
            List<RoutesModel> _Routes = new List<RoutesModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("select RID,RName from tblRoutes", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        RoutesModel __routes = new RoutesModel()
                        {
                            RouteName = reader["RName"].ToString(),
                            id = Convert.ToInt32(reader["RID"].ToString()),

                        };
                        _Routes.Add(__routes);
                    }

                }
            }
            return _Routes;
        }

        public List<ContainersDataModel> GetContainers()
        {
            List<ContainersDataModel> _containers = new List<ContainersDataModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("select id,ContainerName from tblContainers", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ContainersDataModel _conts = new ContainersDataModel()
                        {
                            ContainerName = reader["ContainerName"].ToString(),
                            id = Convert.ToInt32(reader["id"].ToString()),

                        };
                        _containers.Add(_conts);
                    }

                }
            }
            return _containers;
        }
        public List<TruckHelperModel> GetTruckHelper()
        {
            List<TruckHelperModel> _THelper = new List<TruckHelperModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("select * from tbl_truck_helpers where Status='0'", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TruckHelperModel _data = new TruckHelperModel()
                        {
                            truck_helper = reader["first_name"].ToString()+ " "+reader["last_name"].ToString(),
                            thelper_id = Convert.ToInt32(reader["thelper_id"].ToString()),
                        };
                        _THelper.Add(_data);
                    }
                }
            }
            return _THelper;
        }
        public  int AssignRoutes(string[] Routes, string DriverID)
        {
            int i = 0;
            DeassignRoutes( DriverID);
            foreach (string _route in Routes)
            {
                using (SqlConnection con = new SqlConnection(DBCon))
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("Insert into tblRouteAssignments(RID,DriverID,Status)VALUES('" + _route + "','" + DriverID + "','1')", con);
                    com.CommandType = CommandType.Text;
                    i = com.ExecuteNonQuery();
                    i = 0;
                }
            }
            return i;
        }
        public int DeassignRoutes(string DriverID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(DBCon))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Update tblRouteAssignments set Status ='0' where Status='1'AND DriverID='" + DriverID + "'", con);
                com.CommandType = CommandType.Text;
                i = com.ExecuteNonQuery();               
            }
            return i;
        }
        public List<DriverListModel> ArchivedUsersList()
        {
            List<DriverListModel> _UsersListModel = new List<DriverListModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("GetArchivedUsers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DriverListModel __UsersListModel = new DriverListModel()
                        {
                            DriverID = int.Parse(reader["DriverID"].ToString()),
                            NationalID = reader["DriverID"].ToString(),
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            FirstName = reader["FirstName"].ToString(),
                            Truckreg = reader["VRegNo"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            CurrentRoute = reader["CurrentRoutes"].ToString(),
                            CurrentTill = reader["TillNumber"].ToString(),

                        };

                        _UsersListModel.Add(__UsersListModel);
                    }
                }
            }

            return _UsersListModel;

        }

        //View user details list
        public List<EditUserModel> ViewUserDetailsList()
        {
            List<EditUserModel> _EditUserModel = new List<EditUserModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("GetUsers", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        EditUserModel __EditUserModel = new EditUserModel();
                        __EditUserModel.UserID = int.Parse(reader["UserID"].ToString());
                        __EditUserModel.Firstname = reader["Firstname"].ToString();
                        __EditUserModel.Middlename = reader["Middlename"].ToString();
                        __EditUserModel.Lastname = reader["Lastname"].ToString();
                        __EditUserModel.Gender = reader["Gender"].ToString();
                        __EditUserModel.Email = reader["Email"].ToString();
                        __EditUserModel.Phone = reader["Phone"].ToString();
                        __EditUserModel.Position = reader["Position"].ToString();
                        __EditUserModel.Image = (byte[])reader["Image"];

                        _EditUserModel.Add(__EditUserModel);
                    }
                }
            }

            return _EditUserModel;

        }

        //Update user to the database
        public int UpdateUser( EditDriverModel entity)
        {
            try
            {


                using (SqlConnection conn = new SqlConnection(DBCon))
                {

                    conn.Open();
                    SqlCommand cmd = new SqlCommand("EditDriver", conn);//call Stored Procedure
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DriverID", entity.DriverID);
                    cmd.Parameters.AddWithValue("@FirstName", entity.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", entity.LastName);
                    cmd.Parameters.AddWithValue("@Email", entity.Email);
                    cmd.Parameters.AddWithValue("@PhoneNumber", entity.PhoneNumber);                    
                    cmd.Parameters.AddWithValue("@Password", _SystemTools.EncryptPass(entity.Password));
                    int rs = cmd.ExecuteNonQuery();

                    return rs;

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public  int AssignVehicle(PlanningDataModel planning)
        {
            int i;
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon))
                {
                    con.Open();
                    DeassignVehicle(planning.DriverID);
                    //check any pending or active trip----with ths change drivers id and
                    UpdateActiveTrip(planning.Truckreg,planning.DriverID);
                    SqlCommand com = new SqlCommand("INSERT INTO tblVehicleAssignments (VID,DriverID,Status,truckhelper)" +
                        "VALUES(@VID,@DriverID,@Status,@truckhelper)", con);
                    com.Parameters.AddWithValue("@DriverID", planning.DriverID);                    
                    com.Parameters.AddWithValue("@VID", planning.Truckreg);                    
                    com.Parameters.AddWithValue("@truckhelper", planning.truck_helper);                   
                    com.Parameters.AddWithValue("@Status", 1);
                    com.CommandType = CommandType.Text;
                    i = com.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                i = 0;
                Console.WriteLine(ex.Message);

            }
            return i;
        }

        public int DeassignVehicle(string DriverID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(DBCon))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Update tblVehicleAssignments set Status ='0' where Status='1'AND DriverID='"+DriverID+"'", con);
                com.CommandType = CommandType.Text;
                i = com.ExecuteNonQuery();              
            }
            return i;
        }


        public int UpdateActiveTrip(string vid,string DriverID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(DBCon))
            {
                con.Open();
                SqlCommand com = new SqlCommand("Update tbl_trips_planning set driver_id =@driver_id where Status!='2'AND vid=@vid", con);
                com.Parameters.AddWithValue("@vid",vid.Trim());
                com.Parameters.AddWithValue("@driver_id", DriverID.Trim());
                com.CommandType = CommandType.Text;
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}