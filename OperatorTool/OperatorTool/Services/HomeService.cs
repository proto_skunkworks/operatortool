﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using OperatorToolSpace.Models;

namespace OperatorToolSpace.Services
{
    public class HomeService
    {
        //Db Connection string
        string DBCon = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
        SystemTools _SystemTools = new SystemTools();

        public LoginResultModel UserAuthenticate(LoginModel model)
        {
            LoginResultModel __LoginResultModel = new LoginResultModel();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("AuthenticateUser", conn);//call Stored Procedure
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email", model.Email);
                cmd.Parameters.AddWithValue("@Password", _SystemTools.EncryptPass(model.Password));
                //cmd.Parameters.AddWithValue("@Password", model.Password);
                //int rs = cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    __LoginResultModel.UserID = int.Parse(reader["UserID"].ToString());
                    __LoginResultModel.Firstname = reader["FirstName"].ToString();
                    __LoginResultModel.Position = reader["Position"].ToString();
                    __LoginResultModel.Email = reader["Email"].ToString();
                }
                return __LoginResultModel;
            }

           
        }

        public DashboardModels DashboardStats()
        {
            DashboardModels _DashboardModels = new DashboardModels();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select * from tblDrivers", conn);//call Stored Procedure
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    _DashboardModels.UsersList = "30";
                    _DashboardModels.ActiveUsers = "3";
                    _DashboardModels.InActiveUsers = "23";
                    _DashboardModels.ArchivedUsers = "13";
                }

                return _DashboardModels;

            }
        }

    }
}