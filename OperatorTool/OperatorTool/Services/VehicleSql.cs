﻿using OperatorToolSpace.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Services
{
    public class VehicleSql
    {
        string DBCon = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
        public List<VehicleDataModel> GetVehicleList()
        {
            List<VehicleDataModel> _Vehicles = new List<VehicleDataModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from tblVehicles", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        VehicleDataModel _data = new VehicleDataModel()
                        {

                            VID = Convert.ToInt32(reader["VID"].ToString()),
                            VRegNo = reader["VRegNo"].ToString(),
                            AssetType = reader["VType"].ToString(),
                            CurrentDriver = "",
                            Capacity = Convert.ToInt32(reader["VCapacity"].ToString()),
                            Availability = reader["VStatus"].ToString(),
                           
                        };

                        _Vehicles.Add(_data);
                    }
                }
            }

            return _Vehicles;

        }
        public FuelingDashBoard GetFuelingDashBoard()
        {
            var _FuelingData = new FuelingDashBoard();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("FuelingDashBoard", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        _FuelingData.pending = Convert.ToInt32(reader["pending"].ToString());
                        _FuelingData.approved = Convert.ToInt32(reader["approved"].ToString());
                        _FuelingData.closed = Convert.ToInt32(reader["closed"].ToString());
                        _FuelingData.rejected = Convert.ToInt32(reader["rejected"].ToString());
                        
                    }

                }
            }
            return _FuelingData;
        }
        public List<VehicleDataModel> GetVehicleList(int id)
        {
            List<VehicleDataModel> _Vehicles = new List<VehicleDataModel>();

            using (SqlConnection conn = new SqlConnection(DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from tblVehicles where VID='"+id+"'", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        VehicleDataModel _data = new VehicleDataModel()
                        {

                            VID = Convert.ToInt32(reader["VID"].ToString()),
                            VRegNo = reader["VRegNo"].ToString(),
                            AssetType = reader["VType"].ToString(),
                            CurrentDriver = "",
                            Capacity = Convert.ToInt32(reader["VCapacity"].ToString()),
                            Availability = reader["VStatus"].ToString(),

                        };

                        _Vehicles.Add(_data);
                    }
                }
            }

            return _Vehicles;

        }

        //SaveGarageSchedule
    }
}