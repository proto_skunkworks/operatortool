﻿using OperatorToolSpace.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Services
{
    public class SalesSQLServices
    {
        string DBConn = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;

        public List<RoutePricesModel> GetRoutePrices()
        {
            List<RoutePricesModel> _Prices = new List<RoutePricesModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from tblPrices where Status='1'", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        RoutePricesModel __prices = new RoutePricesModel
                        {

                            RouteID = reader["id"].ToString(),
                            RName = reader["RName"].ToString(),
                            PriceCategory = reader["PriceCategory"].ToString(),
                            Refill6KG = reader["Refill6KG"].ToString(),
                            Refill13KG = reader["Refill13KG"].ToString(),
                            Refill50KG = reader["Refill50KG"].ToString(),
                            Outright6KG = reader["Outright6KG"].ToString(),
                            Outright13KG = reader["Outright13KG"].ToString(),
                            Outright50KG = reader["Outright50KG"].ToString(),
                            PriceBurner = reader["PriceBurner"].ToString(),
                            PriceGrill = reader["PriceGrill"].ToString(),
                            Status = Convert.ToInt32(reader["Status"].ToString()),
                        };
                        _Prices.Add(__prices);
                    }
                }
            }
            return _Prices;
        }

        public List<EditPricesModel> ViewPriceList()
        {
            List<EditPricesModel> _EditPricesModel = new List<EditPricesModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from tblPrices where Status='1'", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        EditPricesModel _data = new EditPricesModel()
                        {

                            RouteID = Convert.ToInt32(reader["id"].ToString()),
                            RName = reader["RName"].ToString(),
                            PriceCategory = reader["PriceCategory"].ToString(),
                            Refill6KG = reader["Refill6KG"].ToString(),
                            Refill13KG = reader["Refill13KG"].ToString(),
                            Refill50KG = reader["Refill50KG"].ToString(),
                            Outright6KG = reader["Outright6KG"].ToString(),
                            Outright13KG = reader["Outright13KG"].ToString(),
                            Outright50KG = reader["Outright50KG"].ToString(),
                            PriceBurner = reader["PriceBurner"].ToString(),
                            PriceGrill = reader["PriceGrill"].ToString(),
                            Status = Convert.ToInt32(reader["Status"].ToString()),
                        };
                        
                        _EditPricesModel.Add(_data);
                    }
                }
            }

            return _EditPricesModel;

        }
        
        public List<SalesDataModels> GetAllSales(DateTime ?StartDate, DateTime? EndDate)
        {
            List<SalesDataModels> _Sales = new List<SalesDataModels>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetSalesFilter", conn))//call Stored Procedure
                {
                    if (StartDate == null) { StartDate = DateTime.Now.Date; }
                    if (EndDate == null) { EndDate = DateTime.Now.Date; }
                    cmd.Parameters.AddWithValue("@StartDate", StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", EndDate);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    conn.Open();
                   
                    SqlDataReader reader = cmd.ExecuteReader();
                   
                    while (reader.Read())
                    {

                        SalesDataModels _data = new SalesDataModels()
                        {
                            DriverName = reader["FirstName"].ToString() + " " + reader["LastName"].ToString(),
                            DriverID = reader["DriverID"].ToString(),
                            OutletID = reader["OutletID"].ToString(),
                            Truckreg = reader["VRegNO"].ToString(),
                            SaleID = reader["SaleID"].ToString(),
                            OutletName = reader["CustomerName"].ToString(),
                            Refill_6KG = Convert.ToInt32(reader["Refill6KG"].ToString()),
                            Refill_13KG = Convert.ToInt32(reader["Refill13KG"].ToString()),
                            Refill_50KG = Convert.ToInt32(reader["Refill50KG"].ToString()),
                            Outright_6KG = Convert.ToInt32(reader["Outright6KG"].ToString()),
                            Outright_13KG = Convert.ToInt32(reader["Outright13KG"].ToString()),
                            Outright_50KG = Convert.ToInt32(reader["Outright50KG"].ToString()),
                            Burners = Convert.ToInt32(reader["Burner"].ToString()),
                            Grills = Convert.ToInt32(reader["Grill"].ToString()),
                            TranAmount= Convert.ToInt32(reader["TransAmount"].ToString()),
                            Variance = Convert.ToInt32(reader["Variance"].ToString()),
                            MpesaAmount = Convert.ToInt32(reader["MpesaAmount"].ToString()),
                            DateSold = Convert.ToDateTime(reader["TransTime"].ToString()),
                            comment = reader["comment"].ToString(),
                        };

                        _Sales.Add(_data);
                    }
                }
            }

            return _Sales;

        }

       


        public List<TruckWiseSalesModel> TruckWiseSales()
        {
            List<TruckWiseSalesModel> _TruckSales = new List<TruckWiseSalesModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetSalesPerTruck", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TruckWiseSalesModel _data = new TruckWiseSalesModel()
                        {
                            DriverName = reader["DriverName"].ToString(),
                            Truckreg = reader["VRegNO"].ToString(),
                            DriverID = reader["DriverID"].ToString(),
                            Refill_6KG = Convert.ToInt32(reader["Refill6KG"].ToString()),
                            Refill_13KG = Convert.ToInt32(reader["Refill13KG"].ToString()),
                            Refill_50KG = Convert.ToInt32(reader["Refill50KG"].ToString()),
                            Outright_6KG = Convert.ToInt32(reader["Outright6KG"].ToString()),
                            Outright_13KG = Convert.ToInt32(reader["Outright13KG"].ToString()),
                            Outright_50KG = Convert.ToInt32(reader["Outright50KG"].ToString()),
                            Burners = Convert.ToInt32(reader["Burner"].ToString()),
                            Grills = Convert.ToInt32(reader["Grill"].ToString()),
                            TotalRevenue = Convert.ToInt32(reader["TransAmount"].ToString()),
                            TotalSold = Convert.ToInt32(reader["Refill6KG"].ToString())
                            +Convert.ToInt32(reader["Refill13KG"].ToString())
                            +Convert.ToInt32(reader["Refill50KG"].ToString())
                            +Convert.ToInt32(reader["Outright6KG"].ToString())
                            +Convert.ToInt32(reader["Outright13KG"].ToString())
                            +Convert.ToInt32(reader["Outright50KG"].ToString())
                        };

                        _TruckSales.Add(_data);
                    }
                }
            }

            return _TruckSales;

        }
        public List<EditSalesModel> GetProductSales(string SaleID)
        {
            List<EditSalesModel> _EditSales = new List<EditSalesModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetEditSale", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SaleID",SaleID);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        EditSalesModel _data = new EditSalesModel()
                        {
                            SaleID = reader["SaleID"].ToString(),
                            RowID = reader["id"].ToString(),
                            DriverName = reader["FirstName"].ToString() + " " + reader["LastName"].ToString(),
                            SKUTransAmount = Convert.ToInt32(reader["SKUTransAmount"].ToString()),
                            Truckreg = reader["VRegNO"].ToString(),
                            OutletName = reader["CustomerName"].ToString(),
                            ProductDesc = reader["ProductDesc"].ToString(),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            DateSold = Convert.ToDateTime(reader["date_added"].ToString()),

                        };

                        _EditSales.Add(_data);
                    }
                }
            }

            return _EditSales;

        }
        


        public List<EditSalesModel> GetSalesPayments(string SaleID)
        {
            List<EditSalesModel> _EditSales = new List<EditSalesModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetEditSale", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SaleID", SaleID);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        EditSalesModel _data = new EditSalesModel()
                        {
                            SaleID = reader["SaleID"].ToString(),
                            RowID = reader["id"].ToString(),
                            DriverName = reader["FirstName"].ToString() + " " + reader["LastName"].ToString(),
                            SKUTransAmount = Convert.ToInt32(reader["SKUTransAmount"].ToString()),
                            Truckreg = reader["VRegNO"].ToString(),
                            OutletName = reader["CustomerName"].ToString(),
                            ProductDesc = reader["ProductDesc"].ToString(),
                            Quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            DateSold = Convert.ToDateTime(reader["date_added"].ToString()),

                        };

                        _EditSales.Add(_data);
                    }
                }
            }

            return _EditSales;

        }
        public class CylinderReturns
        {
            public string RCylinder { get; set; }
            public string Returns6KG { get; set; }
            public string Returns13KG { get; set; }

        }
        public List<CylinderReturns> GetCylinderReturns(string SaleID)
        {
            var Returns = new List<CylinderReturns>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from tblCylinderReturns where SaleID='"+SaleID.Trim()+"'", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CylinderReturns _data = new CylinderReturns()
                        {
                            RCylinder = reader["RCylinder"].ToString(),
                            Returns6KG = reader["RQuantity6KG"].ToString(),
                            Returns13KG = reader["RQuantity13KG"].ToString(),
                            
                        };

                        Returns.Add(_data);
                    }
                }
            }

            return Returns;

        }

        public List<PaymentsModel> GetPayments(string SaleID)
        {
            List<PaymentsModel> _Payments = new List<PaymentsModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM tblPayments where SaleID='"+SaleID+"'", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;                   
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        PaymentsModel _data = new PaymentsModel()
                        {
                            SaleID = reader["SaleID"].ToString(),
                            RowID = reader["PaymentID"].ToString(),
                            PaymentMethod = reader["PaymentMethod"].ToString() ,
                            AmountPaid = Convert.ToInt32(reader["TransAmount"].ToString()),
                            TransID = reader["TransID"].ToString(),
                            PhoneNumber = reader["PhoneNumber"].ToString(),
                            DateReceived = Convert.ToDateTime(reader["DateReceived"].ToString()),
                        };

                        _Payments.Add(_data);
                    }
                }
            }

            return _Payments;

        }

        public List<CustomersModel> GetCustomersList()
        {
            List<CustomersModel> _Customers = new List<CustomersModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select top 1000 ROWID,Outletname,Route,Area,Contactname,Contactnumber,PriceCategory from frm_customer_details order by Outletname asc", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CustomersModel _data = new CustomersModel()
                        {

                            OutletID = Convert.ToInt32(reader["ROWID"].ToString()),
                            OutletName = reader["Outletname"].ToString(),
                            PriceCategory = reader["PriceCategory"].ToString(),
                            ContactName = reader["Contactname"].ToString(),
                            ContactNumber = reader["Contactnumber"].ToString(),
                            Area = reader["Area"].ToString(),
                            Route = reader["Route"].ToString(),

                        };

                        _Customers.Add(_data);
                    }
                }
            }

            return _Customers;

        }


        public List<CustomersModel> CustomerSaleList()
        {
            List<CustomersModel> _Customers = new List<CustomersModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select distinct OutletID,CustomerName from " +
                    "tblMasterSales where OutletID is not null and CustomerName is not null  " +
                    "and cast(TransTime as date) > '2018-11-01' order by CustomerName asc", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CustomersModel _data = new CustomersModel()
                        {

                            OutletID = Convert.ToInt32(reader["OutletID"].ToString()),
                            OutletName = reader["CustomerName"].ToString(),                           
                        };

                        _Customers.Add(_data);
                    }
                }
            }

            return _Customers;

        }

        public List<AcceptedCylinderModel> GetAcceptedCylinders()
        {
            List<AcceptedCylinderModel> _AcceptedCylinders = new List<AcceptedCylinderModel>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from tblCompetitorCylinders ORDER BY Status desc,Brand asc", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        AcceptedCylinderModel _data = new AcceptedCylinderModel()
                        {

                            id = Convert.ToInt32(reader["CylinderID"].ToString()),
                            BrandName = reader["Brand"].ToString(),
                            Category = reader["ReturnsCategory"].ToString(),
                            Status = Convert.ToBoolean(reader["Status"]),
                        };

                        _AcceptedCylinders.Add(_data);
                    }
                }
            }

            return _AcceptedCylinders;

        }

        public  string UpdateRoutePrices(EditPricesModel model)
        {

            string msg = "";
            try
            {
                using (SqlConnection conn = new SqlConnection(DBConn))
                {
                    using (SqlCommand cmd = new SqlCommand("Update tblPrices SET Refill6KG='" + model.Refill6KG + "' ,Refill13KG='" + model.Refill13KG + "',Refill50KG='" + model.Refill50KG + "'," +
                      "Outright6KG='" + model.Outright6KG + "',Outright13KG='" + model.Outright13KG + "',Outright50KG='" + model.Outright50KG + "',PriceGrill='" + model.PriceGrill + "',PriceBurner='" + model.PriceBurner + "' where id='" + model.RouteID + "'" +
                        "", conn))
                    {
                        conn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;

                       msg = cmd.ExecuteNonQuery().ToString();
                    }
                }
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }
        
        public int UpdateAcceptedCylinders(bool Status,int CylinderID)
        {
            int StatusInt = 0;
            if (Status == true) { StatusInt = 1; } else { StatusInt = 0; }
            int msg = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(DBConn))
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE tblCompetitorCylinders set Status='"+StatusInt+ "' WHERE CylinderID='" + CylinderID+"'", conn))
                    {
                        conn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;

                        msg = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                msg = 0;
            }
            return msg;
        }

        public int ChangePricing(string OutletID, string PriceCategory)
        {
            int msg = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(DBConn))
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE frm_Customer_Details set PriceCategory='" + PriceCategory + "' WHERE ROWID='" + OutletID + "'", conn))
                    {
                        conn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;

                        msg = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                msg = 0;
            }
            return msg;
        }
        public List<TerritorySummaryGraph> TerritorySummaryGraph()
        {
            List<TerritorySummaryGraph> _TerritorySales = new List<TerritorySummaryGraph>();

            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select Territory,sum(Quantity) TQuantity from tblSoldProducts p " +
                    "left outer  join tblMasterSales m on m.SaleID=p.SaleID left outer join tblRoutes r on r.RID = m.RouteID " +
                    " where cast(m.TransTime as date) = cast(getdate() as date) group by r.Territory ", conn))//query
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TerritorySummaryGraph _data = new TerritorySummaryGraph()
                        {
                            
                            TerritorySales = Convert.ToInt32(reader["TQuantity"].ToString()),                            
                            TerritoryName = reader["Territory"].ToString()                           
                        };

                        _TerritorySales.Add(_data);
                    }
                }
            }
            return _TerritorySales;
        }


        public List<SoldProductsModel> GetSoldProducts(DateTime? startDate,DateTime? endDate)
        {
            List<SoldProductsModel> _Returns = new List<SoldProductsModel>();
            if(startDate==null)
            {
                startDate = DateTime.Now.Date.AddMinutes(11);
            }
            if (endDate == null)
            {
                endDate = DateTime.Now.Date.AddDays(1);
            }
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("GetSoldProducts", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@startDate", startDate.Value.ToString("yyyy-MM-dd HH:mm"));
                    cmd.Parameters.AddWithValue("@endDate", endDate.Value.ToString("yyyy-MM-dd HH:mm"));
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SoldProductsModel _data = new SoldProductsModel
                        {

                            vregno = reader["vregno"].ToString(),
                            outletname = reader["CustomerName"].ToString(),
                            driver_name = reader["DriverName"].ToString(),
                            route_name = reader["RName"].ToString(),
                            productdesc = reader["productdesc"].ToString(),
                            quantity = Convert.ToInt32(reader["Quantity"].ToString()),
                            sku_trans_amount = Convert.ToInt32(reader["SKUTransAmount"].ToString()),
                            date_added = Convert.ToDateTime(reader["date_added"].ToString()),                            
                        };
                        _Returns.Add(_data);
                    }
                }
            }
            return _Returns;
        }





        public List<BulkOrdersModel> GetBulkOrders()
        {
           var _Orders = new List<BulkOrdersModel>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select order_id,c.customer_id,customer_name,b.status, price,quantity,delivery_date from tbl_bulk_orders b inner join tbl_bulk_customers c on c.customer_id = b.customer_id where b.Status='0'", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();                    
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int request_status = Convert.ToInt32(reader["status"].ToString());
                        OrderStatus _status = (OrderStatus)request_status;
                        string strStatus = _status.ToString();
                        BulkOrdersModel _data = new BulkOrdersModel
                        {
                            status = strStatus,
                            order_id = reader["order_id"].ToString(),
                            customer_id = reader["customer_id"].ToString(),
                            customer_name = reader["customer_name"].ToString(),
                            price = Convert.ToInt32(reader["price"].ToString()),                          
                            requested_quantity = Convert.ToInt32(reader["quantity"].ToString()),                            
                            expected_delivery_date = Convert.ToDateTime(reader["delivery_date"].ToString()),
                        };
                        _Orders.Add(_data);
                    }
                }
            }
            return _Orders;
        }

        public List<BulkOrdersModel> GetPlanningOrders()
        {
            var _Orders = new List<BulkOrdersModel>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select order_id,c.customer_id,customer_name,b.status, price,quantity_approved,delivery_date from tbl_bulk_orders b inner join tbl_bulk_customers c on c.customer_id = b.customer_id where b.Status='1'", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int request_status = Convert.ToInt32(reader["status"].ToString());
                        OrderStatus _status = (OrderStatus)request_status;
                        string strStatus = _status.ToString();
                        BulkOrdersModel _data = new BulkOrdersModel
                        {
                            status = strStatus,
                            order_id = reader["order_id"].ToString(),
                            customer_id = reader["customer_id"].ToString(),
                            customer_name = reader["customer_name"].ToString(),
                            price = Convert.ToInt32(reader["price"].ToString()),
                            approved_quantity = Convert.ToInt32(reader["quantity_approved"].ToString()),
                            expected_delivery_date = Convert.ToDateTime(reader["delivery_date"].ToString()),
                        };
                        _Orders.Add(_data);
                    }
                }
            }
            return _Orders;
        }

        public List<BulkOrdersModel> getpendingOrders()
        {
            var _Orders = new List<BulkOrdersModel>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("select order_id,FirstName,LastName,Vregno,c.customer_id,customer_name,b.status,quantity_approved, price, " +
                    "delivery_date from tbl_bulk_orders b  inner join tbl_bulk_customers c on c.customer_id = b.customer_id inner join tblDrivers d on d.DriverID = b.driver_id " +
                    " inner join tblVehicles v on v.VID = b.vid", conn))//call Stored Procedure
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int request_status = Convert.ToInt32(reader["status"].ToString());
                        OrderStatus _status = (OrderStatus)request_status;
                        string strStatus = _status.ToString();
                        BulkOrdersModel _data = new BulkOrdersModel
                        {
                            status = strStatus,
                            order_id = reader["order_id"].ToString(),
                            customer_id = reader["customer_id"].ToString(),
                            driver_name = reader["FirstName"].ToString() + " " +reader["LastName"].ToString(),
                            vregno = reader["vregno"].ToString(),
                            customer_name = reader["customer_name"].ToString(),
                            price = Convert.ToInt32(reader["price"].ToString()),
                            approved_quantity = Convert.ToInt32(reader["quantity_approved"].ToString()),                           
                            expected_delivery_date = Convert.ToDateTime(reader["delivery_date"].ToString()),
                        };
                        _Orders.Add(_data);
                    }
                }
            }
            return _Orders;
        }
        enum OrderStatus
        {
            Pending =0,
            Approved=1,
            Planned = 2,
            Delivered =3,
        }
        public List<TruckregsModel> GetBulkTrucks()
        {
            List<TruckregsModel> __Trucks = new List<TruckregsModel>();
            using (SqlConnection conn = new SqlConnection(DBConn))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT VID,VRegNO FROM tblVehicles WHERE " +
                    "VID  in (SELECT vid FROM tblVehicleAssignments WHERE status = 1) and UsageType='2'", conn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        TruckregsModel __trucks = new TruckregsModel()
                        {
                            Truckreg = reader["VRegNo"].ToString(),
                            id = Convert.ToInt32(reader["VID"].ToString()),

                        };
                        __Trucks.Add(__trucks);
                    }

                }
            }
            return __Trucks;
        }

        public int ApproveBulkOrder(OrderApproval order)
        { 
            int msg = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(DBConn))
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE tbl_bulk_orders set status='1',quantity_approved='"+order.app_quantity+"',action_message='"+order.message.Trim()+"' WHERE order_id='" + order.order_id + "'", conn))
                    {
                        conn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;

                        msg = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                msg = 0;
            }
            return msg;

        }

        public int UpdateDeliveryPlan(DeliveryPlan order)
        {
            int msg = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(DBConn))
                {
                    using (SqlCommand cmd = new SqlCommand("UPDATE tbl_bulk_orders set status='1', driver_id='"+order.driver_id+"',vid='" + order.vid+ "' WHERE order_id='" + order.order_id + "'", conn))
                    {
                        conn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;
                        msg = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                msg = 0;
            }
            return msg;

        }

        public int AddPayment(AddPaymentModel data)
        {
            int i = 0;
            try
            {
               
                using (SqlConnection con = new SqlConnection(DBConn))
                    {
                        con.Open();
                        SqlCommand com = new SqlCommand("INSERT INTO [dbo].[tblPayments] ([SaleID] ,[PaymentMethod] ,[PhoneNumber],[TransAmount] ,[Status])" +
                            "VALUES(@saleid, @paymentMethod, @phoneNumber, @TransAmount, @Status)", con);
                        com.CommandType = CommandType.Text;
                        com.Parameters.AddWithValue("@saleid", data.saleid.Trim());
                        com.Parameters.AddWithValue("@paymentMethod", data.paymentMethod.Trim());
                        com.Parameters.AddWithValue("@phoneNumber", data.phoneNumber.Trim());
                        com.Parameters.AddWithValue("@TransAmount", data.paidAmount.Trim());
                         com.Parameters.AddWithValue("@Status", "Web");
                        i = com.ExecuteNonQuery();
                    }
              
            }

            catch (Exception ex)
            {
                i = 0;
               Console.WriteLine(ex.Message);
            }
            return i;

        }
    }
}