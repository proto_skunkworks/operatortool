﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Services
{
    public class SMSSender
    {
        public void sendSMS(string phoneNumber, string message)
        {
            string username = "SMSOrders";
            string apiKey = "c8acaf32ba747f6e8e124c940da86597ce5399e4bd8418f5d1dcbbc1354525ce";
            string recipients = phoneNumber;
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            try
            {

                gateway.sendMessage(recipients, message);
             
            }
            catch (AfricasTalkingGatewayException e)
            {
                //logs.WriteLog(e.Message);
            }
        }
    }
}