﻿using OperatorToolSpace.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OperatorToolSpace.Services
{
    public class WorkshopService
    {
        string DBConn = ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString;
        public List<WorkshopDataModel> GetUnderRepair()
        {
            List<WorkshopDataModel> _requests = new List<WorkshopDataModel>();
            using (SqlConnection Conn = new SqlConnection(DBConn))
            {
                string sql = "Select * from tbl_workshop_schedule";

                using (SqlCommand Cmd = new SqlCommand(sql, Conn))
                {
                    Conn.Open();
                    Cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = Cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        WorkshopDataModel _data = new WorkshopDataModel()
                        {
                            VRegNo = reader["vehicle_reg"].ToString().Trim(),
                            repairType = reader["repair_type"].ToString().Trim(),
                            description = reader["description"].ToString().Trim(),
                            MoreInfo = reader["repair_user_info"].ToString().Trim(),
                            date_checkedin =reader["date_checked_in"].ToString(),
                            date_checkedout = reader["date_checked_out"].ToString(),
                            date_requested = Convert.ToDateTime(reader["date_added"].ToString()),
                            // approved_quantity = reader["approved_quantity"].ToString()
                        };
                        _requests.Add(_data);
                    }

                }

            }
            return _requests;
        }
    }
}