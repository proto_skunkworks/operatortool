﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.BulkOrdersModel>>" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Bulk Orders" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- row -->
     <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.min.css" rel="stylesheet" />
    <link href="../../Scripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <%: @TempData["Success"] %> 
       
            </div>
            <% } %>
        </div>
    </div>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                             <tr>  
                                <th>Order ID</th>
                                <th>Customer ID</th>  
                                <th>Customer Name</th>                                
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Delivery Date</th> 
                                 <th>Status</th>  
                                <th>Approval</th>
                             </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                            <tr> 
                                <td><%: Html.DisplayFor(m => item.order_id) %></td>
                                <td><%: Html.DisplayFor(m => item.customer_id) %></td>
                                <td><%: Html.DisplayFor(m => item.customer_name) %></td>                              
                                <td><%: Html.DisplayFor(m => item.price) %></td>
                                <td><%: Html.DisplayFor(m => item.quantity) %></td>
                                <td><%: Html.DisplayFor(m => item.expected_delivery_date) %></td>
                                <td><%: Html.DisplayFor(m => item.status) %></td>
                               
                                 <td>
                               <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#<%: Html.DisplayFor(m => item.order_id) %>">APPROVE</button>
                               <div class="modal fade" id="<%: Html.DisplayFor(m => item.order_id) %>" role="dialog">
                               <div class="modal-dialog modal-sm">
                               <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title"><%: Html.DisplayFor(m => item.customer_name) %> order approval</h4>
                                </div>
                                <div class="modal-body">
                                     <form>
                                  <h4><%: Html.DisplayFor(m => item.customer_name) %></h4>  
                                    <div class="form-group">
                                       <label>Quantity to Approve</label>
                                    <input type="text" class="form-control" id="approved_quantity<%:item.order_id %>" style="width:80%;"/>
                                   </div>
                                   
                                      
                                      <div class="form-group">
                                        <label for="message-text" class="col-form-label">Message:</label>
                                        <textarea class="form-control" id="message<%:item.order_id %>"  style="width:80%;" ></textarea>
                                      </div>
                                    </form>
                                   <input type="hidden" id="saleid" value="<%:item.order_id %>" />
                                </div>
                                   
                                <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="ApproveOrder(<%:item.order_id %>);">SAVE</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                       </td>
                            </tr>
                            <% } %>
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
 </div>
 <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.min.css" rel="stylesheet" />
  <script>
      function ApproveOrder(order_id) {       
          var app_quantity = $('#approved_quantity' + order_id).val();
          var message = $('#message' + order_id).val();
          var appObj = {
              app_quantity: app_quantity,
              order_id: order_id,
              message:message,
          };
          console.log(appObj);
          $.ajax({
              url: '/bulk/approveorder',
              data:JSON.stringify(appObj),
              contentType: 'application/json;charset=utf-8',
              type: 'POST',
              dataType: 'JSON',
              success: function (data) {
              $('#' + order_id + '').modal('hide');
              alertify.alert('Success', data.Message, function () { alertify.message('Ok'); });
              },
              failure: function (error) {
              alertify.alert('Failed', data.Message, function () { alertify.message('Ok'); });
              }
        });
      }
</script>
<style>

  .modal fade {
    width: 100px;
     height: 100px;
 }
    </style>
</asp:Content>
