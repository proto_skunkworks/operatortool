﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.DashboardModels>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Planning" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">      
     <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/select2.min.js"></script>
    <link href="../../Scripts/select2.min.css" rel="stylesheet" />
    <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.css" rel="stylesheet" />
        <script src="../../Scripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <link href="../../Scripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" /> 
        <div class="row">
           <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">New  Trip</h3>
            </div>
            <div class="box-body">
              
                 <div class="form-group">
                <label>Vehicle:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-truck"></i>
                  </div>
                <select class="form-control select2" id="Truckreg" style="width: 100%;"> </select>
                </div>
              </div>

                 <div class="form-group">
                <label>Category</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-shopping-cart"></i>
                  </div>
                 <select class="form-control select2" style="width: 100%;" id="Category">  
                  <option selected="selected" value="0" disabled = "disabled">Select one</option>
                  <option value="1">Sales</option>
                  <option value="2">Replenishment</option>                 
                </select>
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Destination</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-map-marker"></i>
                  </div>
                  <select class="form-control select2" style="width: 100%;" id="destination"> </select>
                </div>
                <!-- /.input group -->
              </div>
              <!-- Date -->
               <div class="form-group">
                <label>Date: (Date you are Planning for)</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                <label>Quantity 6KG</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-upload"></i>
                  </div>
                  <input type="text" class="form-control" id="kg6_refill" />
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Quantity 13KG</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-upload"></i>
                  </div>
                  <input type="text" class="form-control" id="kg13_refill" />
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Quantity 50KG</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-upload"></i>
                  </div>
                  <input type="text" class="form-control" id="kg50_refill" />
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                <label>Burners</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sun-o"></i>
                  </div>
                  <input type="text" class="form-control" id="burners" />
                </div>
                <!-- /.input group -->
              </div>
                  <div class="form-group">
                <label>Grills</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-sun-o"></i>
                  </div>
                  <input type="text" class="form-control" id="grills" />
                </div>
                <!-- /.input group -->
              </div>
               <div class="box-footer">
                   <input type="hidden" class="form-control" id="driver_id" />
                   <input type="hidden" class="form-control" id="truckhelper" />
                <button type="submit" class="btn btn-primary" onclick="SavePlanning();">Submit</button>
              </div>
                
            </div>
            
          </div>
        </div>
          
            <div class="box-body">
             <div class="col-md-6">
             <div class="box box-primary">
              <div id="details"> </div>
              </div>
               </div>
                </div>
               
            </div>
             
        <div class="row">        
        <div class="col-md-6">
              <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Recently Planned Trips</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
           
          </div>
       </div>
      </div>
    <script type="text/javascript">

        $('#datepicker').datepicker({
            autoclose: true,
            format:"dd-mm-yyyy"
        })
          $(function(){            
              $("#destination").select2();
          })
         
         function GetContainers() {
              var destination = $("#destination");           
            $.ajax({
                type: "GET",
                url: "/Drivers/GetContainers",               
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                          destination.append($("<option></option>").val(this['ContainerName']).html(this['ContainerName']));                         
                    });
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

        }
     
$( document ).ready(function() {
    LoadTrucks();
});
     function LoadTrucks() {
            var Truckregs = $("#Truckreg");
            Truckregs.empty().append('<option selected="selected" value="0" disabled = "disabled">Select Truck</option>');
            $.ajax({
                type: "POST",
                url: "/planning/GetTruckRegs",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.each(response, function () {
                        Truckregs.append($("<option></option>").val(this['id']).html(this['Truckreg']));
                    });
                },
                failure: function (response) {
                    //alert(response.responseText);
                },
                error: function (response) {
                    //alert(response.responseText);
                }
            });
        }

        function GetVehicleAssignments(vid) {            
            var vidObj =
            {
                VID: vid,
            };
            $.ajax({
                type: "POST",
                url: "/planning/GetVehicleAssignments",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vidObj),
                dataType: "json",
                success: function (data) {                  
                 var table = $("#details");         
                 var count = Object.keys(data).length;
                 if(count>0)
                 {
                     var driver_id;
                    
                     var eTable = "<table><thead><tr><th>VID</th><th>Vreg</th><th>Driver</th><th>Truck Helper</th><th>Routes</th><th>Change</th></tr></thead><tbody>"
                  $.each(data,function(index, row){   
                    eTable += "<tr>";
                      $.each(row, function (key, value) {
                          if (key == "driver_id") {
                              driver_id = value;
                          }
                          if (key == "driver_id") {
                             
                          }
                          else {
                               eTable += "<td>" + value + "</td>";
                          }
                      });
                      eTable += '<td><a class="btn btn-xs btn-default" href="/Drivers/EditDriver/' + driver_id + '"><i class="fa fa-edit">RETURNS</i></a </td>';
                    eTable += "</tr>";
                  });
                  eTable +="</tbody></table>";
                 table.html(eTable);

                 }
                else{
                table.html("No records found!");
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

        }
        function GetLastLoadedQuantity(VID) {            
            var vidObj =
            {
                VID: VID,
            };
            $.ajax({
                type: "POST",
                url: "/planning/GetLastLoaded",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vidObj),
                dataType: "json",
                success: function (data) { 
                 var count = Object.keys(data).length;
                 if(count>0)
                 { 
                  $.each(data,function(index, row)
                        {  
                        $.each(row, function (key, value) 
                             {
                              if (key == "kgs_6") {
                               $('#kg6_refill').val(value);
                             }
                             if (key == "kgs_13") {
                               $('#kg13_refill').val(value);
                              }
                             if (key == "kgs_50") {
                               $('#kg50_refill').val(value);
                              }
                             if (key == "burners") {
                               $('#burners').val(value);
                            }
                              if (key == "grills") {
                               $('#grills').val(value);
                               }
                            
                            });
                     
                        });
                 }
                else{
                
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });
        }


    $('#Category').change( function() {    
        var selectedItem = $(this).val();
        console.log(selectedItem);
    if (selectedItem == "2") {
        $("#destination").empty();       
        GetContainers();
        $("#destination").prop("disabled", false);
    }
    else {
        $("#destination").empty();
        $("#destination").empty().append('<option selected="selected" value="Sales" disabled = "disabled">Sales</option>');
         $("#destination").prop("disabled", true);
            }
        });

      var $eventSelect = $("#Truckreg");
      $eventSelect.select2();       
        $eventSelect.on("change", function (e)
        {
            var VID = $("#Truckreg :selected").attr('value');
            GetVehicleAssignments(VID);
            GetLastLoadedQuantity(VID);
        });
   function SavePlanning() {  
       var Truck = $("#Truckreg :selected").attr('value');
       var Category = $("#Category :selected").attr('value');
       var destination = $("#destination :selected").attr('value');
       var planned_date = $("#datepicker").datepicker().val();
       var kg6_refill = $("#kg6_refill").val();
       var kg13_refill = $("#kg13_refill").val();
       var burners = $("#burners").val();
       var grills = $("#grills").val();
       var kg50_refill = $("#kg50_refill").val();

       if ($.trim(Truck) =="0") {
           alertify.alert('Error', 'Ooops! You must Select vehicle to plan trip.', function () { alertify.error('Ok'); });
           return;
       }
       
       if ($.trim(Truck) =="" || $.trim(Category) == "" || $.trim(destination) == "" ||  $.trim(planned_date)=="") {
           alertify.alert('Error', 'Ooops! Make sure you filled all the necessary fields', function () { alertify.error('Ok'); });
           return;
       }
       if ($.trim(kg6_refill) == "" &&
           $.trim(kg13_refill) == "" &&
           $.trim(burners) == "" &&
           $.trim(grills) == "" &&
           $.trim(kg50_refill) == "") {
           alertify.alert('Error', 'Ooops! Quantity fields are empty!', function () { alertify.error('Ok'); });
           return;
       }
        var empObj = {      
         vid: Truck.trim(),       
         Category: Category.trim(),
         destination: destination.trim(),
         planned_date: planned_date,
         kg6_refill: kg6_refill,
         kg13_refill: kg13_refill,
         burners: burners,
         grills: grills,
         kg50_refill:kg50_refill,
        
       };  

   
    $.ajax({  
        url: "/Planning/SavePlanning",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alertify.alert('Message', result.responseText, function () { alertify.message('Ok'); }); 
                $("#Truckreg").empty(); 
                LoadTrucks();            
        },  
         failure: function (result) {
                  alertify.alert('Error', 'Ooops! Make sure you filled all the necessary fields', function () { alertify.error('Ok'); });
                },
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });  
   }
</script>

        <style>
        #details{
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size:11px;
        width: 100%;
        }
        #details td, #details th {
        border: 1px solid #ddd;
        padding: 5px;
        }
        #details tr:nth-child(even){background-color: #f2f2f2;}

        </style>
</asp:Content>
