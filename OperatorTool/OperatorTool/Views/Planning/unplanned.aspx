﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.TripsDownloadModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Planning" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../../Scripts/plugins/iCheck/icheck.min.js"></script>
    <link href="../../Scripts/plugins/iCheck/all.css" rel="stylesheet" />
    <script src="../../Scripts/select2.min.js"></script>
    <link href="../../Scripts/select2.min.css" rel="stylesheet" />
    <div class="content-wrapper"> 
        <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">AVAILABLE DRIVERS </span>
              <span class="info-box-text">34 </span>
            </div>
          </div>
        </div>

          <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">ON LEAVE/OFF</span>
              <span class="info-box-text">3</span>
            </div>
         
          </div>
       
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-truck"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">AVAILABE ASSETS</span>
              <span class="info-box-text">60</span>
            </div>
           
          </div>
        
        </div>
        
           <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">WORKSHOP</span>
              <span class="info-box-text">1</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
          </div>
















       <div class="row">
       <div class="box box-primary">

            <div class="col-xs-4">
                <div class="form-group">
               <label>Date </label>
               <input type="text" id="datepicker" style="width: 50%" />
                </div>
            </div>
           

             <div class="col-xs-4">
                 <div class="form-group">
                <label>Status </label>
              <select class="form-control select2" id="tripstatus" style="width: 50%;">
                  <option value="0">Planned</option>
                  <option value="1">Unplanned</option>

              </select>
            </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">               
               <button type="button" class="btn btn-xs btn-primary">EXPORT AS EXCEL</button>
                </div>
            </div>
 
        </div>

            </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="viewPlanning">
            
            </div>
        </div>
    </div>
     <script src="../../Scripts/bower_components/dist/js/adminlte.min.js"></script>
     <a href="../../Scripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css.map">
 <script type="text/javascript">

$(document).ready(function(){
$('#viewPlanning').load("/planning/allUnplannedAssets");
});
     $('#datepicker').datepicker({
         setDate: new Date(),
         format:'dd-MM-yyyy',
                autoclose: true
     }).datepicker("setDate", 'now');
     

      //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
     })

     $('#datepicker').change(function() { 
         //console.log('date changed');
         var date_selected = $('#datepicker').datepicker().val();
         var status = $("#tripsttus :selected").attr('value');
         if (status == null) {status="all"}
         getPlanningDetails(date_selected,status);
          });
    
    function getPlanningDetails(date,status)
    {
         var urlValue;
        if (status == "1") {
            urlValue = 'unplannedVehicles?date=' + date;
        }
        else if (status == "0") {
            urlValue = 'plannedtrips?date=' + date;
        }
        else if (status == "all") {
            urlValue = 'allVehicles?date=' + date;
        }
        else {
            alert("Please Select Category.");
            return;
        }
       
        $.ajax
        (
            {
                url: urlValue,
                contentType: 'application/html; charset=utf-8',
                type: 'GET',
                dataType: 'html'
            }
         )
        .success
        (function (result) {           
            $('#viewPlanning').html("");
            $('#viewPlanning').html(result);
            $("#tabs").tabs();
            $("#example1 table").css("width", "80%");
        }
        )
        .error
        (function (xhr, status) {
        }
        )

     }
$('#tripstatus').select2();
$('#tripstatus').change( function() {    
    var selectedItem = $(this).val();
    var date_selected = $('#datepicker').datepicker().val();
    getPlanningDetails(date_selected,selectedItem);
    //console.log(selectedItem);
    //console.log(date_selected);
});

</script>
</div>
 <style>
    input[type=text] {
    width: 80%;
    padding: 5px 20px;
    margin: 1px 0;
    box-sizing: border-box;
    border-radius: 3px;
    font-size:13px;
}

.info-box {
    display: block;
    min-height: 50px;
    background: #fff;
    width: 60%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 100px;
    margin-bottom: 10px;
    font-size:10px;
    font-style:normal;
    font-weight:bold

}
.info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
     border-radius: 100px;
    display: block;
    float: left;
    height: 50px;
    width: 50px;
    text-align: center;
    font-size: 20px;
    line-height: 50px;
    background: rgba(0,0,0,0.2)
}
.info-box-number {
    display: block;
    font-weight: bold;
    font-size: 12px
}
</style>
  <link href="../../Scripts/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
</asp:Content>
