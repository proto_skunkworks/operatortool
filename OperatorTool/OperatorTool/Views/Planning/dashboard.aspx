﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.PlanningDashBoard>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Overview" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/jquery.min.js"></script>
     <section class="content">
      <div class="row">
          <a href="/planning/unplanned">
        <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-compass"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">PLANNING </span>
              <span class="info-box-text"><%:Model.unplanned%> UNPLANNED</span>
            </div>
         
          </div>
       
        </div>
                </a>
                <a href="/planning/active_trips">
               <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">ACTIVE</span>
              <span class="info-box-text"><%:Model.Active%></span>
            </div>
         
          </div>
       
        </div>
      </a>
          <a href="/planning/completed_trips">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-suitcase"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">COMPLETED</span>
              <span class="info-box-text"><%:Model.Completed%></span>
            </div>
           
          </div>
        
        </div>
           </a>
            <a href="/planning/all_trips">
           <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">ALL TRIPS</span>
              <span class="info-box-text"><%:Model.all_trips%></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         </a>
          </div>
          <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
             <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Planned Pending trips</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin" id="pendingTrips">
                  <thead></thead>
                    <tbody></tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="/planning/planning" class="btn btn-sm btn-info btn-flat pull-left">NEW TRIP</a>
              <a href="/planning/pending_trips" class="btn btn-sm btn-default btn-flat pull-right">VIEW ALL</a>
            </div>
                 </div>
    </div>
</div>
 </section>

    <script type="text/javascript">
      
        $( document ).ready(function() {
    GetPendTrips();
});
 function GetPendTrips() {          
            $.ajax({
                type: "GET",
                url: "/planning/Getpending_trips",               
                contentType: "application/json; charset=utf-8",               
                dataType: "json",
                success: function (data) {                  
                 var table = $("#pendingTrips");         
                 var count = Object.keys(data).length;
                    if (count > 0) {
                        var trip_id;
                        var eTable = "<table><thead><tr><th>Vehicle</th><th>Driver</th><th>Category</th><th>Destination</th><th>Date</th><th>Trip ID</th><th>Driver Phone</th><th>Status</th><th>Action</th></tr></thead><tbody>"
                        $.each(data, function (index, row) {
                            eTable += "<tr>";
                            $.each(row, function (key, value) {
                               
                             if (key == "time_taken" || key == "routes" || key=="trip_cost" || key=="truck_helper")
                            {

                             }
                             else if (key == "planned_date")
                             {
                                 var date = new Date(parseInt(value.substr(6)));
                                 eTable += "<td>" + date.toLocaleString() + "</td>";
                             }
                            else {
                                   eTable += "<td>" + value + "</td>";
                                 }
                                if (key == "trip_id") {
                                    trip_id = value;
                                }
                         
                      });
                      eTable += '<td><a class="btn btn-xs btn-default" href="/planning/edittrip/' + trip_id + '"><i class="fa fa-edit">EDIT</i></a </td>';
                    eTable += "</tr>";
                  });
                  eTable +="</tbody></table>";
                 table.html(eTable);

                 }
                else{
                table.html("No records found!");
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
            });

        }
       
    </script>
      <style>
        #pendingTrips{
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size:11px;
        width: 100%;
        }
        #pendingTrips td, #details th {
        border: 1px solid #ddd;
        padding: 4px;
        }
          #driver_id {
         display:none;
        }
        #pendingTrips tr:nth-child(even){background-color: #f2f2f2;}

        </style>
<link href="../../Scripts/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Scripts/bower_components/adminlite.css" rel="stylesheet" />
</asp:Content>
