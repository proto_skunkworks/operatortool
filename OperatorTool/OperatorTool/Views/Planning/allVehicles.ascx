﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<OperatorToolSpace.Models.ActiveTripsModel>>" %>
 <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>     
                                <th>No.</th>
                                 <th>Trip ID</th>
                                <th>Truck Reg</th>
                                <th>Driver Name</th>  
                                <th>Phone Number</th>
                                <th>Truck Helper</th>
                                 <th>Status</th>
                                <th>Routes</th>
                                <th>Planning</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% 
                                if (Model.Any())
                                {
                                    int count = 1;
                                    foreach (var item in Model)
                                    {%>
                                    <tr>
                                    <td> <%:count %></td>
                                        <td><%:Html.DisplayFor(m => item.trip_id) %>
                                    <td><%:Html.DisplayFor(m => item.vregno) %>
                                    <td><%:Html.DisplayFor(m => item.driver_name) %>
                                    <td><%:Html.DisplayFor(m => item.driver_phone) %>
                                        <td><%:Html.DisplayFor(m => item.truck_helper) %>
                                    <td><%:Html.DisplayFor(m => item.destination) %>
                                      <td><%:Html.DisplayFor(m => item.status) %>
                                    
                                   <%-- <td><a class="btn btn-xs btn-primary" href="/planning/plan_asset/<%: Html.DisplayFor(m => item.vid) %>&&date=">PLAN<i class="fa fa-save"></i></a>--%>
                                        <td><button type="button" class="btn btn-primary" onclick="editTrip(<%:item.trip_id %>);">PLAN</button></td> 
                                    </tr>
                            <% count++; }
                                }
                                else
                                {%>
                                <tr> <td rowspan="2"> No Plannings for the selected Date.</td> </tr>  
                            <%} %>
                        </tbody>
                    </table>
                </div>
            </div>
<script type="text/javascript">
function editTrip(token)
{
    alert(token +' Editing this trip is not allowed.');
}
  $(function () {
            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true
            });
        });

</script>
<style> 
#example1{
    font-size:11px;
}

</style>
