﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.TripsDownloadModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Pending Trips" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/moment/min/moment.min.js"></script>
 <div class="content-wrapper">    
 <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                    
                    <input type="hidden" id="download_token_value_id"/>
                    <button type="button"  class="btn btn-primary" id="ExportData">EXPORT AS EXCEL</button>
                </div>
              </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>                                
                                <th>Trip ID</th>
                                <th>Vehicle Reg</th>                               
                                <th>Type</th>
                                <th>Driver Name</th>
                                <th>Phone Number</th>
                                <th>Truck Helper</th>
                                 <th>Routes</th>
                                <th>6KGS</th>
                                 <th>13KGS</th>
                                  <th>50KGS</th>
                                 <th>Grills</th>
                                 <th>Burners</th>
                                  <th>Date Planned</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                                    <tr>
                                    <td><%:Html.DisplayFor(m =>item.trip_id) %>
                                    <td><%:Html.DisplayFor(m =>item.vregno) %>
                                    <td><%:Html.DisplayFor(m =>item.vtype) %>
                                    <td><%:Html.DisplayFor(m =>item.DriverName) %>
                                    <td><%:Html.DisplayFor(m =>item.PhoneNumber) %>
                                    <td><%: Html.DisplayFor(m => item.truck_helper) %></td>
                                    <td><%: Html.DisplayFor(m => item.route_names) %></td>
                                    <td><%: Html.DisplayFor(m => item.planned_6KG) %></td>
                                    <td><%: Html.DisplayFor(m => item.planned_13KG) %></td>
                                         <td><%: Html.DisplayFor(m => item.planned_50KG) %></td>
                                    <td><%: Html.DisplayFor(m => item.Grills) %></td>
                                         <td><%: Html.DisplayFor(m => item.Burners) %></td>
                                         <td><%: Html.DisplayFor(m => item.date_added) %></td>
                                    </tr>
                            <% } %>
                        </tbody>
                       
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
 <link href="../../Scripts/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
     <style>           

#example1 {
width:100%;
   font-size:12px;
      table-layout:fixed;     
      
}
 </style>
</div>

<script>

    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

     $('#LoadData').click(function(){
                 console.log('Start date: ' + endDate + '  EndDate: ' + startDate);
                 window.location = '<%:Url.Action("pending_trips","planning")%>?startDate=' + startDate+'&endDate='+endDate;

               });
            $('#ExportData').click(function () {
                 blockUIForDownload();
             var token = $('#download_token_value_id').val();
             window.location = '<%:Url.Action("TripsDownload","planning")%>';
             });

        

  })
</script>
</asp:Content>
