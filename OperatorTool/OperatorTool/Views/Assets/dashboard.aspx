﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.FuelingDashBoard>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Fuel DashBoard" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/jquery.min.js"></script>
     <section class="content">
      <div class="row">
          <a href="/Assets/pending_requests">
        <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">PENDING REQUESTS</span>
              <span class="info-box-text"><%:Model.pending %></span>
            </div>
         
          </div>
       
        </div>
          </a>
         <a href="/Assets/approved_requests">
          <div class="col-md-3 col-sm-6 col-xs-12" >
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">APPROVED</span>
              <span class="info-box-text"><%:Model.approved %></span>
            </div>
         
          </div>
       
        </div>
      </a>
        <a href="/Assets/closed_requests">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-grey"><i class="fa fa-suitcase"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">CLOSED</span>
              <span class="info-box-text"><%:Model.closed %></span>
            </div>
           
          </div>
        
        </div>
           </a>
           <a href="/Assets/rejected_requests">
           <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-times-circle-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">REJECTED</span>
              <span class="info-box-text"><%:Model.rejected %></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         </a>
     </div>

 </section>
<link href="../../Scripts/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Scripts/bower_components/adminlite.css" rel="stylesheet" />
</asp:Content>
