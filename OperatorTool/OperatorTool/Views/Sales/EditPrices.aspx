﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.EditPricesModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Edit Prices" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- row -->
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h4 class="box-title">Change <strong> <%: Model.PriceCategory %> Prices for <%: Model.RName %> </strong> </h4>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <% using (Html.BeginForm("UpdatePrices", null, FormMethod.Post, new { role = "form", id = "FormUser", enctype = "multipart/form-data" }))
                   { %>
                
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Refill 6KG</label>
                                <%: Html.TextBoxFor(model => model.Refill6KG, new { @class = "form-control",@required = "true"}) %>                                
                                <input type="hidden" class="form-control" name="UserID" id="UserID" value="<%: Model.RouteID %>" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Refill 13KG</label>
                                <%: Html.TextBoxFor(model => model.Refill13KG, new { @class = "form-control"}) %>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Refill 50KG</label>
                                <%: Html.TextBoxFor(model => model.Refill50KG, new { @class = "form-control"}) %>
                            </div>
                            
                            <div class="form-group">
                                <label>Outright 6KG</label>
                                <%: Html.TextBoxFor(model => model.Outright6KG, new { @class = "form-control"}) %>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="UserID" id="RouteID" value="<%: Model.RouteID %>" />
                                <label for="exampleInputEmail1">Outright 13KG</label>
                                <%: Html.TextBoxFor(model => model.Outright13KG, new { @class = "form-control" }) %>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Outright 50KG</label>
                                <%: Html.TextBoxFor(model => model.Outright50KG, new { @class = "form-control", @required = "true"}) %>                               
                            </div>  
                              <div class="form-group">
                                <label for="exampleInputEmail1">Burner</label>
                                <%: Html.TextBoxFor(model => model.PriceBurner, new { @class = "form-control", @required = "true"}) %>                               
                            </div> 
                              <div class="form-group">
                                <label for="exampleInputEmail1">Grill</label>
                                <%: Html.TextBoxFor(model => model.PriceGrill, new { @class = "form-control", @required = "true"}) %>                               
                            </div> 
                              <div class="form-group">                                
                                <%: Html.HiddenFor(model => model.RouteID, new { @class = "form-control", @placeholder=""}) %>
                            </div>
                              <div class="form-group">                                
                                <%: Html.HiddenFor(model => model.RName, new { @class = "form-control"}) %>
                            </div>
                          
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer" align="right">
                    <button type="submit" class="btn btn-primary">Apply Changes</button>
                </div>
                <% } %>
            </div>
            <!-- /.box -->

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->
</asp:Content>
