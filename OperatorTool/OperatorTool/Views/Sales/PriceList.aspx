﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.RoutePricesModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Prices Table View" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- row -->
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <%: @TempData["Success"] %> 
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                             <tr>
                              
                                <th>Route</th> 
                                <th>Price Category</th>                                
                                <th>6KG Refill</th>
                                <th>6KG Outright</th>
                                <th>13KG Refill</th>
                                <th>13KG Outright</th>
                                <th>50KG Refill</th>
                                <th>50KG Outright</th> 
                                <th>Burners</th> 
                                <th>Grills</th>                                                               
                                <th>Status</th> 
                                <th>Price Changes</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                            <tr>
                                

                                <td><%: Html.DisplayFor(m => item.RName) %></td>
                                <td><%: Html.DisplayFor(m => item.PriceCategory) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill6KG) %></td>                              
                                <td><%: Html.DisplayFor(m => item.Outright6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright50KG) %></td>
                                 <td><%: Html.DisplayFor(m => item.PriceBurner) %></td>
                                <td><%: Html.DisplayFor(m => item.PriceGrill) %></td>                                 
                                <td> <% if (item.Status == 1) 
                                    { %>                                    
                                        <%--<span class="label label-primary">Active</span>--%>

                                        <a href="#ApplyPriceCat<%: Html.DisplayFor(m => item.RouteID) %>" data-toggle="modal"><span class="label label-success">In Use</span></a>
                                     
                                        <div class="modal fade" id="ApplyPriceCat<%: Html.DisplayFor(m => item.RouteID) %>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title"> Change Price Category</h4>
                                                    </div>
                                                    <% using (Html.BeginForm("ApplyPrice", null, FormMethod.Post, new { @class = "smart-form client-form", role = "form", id = "smart-form-register", enctype = "multipart/form-data" }))
                                                    { %>
                                                        <div class="modal-body">
                                                           Are you sure you want to apply <%: Html.DisplayFor(m => item.PriceCategory) %> <strong> for <%: Html.DisplayFor(m => item.RName) %> </strong>?
                                                           
                                                            <input type="hidden" class="form-control" name="UserID" value="<%: Html.DisplayFor(m => item.RouteID) %>" />
                                                           <input type="hidden" name="Operation" value="ApplyPriceCat" />
                                                        </div>
                                                        <div class="modal-footer clearfix"> 
                                                             <button type="submit" class="btn btn-primary"> Yes</button>                         
                                                             <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel</button> 
                                                        </div>
                                                    <% }%>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                         <% }%>
                                   </td>
                                <td><a class="btn btn-xs btn-primary" href="/Sales/EditPrices/<%: Html.DisplayFor(m => item.RouteID) %>"><i class="fa fa-edit"></i></a></td>
                               
                            </tr>
                            <% } %>
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</asp:Content>
