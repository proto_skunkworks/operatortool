﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<OperatorToolSpace.Models.EditDriverModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Edit Sales" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
    <script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Scripts/jquery-ui.min.js"></script>
    <script src="../../Scripts/select2.full.min.js"></script>
    <script src="../../Scripts/select2.min.js"></script>
    <link href="../../Scripts/select2.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Edit Form <%: Html.DisplayTextFor(model => model.FirstName) %>  <%: Html.DisplayTextFor(model => model.LastName) %> </h3>
                </div>
                
                <div class="box-body">
                    <div class="row">
                       
                         <div class="container">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a data-toggle="tab" class="nav_link" data_id="1" href="#home">TRUCK</a></li>
                                                            <li><a data-toggle="tab" class="nav_link" data_id="2" href="#menu1">ROUTE(S)</a></li>
                                                            <li><a data-toggle="tab" class="nav_link" data_id="4" href="#menu3">TRUCK HELPER</a></li>
                                                            <li><a data-toggle="tab" class="nav_link" data_id="3" href="#menu2">TILL NUMBER</a></li>
                                                            <li><a data-toggle="tab" class="nav_link" data_id="3" href="#menu4">BIO DATA</a></li>
                                                            <li><a data-toggle="tab" class="nav_link" data_id="5" href="#menu5">ACCOUNT</a></li>
                                                            
                                                        </ul>

                                                        <div class="tab-content">
                                                            <div id="home" class="tab-pane fade in active">
                                                                <h4>ASSIGN TRUCK</h4>
                                                                CURRENT TRUCK: <strong><%: Html.DisplayFor(model => model.Truckreg, "", new { @class = "text-danger" }) %> </strong>
                                                                <input type="hidden" id="DriverID" value=<%: Html.DisplayTextFor(model => model.DriverID) %> />                                                                
                                                                   <div class="form-group">
                                                                    <label>Trucks</label>                                
                                                                    <select id="Truckreg" class="form-control" style="width:30%"></select>
                                                                        
                                                                   </div>                                                                
                                                               <button type="button" class="btn btn-primary" id="btnUpdateTruck" onclick="AssignTruck();">Update</button>
                                                                <br />
                                                                <div id="ProductsDiv1">
                                                                </div>
                                                            </div>
                                                            <div id="menu1" class="tab-pane fade">
                                                                <h4>ASSIGN ROUTE</h4>
                                                                    CURRENT ROUTES: <strong><%: Html.DisplayFor(model => model.CurrentRoute, "", new { @class = "text-danger" }) %> </strong>
                                                                    <div class="form-group">
                                                                    <label>Routes</label>

                                                                    <select class="form-control select2" multiple="multiple" id ="routes"data-placeholder="Select Routes"
                                                                    style="width: 40%;">
                  
                                                                    </select>

                                                                    </div>
                                                                   
                                                                <button type="button" class="btn btn-primary" id="btnUpdateRoute" onclick="AssignRoutes();">Assign</button>
                                                                <div id="ProductsDiv2">
                                                                </div>
                                                              </div>
                                                             <div id="menu3" class="tab-pane fade">
                                                                <h3>TRUCK HELPER</h3>
                                                                   CURRENT: <strong><%: Html.DisplayFor(model => model.CurrentRoute, "", new { @class = "text-danger" }) %> </strong>
                                                                   <div class="form-group">
                                                                    <label>Select Truck Helper</label>                                
                                                                    <select id="TruckHelper" class="form-control" style="width:30%"></select>                                                                        
                                                                   </div>
                                                                <button type="button" class="btn btn-primary" id="tn" onclick="UpdateTillNumber();">Update</button>
                                                                <div id="TruckHelperAss">
                                                                </div>
                                                            </div>
                                                         
                                                            <div id="menu2" class="tab-pane fade">
                                                                <h3>ASSIGN TILL</h3>
                                                                CURRENT TILL: <strong><%: Html.DisplayFor(model => model.CurrentTill, "", new { @class = "text-danger" }) %> </strong>
                                                                  <div class="form-group">
                                                                    <label>Select Till Number</label>                                
                                                                    <select id="TillNumber" class="form-control" style="width:30%"></select>                                                                        
                                                                   </div>
                                                                <button type="button" class="btn btn-primary" id="btnUpdateTill" onclick="UpdateTillNumber();">Update</button>
                                                                <div id="ProductsDiv3">
                                                                </div>
                                                            </div>

                                                             <div id="menu4" class="tab-pane fade">
                                                                <h3>EDIT DRIVER</h3>
                                                                  <% using (Html.BeginForm("UpdateDriver", null, FormMethod.Post, new { role = "form", id = "FormUser", enctype = "multipart/form-data" }))
                                                                      { %>
                                                                   <div class="form-group">
                                                                    <label for="exampleInputEmail1">First Name</label>
                                                                    <%: Html.TextBoxFor(model => model.FirstName, new { @class = "form-control", @required = "true", @style = "width:30%" }) %>                                
                                                                    <input type="hidden" class="form-control" name="UserID" id="UserID" value="<%: Model.DriverID %>" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Last Name</label>
                                                                    <%: Html.TextBoxFor(model => model.LastName, new { @class = "form-control", @style = "width:30%" }) %>
                                                                </div>
                                                                  <div class="form-group">
                                                                    <label for="exampleInputEmail1">Email</label>
                                                                    <%: Html.TextBoxFor(model => model.Email, new { @class = "form-control", @style = "width:30%" }) %>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Phone Number</label>
                                                                    <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "form-control", @style = "width:30%" }) %>
                                                                </div>
                                                                <%: Html.HiddenFor(model => model.DriverID, new { @class = "form-control", @style = "width:30%" }) %>
                                                                <div class="form-group">
                                                                    <label>Change Password</label>
                                                                    <%: Html.TextBoxFor(model => model.Password, new { @class = "form-control", @style = "width:30%" }) %>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary" id="EditDriver">Update</button>
                                                                <div id="ProductsDiv4">
                                                                </div>
                                                                 <%} %>
                                                            </div>
                                                            <div id="menu5" class="tab-pane fade">
                                                                <h3>App Accout Status</h3>
                                                                   Account Status: <strong><%: Html.DisplayFor(model => model.CurrentTill, "", new { @class = "text-danger" }) %> </strong>
                                                                 
                                                                <button type="submit" class="btn btn-danger" id="BlockAccount" onclick="Block();">BLOCK</button>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>

                                        </div>
                     
                                    </div>
                
                                
                                </div>
            <!-- /.box -->

        </div>
         <div class="row"  align="center">
         <a class="btn btn-xs btn-primary" href="/Drivers/DriversList"><i class="fa fa-angle-double-left" >Back</i></a>
        </div>
</div>
   <script type="text/javascript">
      $(".myselect").select2();
</script> 
  
     <script type="text/javascript">
        $(function () {
            var Truckregs = $("#Truckreg");
            Truckregs.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
            $.ajax({
                type: "POST",
                url: "/Drivers/GetTruckRegs",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    Truckregs.empty().append('<option selected="selected" value="0">Please select</option>');
                    $.each(response, function () {
                        Truckregs.append($("<option></option>").val(this['id']).html(this['Truckreg']));
                    });
                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
         });
 function AssignTruck() {  
   var Truck=$("#Truckreg :selected").attr('value');
    
    var empObj = {
        Truckreg: Truck,
        DriverID: $('#DriverID').val(),
    };  
    console.log(empObj);
    $.ajax({  
        url: "/Drivers/AssignTruck",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alert(result.responseText);
          
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
   }

         
function AssignRoutes() {  
    var selectedRoutes = $('#routes').val();
   
    
    var empObj = {
        Routes: selectedRoutes,   
           DriverID: $('#DriverID').val(),
    };  
    console.log(empObj);
    $.ajax({  
        url: "/Drivers/AssignRoutes",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            alert(result.responseText);
          
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
 }


$(function () {
            var ddlCustomers = $("#routes");
           
            $.ajax({
                type: "POST",
                url: "/Drivers/GetRoutes",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                        ddlCustomers.append($("<option></option>").val(this['id']).html(this['RouteName']));
                    });
                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
         });

         $(function () {
            var TruckHelper = $("#TruckHelper");
           
            $.ajax({
                type: "POST",
                url: "/Drivers/GetTruckHelper",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                  
                    $.each(response, function () {
                        TruckHelper.append($("<option></option>").val(this['FirstName']).html(this['FirstName']));
                    });
                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
        });
 </script>


    


<script>
$(function(){
$("#routes").select2();
});
</script>
     

<script>  
   $( "#routes" )  
     .change(function() {  
       var str = "";  
       $( "select option:selected" ).each(function() {  
           str += $(this).text() + " , ";
           console.log(str);
       });
     })  
     .trigger( "change" );  
   </script> 
</asp:Content>
