﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.SalesDataModels>>" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Sales" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- row -->
    
    <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/moment/min/moment.min.js"></script>
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <%: @TempData["Success"] %> 
       
            </div>
            <% } %>
        </div>
    </div>
    <div class="row" >
        <div class="col-xs-12">
            <div class="form-group">
                <label>Filter By Date:</label>

                <div class="input-group">
                    <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                        <span>
                            <i class="fa fa-calendar"></i>Date Range picker
                        </span>
                        <i class="fa fa-caret-down"></i>
                    </button>
                      <button class="btn btn btn-primary" ><i class="fa fa-filter"  id="LoadData"> FILTER</i></button>  
                    <input type="hidden" id="download_token_value_id"/>
                    <button type="button"  class="btn btn-primary" id="ExportSales">EXPORT AS EXCEL</button>
                </div>
                
            </div>
            
        </div>
       
           
       

    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="display: none">ID</th>
                                <th>Driver Name</th>
                                <th>Truck reg</th>
                                <th>Outlet Name</th>
                                <th>6KG Refill</th>
                                <th>13KG Refill</th>
                                <th>50KG Refill</th>
                                <th>6KG Outright</th>
                                <th>13KG Outright</th>
                                <th>50KG Outright</th>
                                <th>Grills</th>
                                <th>Burners</th>
                                <th>Total Amount</th>
                                <th>DateTime Sold</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                                {%>
                            <tr>
                                <td style="display: none"><%: Html.DisplayFor(m => item.SaleID) %></td>
                                <td><%: Html.DisplayFor(m => item.DriverName) %></td>
                                <td><%: Html.DisplayFor(m => item.Truckreg) %></td>
                                <td><%: Html.DisplayFor(m => item.OutletName) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Burners) %></td>
                                <td><%: Html.DisplayFor(m => item.Grills) %></td>
                                <td><%: Html.DisplayFor(m => item.DateSold) %></td>
                                <td><a class="btn btn-xs btn-primary" href="/Sales/ViewSale/<%: Html.DisplayFor(m => item.SaleID) %>"><i class="fa fa-edit"></i></a></td>

                            </tr>
                            <% } %>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <link href="../../Scripts/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <script src="../../Scripts/js.cookie.js"></script>
    <script>
        $(function () {
            var fileDownloadCheckTimer;
            $('.select2').select2()

            var startDate;
            var endDate;
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                     timePicker: true,
                   timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    //startDate: moment().subtract(29, 'days'),
                   // endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                   startDate = start.format('D MMMM YYYY h:MM:ss');
                   endDate = end.format('D MMMM YYYY h:MM:ss');
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })
            
             $('#LoadData').click(function(){
                 console.log('Start date: ' + endDate + '  EndDate: ' + startDate);
                 window.location = '<%:Url.Action("Sales","Sales")%>?StartDate=' + startDate+'&EndDate='+endDate;

               });
            $('#ExportSales').click(function () {
             console.log('Start date: ' + endDate + '  EndDate: ' + startDate); 
                 blockUIForDownload();
             var token = $('#download_token_value_id').val();
             window.location = '<%:Url.Action("ExportAsExcel","Sales")%>?token=' + token + '&StartDate=' + startDate + '&EndDate=' + endDate;
             });

        
        function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#download_token_value_id').val(token);
        $('#spinner').show();
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = Cookies.get('fileDownloadToken');
            if (cookieValue == token)
                finishDownload();
        }, 1000);
         }

    function finishDownload() {
        window.clearInterval(fileDownloadCheckTimer);
        Cookies.remove('fileDownloadToken'); //clears this cookie value

        $('#spinner').hide();
        }

});
</script>
</asp:Content>
