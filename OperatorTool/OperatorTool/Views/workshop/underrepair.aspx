﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.WorkshopDataModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Assets" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">     
    <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../Scripts/bower_components/moment/min/moment.min.js"></script>
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>
 
 
  <style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:72%; 
padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
    <script type="text/javascript">
        function ExportAsExcel() {  
            var fileDownloadCheckTimer;
            blockUIForDownload();

    var token = $('#download_token_value_id').val();
        $.ajax(
            {

                url: "/Drivers/ExportAsExcel?token="+token,
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',                
                type: "POST",
                success: function (d) {
                  
                  window.location = '/Drivers/ExportAsExcel?token='+token;
                },
              error: function (errormessage) {
            alert(errormessage.responseText);
              }
            });
   }
   function blockUIForDownload() {
        var token = new Date().getTime(); //use the current timestamp as the token value
        $('#download_token_value_id').val(token);
        $('#spinner').show();
        fileDownloadCheckTimer = window.setInterval(function () {
            var cookieValue = Cookies.get('fileDownloadToken');
            if (cookieValue == token)
                finishDownload();
        }, 1000);
    }        
</script>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                    <button type="button"  class="btn btn-primary" id="btnExcelDownload" onclick="ExportAsExcel();">EXPORT AS EXCEL</button>
                </div>
                         
              </div>
             

         </div>
        
        
            
    </div>
    <div class="row">

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>                           
                           <tr>                              
                                <th>Vehicle</th>
                                <th>Date</th> 
                                <th>Repair Type</th> 
                                <th>Issue Driver</th>
                                <th>Driver Info</th>
                                <th>Mechanic</th>
                                <th>Expected Release</th> 
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                                {%>
                            <tr>                               
                                <td><%: Html.DisplayFor(m => item.VRegNo) %></td>
                                 <td><%: Html.DisplayFor(m => item.date_checkedin) %></td>
                                 <td><%: Html.DisplayFor(m => item.description) %></td>
                                <td><%: Html.DisplayFor(m => item.repairType) %></td>
                                <td><%: Html.DisplayFor(m => item.MoreInfo) %></td>
                                <td>   </td>   
                                 <td><%: Html.DisplayFor(m => item.date_checkedout) %></td>
                                <td><a class="btn btn-xs btn-primary" href="/Assets/editVehicle/<%: Html.DisplayFor(m => item.VRegNo) %>">EDIT<i class="fa fa-cogs"></i></a>
                                  

                                </td>
                            </tr>
                            <% } %>
                        </tbody>                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
<link href="../../Scripts/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <script src="../../Scripts/jquery.min.js"></script>
    <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  })
</script>
<script>
    $(function () {
        var dialog, form,
            
            vregno = $("#vregno"),
            model = $("#model"),
            assetype = $("#assetype"),
            capacity = $("#capacity");

        $(function () {
          
            $("#asset_type")
                .selectmenu()
                .selectmenu("menuWidget")
                .addClass("overflow");
            
        });
        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " +
                    min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }


        function addUser() {
            console.log("Heey CLicked Me!!!");
            console.log($("#vregno"));
            console.log($("#model"));
            console.log( $("#assetype"));
            console.log( $("#capacity"));
        }

        dialog = $("#dialog-form").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "SAVE": addUser,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

       // form = dialog.find("form").on("submit", function (event) {
           // event.preventDefault();
           // addUser();
       // });

        $("#add-asset").button().on("click", function () {
            dialog.dialog("open");
        });
        $("#save_data").submit().on("click"), function () {
            console.log("Heey CLicked Me!!!");
        }
    });

  </script>

</asp:Content>
