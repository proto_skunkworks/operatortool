﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.CustomersModel>>" %>
<link href="../../Content/css/bootstrap.min.css" rel="stylesheet" />
<link href="../../Content/Site.css" rel="stylesheet" />
<link href="../../Content/sorter.min.css" rel="stylesheet" />
<script src="../../Scripts/jquery.min.js"></script>
    <script src="../../Content/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<body class="skin-blue">
<%: Html.Partial("_HeaderPartial") %>
</body>

<style>
    table, th, td {
    padding:2px 2px 2px 2px;


}

    .clientsDiv
{
border:1px solid #a1a1a1;
padding:10px 5px; 
border-radius:5px; 
 font-size:10px;
}

   .divBackground
        {   
            background: no-repeat center 0 url('../../Content/assets/bluetrax.png');
       
            background-position: 50% 50%;            
            background-repeat:no-repeat;  
            width:100px;
            height: 100px;           
        }

</style> 
   
       
            <div id="clients" style="width:20%; float:left; height:100%; " class="clientsDiv">

            <div id="gridClients" class="tablesortera" style=" overflow:scroll; height:100%; width:100%;  text-transform:uppercase; font-size:13px; font-weight:bold; " >
          
                <table id="example1">
                    <thead>
                        <th>Customers</th>
                    </thead>
             <% foreach (var item in Model)
               {%>
                    <tr class="clientsDiv">
                   
                     <td class="client_display-mode" >
                          <a href='javascript: getCustomerSales(<%:item.OutletID %>);' data-id="<%:item.OutletID %>">  <%:item.OutletName %></a>
                     </td>
                   
                    </tr>
              
               
           <%} %>
             </table>
          </div>
    </div>

     <div id="details" style="width:70%; float:left; height:100%; " class="clientsDiv">

            <div id="detailsdata" class="tablesortera" style=" overflow:scroll; height:100%; width:100%;  text-transform:uppercase; font-weight:bold; " >
           <div id="sales_details"> </div>
           </div>
    </div>

  <div id="summary" style="width:10%; float:left; height:100%; " class="clientsDiv">

            <div id="returns">
          </div>
    </div>
<link href="../../Content/css/AdminLTE.css" rel="stylesheet" />
<link href="../../Content/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

 function getCustomerSales(customer_id) {            
            var vidObj =
            {
                customer_id: customer_id,
            };
            $.ajax({
                type: "POST",
                url: "/Customers/SalesPerCustomer",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vidObj),
                dataType: "json",
                success: function (data) {                  
                 var table = $("#sales_details");         
                 var count = Object.keys(data).length;
                 if(count>0)
                 {
                   var saleid;
                  var eTable = "<table><thead><tr><th>Truck</th><th>Driver</th><th>Outlet</th><th>Refill 6KG</th><th>Refill 13KG</th><th>Refill 50KG</th><th>Outright 6KG</th><th>Outright 13KG</th><th>Outright 50KG</th><th>Date</th><th>RETURNS</th><th>PAYMENTS</th></tr></thead><tbody>"
                  $.each(data,function(index, row){   
                     eTable += "<tr>";
                      $.each(row, function (key, value) { 

                          if (key == "SaleID") {
                              saleid = value;
                          }
                             else if (key == "DateSold")
                             {
                                 var date = new Date(parseInt(value.substr(6)));
                                 eTable += "<td>" + date.toLocaleString() + "</td>";

                             }
                           else 
                             {
                                  eTable += "<td>" + value + "</td>"; 
                             }
                      }); 
                      eTable += '<td> <button class="btn btn-xs btn-primary "type="button" onclick="viewReturns('+saleid+');">RETURNS</button></td>';
                      eTable += '<td> </td>';
                  eTable += "</tr>";
                  });
                  eTable +="</tbody></table>";
                 table.html(eTable);

                 }
                else{
                table.html("No records found!");
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
     });
   
     }

     $(document).ready(function() {
    $('#example1').DataTable( {      
        "bPaginate": false,
        "bSortable": false
    } );
     });

     //VIEW RETURNS
     function viewReturns(saleid)
     {
         var IDObj= {
             SaleID:saleid,
         };
        $.ajax({
                type: "POST",
                url: "/Customers/GetCylinderReturns",               
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(IDObj),
                dataType: "json",
                success: function (data) {                  
                 var table = $("#returns");         
                 var count = Object.keys(data).length;
                 if(count>0)
                 {
                   var saleid;
                  var eTable = "<table><thead><th>Cylinder</th><th>6KG</th><th>13KG</th></thead><tbody>"
                  $.each(data,function(index, row){   
                     eTable += "<tr>";
                      $.each(row, function (key, value) { 
                           eTable += "<td>" + value + "</td>"; 
                      }); 
                      eTable += '<td> </td>';
                  eTable += "</tr>";
                  });
                  eTable +="</tbody></table>";
                 table.html(eTable);

                 }
                else{
                table.html("No records found!");
                }
                },
                failure: function (response) {
                 
                },
                error: function (response) {
                
                }
     });
     }
 </script> 
 <style>
        #sales_details{
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        font-size:10px !important;
        }
        #sales_details td, #sales_details th {
        border: 1px solid #ddd;
           font-size:11px !important;
        padding: 5px;
        }
        #sales_details tr:nth-child(even){background-color: #f2f2f2;}
        #example1
        {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size:10px !important;
            font-weight:bold;
            padding:4px;
            border: 0px solid #ddd;
        }
        #returns td, #returns th {
        border: 1px solid #ddd;
           font-size:11px !important;
        padding: 5px;
        }
          #example1 td, #example1 th {
        border: none;
           font-size:11px !important;
        padding: 5px;
        }
</style>

