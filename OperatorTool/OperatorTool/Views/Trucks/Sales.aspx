﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.SalesDataModels>>" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Sales" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/bower_components/jquery/dist/jquery.min.js"></script>



    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <%: @TempData["Success"] %> 
       
            </div>
            <% } %>
        </div>
    </div>
    <div class="row" >
         <div class="col-xs-12">
                     <div class="form-group" >
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date Range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                             <tr>
                              <th style="display:none">ID</th> 
                                <th>Truck reg</th>  
                                <th>Outlet Name</th>
                                <th>6KG Refill</th>
                                <th>13KG Refill</th>
                                <th>50KG Refill</th>
                                <th>6KG Outright</th>                                
                                <th>13KG Outright</th>                              
                                <th>50KG Outright</th> 
                                <th>Grills</th> 
                                <th>Burners</th>                                                               
                                <th>Total Amount</th> 
                                <th>DateTime Sold</th>                                 
                               <th>Mpesa Received</th>
                               <th>Variance</th>
                               <th>Comment</th>
                               <th>Actions</th>
                               <th>Payments</th>   
                             </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                            <tr>
                                 <td style="display:none"><%: Html.DisplayFor(m => item.SaleID) %></td>
                                <td><%: Html.DisplayFor(m => item.Truckreg) %></td>
                                <td><%: Html.DisplayFor(m => item.OutletName) %></td>                              
                                <td><%: Html.DisplayFor(m => item.Refill_6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_13KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Refill_50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_6KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Outright_13KG) %></td>
                                 <td><%: Html.DisplayFor(m => item.Outright_50KG) %></td>
                                <td><%: Html.DisplayFor(m => item.Burners) %></td>
                                <td><%: Html.DisplayFor(m => item.Grills) %></td>
                                 <td><%: Html.DisplayFor(m => item.TranAmount) %></td>
                                  <td><%: Html.DisplayFor(m => item.DateSold) %></td>
                                
                                <td><%: Html.DisplayFor(m => item.MpesaAmount) %></td>
                                 <td><%: Html.DisplayFor(m => item.Variance) %></td>
                                  <td><%: Html.DisplayFor(m => item.comment) %></td> 
                                <td><a class="btn btn-xs btn-primary" href="/Sales/ViewSale/<%: Html.DisplayFor(m => item.SaleID) %>"><i class="fa fa-eye"></i></a>                               
                                </td>
                                <td>
                               <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#<%: Html.DisplayFor(m => item.SaleID) %>">ADD</button>
                               <div class="modal fade" id="<%:item.SaleID%>">" role="dialog">
                               <div class="modal-dialog modal-xs">
                               <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Add Payment</h4>
                                </div>
                                <div class="modal-body">
                                  <h4>Payment for: <%: Html.DisplayFor(m => item.OutletName) %></h4>
                                    <div class="form-group">
                                        <label>Payment Method</label>
                                        <select class="form-control" id="paymentMethod<%:item.SaleID %>">
                                        <option value="Mpesa Payment">Mpesa Payment</option>
                                        <option value="Bank Payment">Bank Payment</option>
                                        <option value="Cheque Payment">Cheque Payment</option>                   
                                   </select>                                        
                                    </div>
                                      <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" name="phoneNumber"   id="phoneNumber<%:item.SaleID %>" class="form-control"/>
                                    </div>
                                     <div class="form-group">
                                        <label>Paid Amount</label>
                                        <input type="text"  name="paidAmount" id="paidAmount<%:item.SaleID %>" class="form-control"/>
                                    </div>                                 
                                </div>
                                   <input type="hidden" id="saleid<%:item.SaleID %>" value="<%:item.SaleID %>"/>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="AddPayment(<%:item.SaleID %>);">SAVE</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                             </td>
                            </tr>
                            <% } %>
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

     <script src="../../Scripts/alertifyjs/alertify.min.js"></script>
    <link href="../../Scripts/alertifyjs/css/alertify.min.css" rel="stylesheet" />
  <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date picker
    $('#datepicker').datepicker({
        autoclose: true,
        format:'yyyy-MM-dd HH:mm:ss',
    })
})


 function AddPayment(saleid) { 

       var saleid = $('#saleid' + saleid + '').val();
       var phoneNumber = $('#phoneNumber' + saleid).val();
       var paidAmount = $('#paidAmount' +saleid).val();    
       var paymentMethod = $('#paymentMethod'+saleid+' :selected').attr('value');
       var empObj = {      
         paymentMethod: paymentMethod,       
         saleid: saleid,
         phoneNumber: phoneNumber,
         paidAmount: paidAmount,
       
       };

     console.log(empObj);
    $.ajax({  
        url: "/Sales/AddPayment",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {
            $('#' + saleid + '').modal('hide');
            alertify.alert('Message', result.responseText, function () { alertify.message('Ok'); }); 
        },  
         failure: function (result) {
                  alertify.alert('Error', 'Ooops! Make sure you filled all the necessary fields', function () { alertify.error('Ok'); });
                },
        error: function (errormessage) {  
              alertify.alert('Message', errormessage.responseText, function(){ alertify.error('Ok'); });   
        }  
    });  
   }

</script>
</asp:Content>
