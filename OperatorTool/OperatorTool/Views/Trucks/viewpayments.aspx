﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.EditSalesModel>>" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "View Sale" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/jquery.min.js"></script>
    <link href="../../Scripts/bootstrap.min.css" rel="stylesheet" />

    <!-- row -->
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-primary">
              
                <div class="box-body">
                    
                        <table id="example1" class="table table-bordered table-hover">
                        <thead>
                             <tr>
                              
                                 <th>Driver Name</th> 
                                 <th>Truck reg</th>  
                                 <th>Outlet Name</th>
                                 <th>Product Sold</th>
                                 <th>Quantity</th>  
                                 <th>SKU Trans Amount</th>
                                 <th>DateTime Sold</th> 
                                 <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                               {%>
                           
                            <tr>
                                <td><%: Html.DisplayFor(m => item.DriverName) %></td>
                                <td><%: Html.DisplayFor(m => item.Truckreg) %></td>
                                <td><%: Html.DisplayFor(m => item.OutletName) %></td>                              
                                <td><%: Html.DisplayFor(m => item.ProductDesc) %></td>
                                <td><%: Html.DisplayFor(m => item.Quantity) %></td> 
                                 <td><%: Html.DisplayFor(m => item.SKUTransAmount) %></td> 
                                  <td><%: Html.DisplayFor(m => item.DateSold) %></td>  
                                 <td>
                                     <button type="button" class="getAllEMP"  id="" onclick="EditSale(<%:item.RowID %>);">Edit</button>
                                </td>                               
                            </tr>
                             
                            <% } %>
                        </tbody>
                        
                    </table>
                    
                </div>
              
            </div>
            <!-- /.box -->

        </div>
                           <div class="modal fade" id="myModal" role="dialog">
                               <div class="modal-dialog modal-xs">
                               <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Add Payment</h4>
                                </div>
                                <div class="modal-body">
                                 <div id="myModalContent"> </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="savepayment">SAVE</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>


    </div>
<script>
    //edit data
    
    function EditSale(id) {
        console.log(id);
            $(".getAllEMP").click(function () {
                var $buttonClicked = $(this);
                var options = { "backdrop": "static", keyboard: true };
                $.ajax({
                    type: "GET",
                    url: '/Sales/_editSale?row_id='+id,
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (data) {
                        debugger;
                        $('#myModalContent').html(data);
                        $('#myModal').modal(options);
                        $('#myModal').modal('show');
                    },
                    error: function (ex) {
                        alert(ex);
                        console.log(ex);
                    }
                });
            });
            $("#closbtn").click(function () {
                debugger;
                $('#myModal').modal('hide');
            });
        }
</script>

</asp:Content>
