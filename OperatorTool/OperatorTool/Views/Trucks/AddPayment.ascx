﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OperatorToolSpace.Models.AddPaymentModel>" %>

<script src="<%: Url.Content("~/Scripts/jquery-1.8.2.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.AntiForgeryToken() %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
       
        <div class="editor-label">
           Payment Method
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.paymentMethod, new { @class = "form-control", @placeholder="Enter Lastname"}) %>
            <%: Html.ValidationMessageFor(model => model.paymentMethod) %>
        </div>

        <div class="editor-label">
           Amount Paid
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.paidAmount) %>
            <%: Html.ValidationMessageFor(model => model.paidAmount) %>
        </div>

        <div class="editor-label">
            Phone Number
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.phoneNumber) %>
            <%: Html.ValidationMessageFor(model => model.phoneNumber) %>
        </div>

        <p>
            <input type="submit"  value="Create" />
        </p>
    </fieldset>
<% } %>

