﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OperatorToolSpace.Models.FuelRequestsModel>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewBag.PageTitle = "Fuel Requests" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!-- row -->
    <div class="row">
        <div class="col-md-6">
            <% if (TempData["Success"] != null)
               { %>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> Success.
       
            </div>
            <% } %>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-hover">
                        <thead>                           
                           <tr>  
                                <th>Order ID</th> 
                                <th>Driver Name</th> 
                                <th>Asset</th> 
                                <th>Mileage </th>
                                <th>Date Requested</th>
                                <th>Status</th>
                                  <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in Model)
                                {%>
                            <tr>                               
                                <td><%: Html.DisplayFor(m => item.order_id) %></td>
                                <td><%: Html.DisplayFor(m => item.driver_name) %></td>
                                <td><%: Html.DisplayFor(m => item.request_vregno) %></td>
                                  <td><%: Html.DisplayFor(m => item.request_mileage)+" Km" %></td>
                                <td><%: Html.DisplayFor(m => item.date_requested) %></td> 
                                <%if (item.request_status == "Pending")
                                              { %>
                                        <td><span class="label label-default"><%: Html.DisplayFor(m => item.request_status) %></span> </td>
                                   <% } else if (item.request_status == "Approved") {%>
                               <td><span class="label label-success"><%: Html.DisplayFor(m => item.request_status) %></span> </td>

                                <%}  else if (item.request_status == "Closed") {%>
                                <td><span class="label label-warning"><%: Html.DisplayFor(m => item.request_status) %></span> </td>

                                <%}  else if (item.request_status == "Rejected") {%>
                                 <td><span class="label label-danger"><%: Html.DisplayFor(m => item.request_status) %></span> </td>

                                <%} %>
                                 <%if (item.request_status == "Pending")
                                     { %>
                               <td>
                                    
                                    <a href="#getdata"><span class="label label-success">Approve</span></a>
                                    <div class="modal fade" id="getdata" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Approve Fuel Request</h4>
                                                </div>
                                                <% using (Html.BeginForm("ApproveFuelRequest", null, FormMethod.Post, new { @class = "smart-form client-form", role = "form", id = "smart-form-register", enctype = "multipart/form-data" }))
                                                   { %>
                                                <div class="modal-body">
                                                   <strong><%: Html.DisplayFor(m => item.driver_name) %> <%: Html.DisplayFor(m => item.request_vregno) %> <%: Html.DisplayFor(m => item.date_requested) %></strong>?
                                                           
                                                       

                                                    <input type="hidden" class="form-control" name="UserID" value="<%: Html.DisplayFor(m => item.order_id) %>" />
                                                    <input type="hidden" name="Operation" value="Activate" />
                                                </div>
                                                <div class="modal-footer clearfix">
                                                    <button type="submit" class="btn btn-primary">Yes</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                </div>
                                                <% }%>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.Activate User modal -->

                                </td>
                                <%} else { %>
                               
                                 <td>_</td>
                                 <%} %>
                            </tr>
                            <% } %>
                        </tbody>                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</asp:Content>
