﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="../../Content/img/avatar3.png" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p><%: Session["Firstname"] %></p>

            <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
        </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
       <ul class="sidebar-menu">
        <li class="active">
            <a href="/Home/Index">
                <i class="fa fa-dashboard"></i><span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-truck"></i>
                  <strong><span>Logistics</span> </strong>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                 <li><a href="/Assets/Trucks"><i class="fa fa-truck"></i>Vehicles</a></li>
                <li><a href="/Drivers/AddDriver"><i class="fa fa-user"></i>Add Driver</a></li>
                <li><a href="/Drivers/DriversList"><i class="fa fa-users"></i>Allocations</a></li> 
                  <li><a href="/Assets/dashboard"><i class="fa fa-tint"></i>Fuel Requests</a></li> 
                 <li><a href="/planning/dashboard"><i class="fa fa-exchange"></i>Planning</a></li> 
                  <li><a href="/workshop/dashboard"><i class="fa fa-cogs"></i>Workshop</a></li> 
            </ul>
        </li>

         <li class="treeview">
            <a href="#">
                <i class="fa fa-money"></i>
                 <strong><span>Cylinder Sales</span> </strong>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu"> 
                <li><a href="/Sales/Sales"><i class="fa fa-angle-double-right"></i>Transactons</a></li> 
                 <li><a href="/Trucks/TruckWiseSale"><i class="fa fa-angle-double-right"></i>Truckwise Sales</a></li> 
                 <li><a href="/Customers/CustomerSales"><i class="fa fa-angle-double-right"></i>Customer Sales</a></li> 
                 <li><a href="/sales/soldproducts"><i class="fa fa-angle-double-right"></i>Products Sales</a></li> 
                <li><a href="/Sales/PriceList"><i class="fa fa-angle-double-right"></i>Price List</a></li> 
                 <li><a href="/Sales/AcceptedCylinders"><i class="fa fa-angle-double-right"></i>Accepted Cylinders</a></li> 
                <li><a href="/Customers/CustomerList"><i class="fa fa-angle-double-right"></i>Customers & Pricing</a></li> 
            </ul>
        </li>

            <li class="treeview">
            <a href="#">
                <i class="fa fa-money"></i>
                 <strong><span>Bulk Sales</span> </strong>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu"> 
                <li><a href="/Bulk/bulkorders"><i class="fa fa-archive"></i>New Orders</a></li> 
                <li><a href="/Bulk/pendingplanning"><i class="fa fa-clock-o"></i>Planning</a></li>
                <li><a href="/Bulk/pendingdelivery"><i class="fa fa-envelope"></i>Pending Delivery</a></li>
                <li><a href="/Bulk/deliveredorders"><i class="fa fa-check-square"></i>Delivered Orders</a></li>
            </ul>
        </li>
        

         <li class="treeview">
            <a href="#">
                <i class="fa fa-road"></i>
                <strong><span>Routes</span> </strong>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="/Sales/PriceList"><i class="fa fa-angle-double-right"></i>Change Prices</a></li>                              
            </ul>
        </li>
             <li class="treeview">
            <a href="#">
                <i class="fa fa-users"></i>
                <strong><span>User Management</span> </strong>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-users"></i>Presellers</a></li>   
                <li><a href="#"><i class="fa fa-users"></i>Drivers</a></li>   
                <li><a href="#"><i class="fa fa-users"></i>Bulk App Users</a></li>   
                <li><a href="#"><i class="fa fa-users"></i>Stock Transfer</a></li> 
              
            </ul>
        </li>
        <li>
            <a href="/Home/SignOut">
                <i class="fa fa-power-off"></i><span>Logout</span>
            </a>
        </li>
       
    </ul>
</section>
